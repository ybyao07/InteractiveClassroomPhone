//
//  HD_OfflineTestVC.m
//  InteractiveClassroomPhone
//
//  Created by 姚永波 on 2018/3/29.
//  Copyright © 2018年 姚永波. All rights reserved.
//

#import "HD_OfflineTestVC.h"
#import "SubjectCollectionCell.h"
#import "TestJudgeVC.h"

@interface HD_OfflineTestVC ()

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (strong, nonatomic) NSMutableArray *resources;

@end

@implementation HD_OfflineTestVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"线下测试";
    [HDSocketSendCommandTool sendSocketParam:@{} commandCode:@(TestPaper) successBlock:^(id result) {
    }];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshViewSockentData:) name:kHDNotificationCenterSocket object:nil];
    [self.collectionView registerNib:[UINib nibWithNibName:@"SubjectCollectionCell" bundle:nil] forCellWithReuseIdentifier:@"SubjectCollectionCell"];
//    [self addTestData];
}
- (void)addTestData
{
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    dic[@"data"] = @[@{
                         @"cardId":@"",
                         @"cardName":@"单选多选判读数字",
                         @"problemNum":@(9),
                         @"Selected":@(0),
                         @"sort":@"1",
                         },
                     @{
                         @"cardId":@"",
                         @"cardName":@"试卷登分",
                         @"problemNum":@(6),
                         @"Selected":@(1),
                         @"sort":@"2",
                         },
                     @{
                         @"cardId":@"",
                         @"cardName":@"试卷登分",
                         @"problemNum":@(1),
                         @"Selected":@(1),
                         @"sort":@"3",
                         }];
    
    [self.resources removeAllObjects];
    NSArray *array = [TestModel mj_objectArrayWithKeyValuesArray:dic[@"data"]];
    [self.resources addObjectsFromArray:array];
    [self.collectionView reloadData];
}
- (void)refreshViewSockentData:(NSNotification *)notif
{
    NSDictionary *dic = notif.userInfo;
    if ([dic[@"code"] integerValue] == TestPaperList) {
        [self.resources removeAllObjects];
        NSArray *array = [TestModel mj_objectArrayWithKeyValuesArray:dic[@"data"]];
        [self.resources addObjectsFromArray:array];
        [self.collectionView reloadData];
    }
}
#pragma mark ================= collection delegate ==================
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.resources.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    SubjectCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"SubjectCollectionCell" forIndexPath:indexPath];
    cell.model = self.resources[indexPath.row];
    return cell;
}
//定义每个UICollectionView 的大小
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake((SCREEN_WIDTH - 40 - 10 )/2, (SCREEN_HEIGHT - 74 - 120 - 3 * 10)/4);
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return 10;
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return 10;
}
//UICollectionView被选中时调用的方法
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    TestJudgeVC *jdugeVc = [TestJudgeVC new];
    TestModel *model = self.resources[indexPath.row];
    jdugeVc.model = model;
    [self.navigationController pushViewController:jdugeVc animated:YES];
}

- (IBAction)startTest:(UIButton *)sender {
    UIAlertView *alertV = [[UIAlertView alloc] initWithTitle:@"" message:@"点击对应题目自动进入测评" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
    [alertV show];
}

- (IBAction)closeAction:(UIButton *)sender {
    [HDSocketSendCommandTool sendSocketParam:@{} commandCode:@(CloseUI) successBlock:^(id result) {
    }];
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (void)goBackPage
{
    [HDSocketSendCommandTool sendSocketParam:@{} commandCode:@(CloseUI) successBlock:^(id result) {
    }];
    [self.navigationController popToRootViewControllerAnimated:YES];
}



#pragma mark ===== accessory =====
- (NSMutableArray *)resources
{
    if (!_resources) {
        _resources = [NSMutableArray array];
    }
    return _resources;
}
- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
