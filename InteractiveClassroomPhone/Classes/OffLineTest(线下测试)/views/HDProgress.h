//
//  HDProgress.h
//  InteractiveClassroom
//
//  Created by 姚永波 on 2018/3/17.
//  Copyright © 2018年 姚永波. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HDProgress : UIView
{
    UIView *viewTop;
    UIView *viewBottom;
    UILabel *progressLb;
}

/**
 进度值
 */
@property (nonatomic,copy) NSString *progressValue;


@property (nonatomic,copy) NSString *progressLableValue;

/**
 进度条的颜色
 */
@property (nonatomic,strong) UIColor *progressColor;

/**
 进度条的背景色
 */
@property (nonatomic,strong) UIColor *bottomColor;

/**
 进度条的速度
 */
@property (nonatomic,assign) float time;

@end
