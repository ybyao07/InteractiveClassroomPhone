//
//  TestStudentProgressCell.h
//  InteractiveClassroom
//
//  Created by 姚永波 on 2018/3/17.
//  Copyright © 2018年 姚永波. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TestStudentProgressCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblName;

@end
