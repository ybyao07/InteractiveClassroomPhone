//
//  TestStudentModel.h
//  InteractiveClassroom
//
//  Created by 姚永波 on 2018/3/17.
//  Copyright © 2018年 姚永波. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TestStudentModel : NSObject

@property (nonatomic, copy) NSString *studentName;
@property (nonatomic, strong) NSNumber *TestState; //0未开始，1进行中， 2结束

@property (nonatomic, copy) NSString *parentName;
@property (nonatomic, copy) NSString *parentTel;
@end
