//
//  TestJudgeVC.m
//  InteractiveClassroomPhone
//
//  Created by 姚永波 on 2018/4/15.
//  Copyright © 2018年 姚永波. All rights reserved.
//

#import "TestJudgeVC.h"
#import "HDProgress.h"
#import "TestProgressVC.h"
#import "TestResultVC.h"

@interface TestJudgeVC ()
{
    HDProgress *progress;
}
@property (weak, nonatomic) IBOutlet UILabel *lblTimeTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblTimeCount;
@property (weak, nonatomic) IBOutlet UILabel *lblProgressTitle;

@end

@implementation TestJudgeVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = _model.cardName;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshViewSockentData:) name:kHDNotificationCenterSocket object:nil];
    [self sendSocketData];
    progress = [[HDProgress alloc]initWithFrame:Rect(10, CGRectGetMaxY(_lblProgressTitle.frame) + 80 , SCREEN_WIDTH-20, 30)];
    [self.view addSubview:progress];
}


- (void)refreshViewSockentData:(NSNotification *)notif
{
    NSDictionary *dic = notif.userInfo;
    if ([dic[@"code"] integerValue] == TestPaperProgress) {
        int finishCount = (int)[dic[@"finishCount"] integerValue];
        int totalCount = (int)[dic[@"totalCount"] integerValue];
        _lblProgressTitle.text = [NSString stringWithFormat:@"测评进度：已完成%d人/未完成%d人",finishCount,totalCount-finishCount];
        float value = (float)finishCount/totalCount;
        progress.progressLableValue=[NSString stringWithFormat:@"已完成%.1f%%",value*100];
        progress.progressValue = [NSString stringWithFormat:@"%f",value] ;
        self.lblTimeCount.text = dic[@"time"];
    }
}

- (void)sendSocketData
{
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    dic[@"cardId"] = self.model.cardId;
    [HDSocketSendCommandTool sendSocketParam:dic commandCode:@(StartTestPaper) successBlock:^(id result) {
    }];
}
- (IBAction)didClickedButton:(UIButton *)btn {
    if (btn.tag == 0) { // 进度详情
        TestProgressVC *progressVC = [TestProgressVC new];
        progressVC.testM = self.model;
        [self.navigationController pushViewController:progressVC animated:YES];
    }
    if (btn.tag == 1) { // 刷新进度
        [HDSocketSendCommandTool sendSocketParam:@{} commandCode:@(RefreshTestPaperProgress) successBlock:^(id result) {
        }];
    }
    if (btn.tag == 2) { // 结束测评
        TestResultVC *resultVC = [TestResultVC new];
        resultVC.navTitle = self.model.cardName;
        [self.navigationController pushViewController:resultVC animated:YES];
    }
}

- (void)goBackPage
{
    [HDSocketSendCommandTool sendSocketParam:@{} commandCode:@(CloseUI) successBlock:^(id result) {
    }];
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
