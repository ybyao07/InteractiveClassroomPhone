//
//  TestResultVC.m
//  InteractiveClassroomPhone
//
//  Created by 姚永波 on 2018/4/15.
//  Copyright © 2018年 姚永波. All rights reserved.
//

#import "TestResultVC.h"
#import "HD_OfflineTestVC.h"
@interface TestResultVC ()

@property (weak, nonatomic) IBOutlet UILabel *lblTime;
@property (weak, nonatomic) IBOutlet UILabel *lblFinish;
@property (weak, nonatomic) IBOutlet UILabel *lblUnfinish;

@end

@implementation TestResultVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = self.navTitle;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshViewSockentData:) name:kHDNotificationCenterSocket object:nil];
    [self sendSocketData];
}


- (void)refreshViewSockentData:(NSNotification *)notif
{
    NSDictionary *dic = notif.userInfo;
    if ([dic[@"code"] integerValue] == TestPaperResult) {
        NSString *time = dic[@"time"];
        NSString *finishCount = dic[@"finishCount"];
        NSString *unfinishCount = dic[@"unfinishCount"];
        _lblTime.text = time;
        _lblFinish.text = finishCount;
        _lblUnfinish.text = unfinishCount;
    }
}


- (void)sendSocketData
{
    [HDSocketSendCommandTool sendSocketParam:@{} commandCode:@(EndTestPaper) successBlock:^(id result) {
    }];
}

- (IBAction)closeSaveAction:(UIButton *)sender {
    [HDSocketSendCommandTool sendSocketParam:@{} commandCode:@(SaveTestPaper) successBlock:^(id result) {
    }];
    for(UIViewController *temp in self.navigationController.viewControllers)
    {
        if([temp isKindOfClass:[HD_OfflineTestVC class]])
        {
            [self.navigationController popToViewController:temp animated:YES];
        }
    }
}

- (void)goBackPage
{
    [HDSocketSendCommandTool sendSocketParam:@{} commandCode:@(SaveTestPaper) successBlock:^(id result) {
    }];
    for(UIViewController *temp in self.navigationController.viewControllers)
    {
        if([temp isKindOfClass:[HD_OfflineTestVC class]])
        {
            [self.navigationController popToViewController:temp animated:YES];
        }
    }
}
- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
