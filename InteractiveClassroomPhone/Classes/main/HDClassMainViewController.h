//
//  HDClassMainViewController.h
//  InteractiveClassroom
//
//  Created by 姚永波 on 2018/2/10.
//  Copyright © 2018年 姚永波. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YBaseViewController.h"


typedef enum
{
    eMainControlTabbarTag_Resource= 0,   //资源
    eMainControlTabbarTag_Photo,           //拍照上传
    eMainControlTabbarTag_AnswerQuestion,             //答题
    eMainControlTabbarTag_Vote,             //投票
    eMainControlTabbarTag_Name,             //点名
    eMainControlTabbarTag_AllAnswer,        // 抢答
    eMainControlTabbarTag_Competition,      // 竞赛
    eMainControlTabbarTag_Test,             // 线下测试
    eMainControlTabbarTag_Close,            // 断开连接
    
} MainControlTabbarTag;

@interface HDClassMainViewController : YBaseViewController


@end
