//
//  HDClassMainViewController.m
//  InteractiveClassroom
//
//  Created by 姚永波 on 2018/2/10.
//  Copyright © 2018年 姚永波. All rights reserved.
//

#import "HDClassMainViewController.h"
#import "TabToolBar.h"
#import "TabBarButton.h"
#import "YBaseViewController.h"
#import "YNavigationViewController.h"
#import "HDResourceViewController.h"
#import "HDWifiView.h"
#import "HD_VoteVCViewController.h"
#import "UIButton+ImageTitleCentering.h"
#import "PhotoViewController.h"
#import "HD_SolveProblemVC.h"
#import "HD_CallNameVC.h"
#import "HD_AllMemerAnswerVC.h"
#import "HD_OfflineTestVC.h"
#import "HD_TeamCompetionVC.h"
#import "UIWindow+Extension.h"
#import "HDSocketService.h"


#define  gBKMain_maxtabNum                10

@interface HDClassMainViewController ()<TabToolBarProtocol>
{
    NSMutableDictionary * _rootArray;
}

@property (strong, nonatomic) TabToolBar *tabBar;
@property (strong, nonatomic) HDWifiView *wifiView;

@property (weak, nonatomic) IBOutlet UIButton *btnResource;

@property (weak, nonatomic) IBOutlet UIButton *btnAnswer;

@property (weak, nonatomic) IBOutlet UIButton *btnCall;

@end

@implementation HDClassMainViewController

- (instancetype)init{
    self = [super init];
    if (self) {
        _rootArray = [[NSMutableDictionary alloc] initWithCapacity:gBKMain_maxtabNum];
        NSOperationQueue *queue = [[NSOperationQueue alloc]init];
        [queue addOperationWithBlock:^{
            // 创建服务对象
            HDSocketService *socketSerview = [[HDSocketService alloc]init];
            //开始服务
            [socketSerview start];
            //循环运行
            [[NSRunLoop currentRunLoop] addPort:[NSPort port] forMode:NSDefaultRunLoopMode];
            [[NSRunLoop currentRunLoop] run];
        }];
    }
    return self;
}

#pragma -mark UIViewcontroller function
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"棒棒答";
    self.hd_navigationBar.leftButton.hidden = YES;
    self.view.backgroundColor = JUIColorFromRGB_bg;
    // 添加WiFI
    [self guideWifiView];
    [self.btnResource centerVertically];
    [self.btnAnswer centerVertically];
    [self.btnCall centerVertically];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(linkSuccessed) name:kLinkNotification object:nil];
}

- (void)guideWifiView
{
    _wifiView = [[HDWifiView alloc] initWithFrame:Rect(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
//    _wifiView.backgroundColor = [UIColor greenColor];
    _wifiView.backgroundColor = JUIColorFromRGB_bgGrayBlue;
    [self.view addSubview:_wifiView];
}
- (void)linkSuccessed
{
    [self.wifiView removeFromSuperview];
    _wifiView = nil;
}

- (void)buildUIViews:(CGRect)frame
{
    CGRect rect = frame;
    rect.origin.x = 0;
    rect.origin.y = 0;
    [self buildTabToolBar:frame];
}


#pragma mark ====== action ========

- (IBAction)goResource:(UIButton *)sender {
    YBaseViewController* ctrl = nil;
    ctrl = [[HDResourceViewController alloc] init];
    [self.navigationController pushViewController:ctrl animated:YES];
}

- (IBAction)voteAction:(UIButton *)sender {
    YBaseViewController* ctrl = [[HD_VoteVCViewController alloc] init];
    [self.navigationController pushViewController:ctrl animated:YES];
}
- (IBAction)photoAction:(UIButton *)sender {
    YBaseViewController* ctrl = [[PhotoViewController alloc] init];
    [self.navigationController pushViewController:ctrl animated:YES];
}

- (IBAction)solveAction:(UIButton *)sender {
    YBaseViewController* ctrl = [[HD_SolveProblemVC alloc] init];
    [self.navigationController pushViewController:ctrl animated:YES];
}

- (IBAction)allAnserAction:(UIButton *)sender {
    YBaseViewController* ctrl = [[HD_AllMemerAnswerVC alloc] init];
    [self.navigationController pushViewController:ctrl animated:YES];
}
- (IBAction)competeAction:(UIButton *)sender {
    YBaseViewController* ctrl = [[HD_TeamCompetionVC alloc] init];
    [self.navigationController pushViewController:ctrl animated:YES];
}
- (IBAction)callNameAction:(UIButton *)sender {
    YBaseViewController* ctrl = [[HD_CallNameVC alloc] init];
    [self.navigationController pushViewController:ctrl animated:YES];
}

- (IBAction)offLineAction:(UIButton *)sender {
    YBaseViewController* ctrl = [[HD_OfflineTestVC alloc] init];
    [self.navigationController pushViewController:ctrl animated:YES];
}

- (IBAction)closeConnectAction:(UIButton *)sender {
//    [getService() stop];
    [HDSocketSendCommandTool sendSocketParam:@{} commandCode:@(CloseConnect) successBlock:^(id result) {
    }];
    HDClassMainViewController *controller = [[HDClassMainViewController alloc] init];
    YNavigationViewController *nav = [[YNavigationViewController alloc] initWithRootViewController:controller];
    [UIWindow changeWindowRootViewController:nav animated:YES];
}




#pragma mark ======= tab ======
- (void)buildTabToolBar:(CGRect)rect
{
    if (_tabBar) {
        return;
    }
    CGFloat toolbar_height =  gJDefaultTabbarHeight;
    CGRect     toolBarRect = CGRectMake(200,
                                        rect.size.height - toolbar_height - (iphoneX ? 34 : 0),
                                        rect.size.width - 400,
                                        toolbar_height);
    
    NSArray* imgs = [NSArray arrayWithObjects:
                     @"icon_home_resource", @"icon_home_resource",@"icon_home_resource",
                     @"icon_home_photo",   @"icon_home_photo",   @"icon_home_photo",
                     @"icon_home_test",     @"icon_home_test",      @"icon_home_test",
                     @"icon_home_vote",   @"icon_home_vote",     @"icon_home_vote",
                     @"icon_home_call",  @"icon_home_call",    @"icon_home_call",
                     @"icon_home_photo",   @"icon_home_photo",   @"icon_home_photo",
                     @"icon_home_team",     @"icon_home_team",      @"icon_home_team",
                     @"icon_home_photo",   @"icon_home_photo",   @"icon_home_photo",
                     @"icon_home_test",     @"icon_home_test",      @"icon_home_test",
                     nil];
    NSArray* txts = [NSArray arrayWithObjects:@"资源", @"拍照上传",@"答题",@"投票",@"点名",@"全员抢答",@"团队竞赛",@"线下测试",@"断开链接", nil];
    int Tags[] =
    {
        eMainControlTabbarTag_Resource,   //资源
        eMainControlTabbarTag_Photo,           //拍照上传
        eMainControlTabbarTag_AnswerQuestion,             //答题
        eMainControlTabbarTag_Vote,             //投票
        eMainControlTabbarTag_Name,             //点名
        eMainControlTabbarTag_AllAnswer,        // 抢答
        eMainControlTabbarTag_Competition,      // 竞赛
        eMainControlTabbarTag_Test,             // 线下测试
        eMainControlTabbarTag_Close,
    };
    
    UIImage* tagNew = [UIImage imageNamed:@"new.png"];
    NSMutableArray* items = [NSMutableArray arrayWithCapacity:[imgs count]];
    for (int ii = 0; ii < [imgs count]/3 ; ++ii)
    {
        UIImageView* tagView = [[UIImageView alloc] initWithImage:tagNew];
        tagView.frame = CGRectMake(35, 2, 22, 10);
        tagView.hidden = YES;
        TabBarButton* btn = [[TabBarButton alloc] initWithFrame:CGRectMake(0, 0, 50, toolbar_height)];
        btn.tag = Tags[ii];
        [btn setImage:[imgs objectAtIndex:ii*3]   forState:UIControlStateNormal];
        [btn setImage:[imgs objectAtIndex:ii*3+1] forState:UIControlStateHighlighted];
        [btn setImage:[imgs objectAtIndex:ii*3+2] forState:UIControlStateSelected];
        [btn setText:[txts objectAtIndex:ii]];
        [btn addSubview:tagView];
        [items addObject:btn];
    }
    UIImageView* bgView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"pgt_tabbar_bg"]];
    bgView.frame = CGRectMake(0, -1, toolBarRect.size.width, toolBarRect.size.height+1);
    bgView.backgroundColor = [UIColor whiteColor];
    
    TabToolBar * tabBar = [[TabToolBar alloc] init];
    tabBar.frame       = toolBarRect;
    tabBar.fixedSpace  = YES;
    tabBar.backgroundView = bgView;
    tabBar.clickDelegate  = self;
    tabBar.items          = items;
    self.tabBar = tabBar;
}



#pragma -mark TabToolBarProtocol
-(void)switchToTab:(MainControlTabbarTag)curTag
{
    [_tabBar setTabBarSelected:curTag];
}
-(void)showRootByTag:(MainControlTabbarTag)curTag
{
    YNavigationViewController *willShow = [self buildRootIfNotfind:curTag];
    YNavigationViewController *curShow  = [self getCurrentRootController];
    if (!curShow){
        [self.view addSubview:willShow.view];
    }else{
        if (willShow!=curShow)
        {
            [self.view insertSubview:willShow.view belowSubview:curShow.view];
            [curShow.view removeFromSuperview];
        }
    }
    [willShow showSysTab:self.tabBar];
}

- (void)tabItemClicked:(id)sender withPerSelected:(id)perSender
{
    UIView * senderItem = (UIView*)sender;
    if (sender == perSender)
    {
        //单击当前tab
    }
    [self showRootByTag:(MainControlTabbarTag)senderItem.tag];
}


#pragma -mark rootView

-(NSString*) controllerKeyFromTag:(MainControlTabbarTag)curTag
{
    NSString* key = [NSString stringWithFormat:@"%ud",curTag];
    return key;
}

/**
 *  get Current root Controller
 *
 *  @return  当前的root Controller
 */
-(id)getCurrentRootPage;
{
    return [self getCurrentRootController];
}

- (MainControlTabbarTag)getCurrentTabBarTag
{
    TabBarButton * selectedItem = [_tabBar getSelectedItem];
    if (selectedItem)
    {
        return (MainControlTabbarTag)selectedItem.tag;
    }
    return 0;
}

//当前的root
-(id) getCurrentRootController
{
    MainControlTabbarTag curTag = eMainControlTabbarTag_Resource;
    if (_tabBar){
        curTag =[self getCurrentTabBarTag];
    }
    return [self findRootController:curTag];
}

//查找root
-(id) findRootController:(MainControlTabbarTag)curTag
{
    NSString* key = [self controllerKeyFromTag:curTag];
    if (key && [key length]>0)
    {
        return _rootArray[key];
    }
    return nil;
}

//创建root
-(id) buildRootIfNotfind:(MainControlTabbarTag)curTag
{
    id ctrl = [self findRootController:curTag];
    if (ctrl) {
        return ctrl;
    }
    NSString* key = [self controllerKeyFromTag:curTag];
    ctrl = [self buildRootControllerObj:curTag];
    ctrl = [ctrl createMainNavigationController];
    if (ctrl && key) {
        _rootArray[key] = ctrl;
    }
    return ctrl;
}
//创建root
-(id) buildRootControllerObj:(MainControlTabbarTag)curTag
{
    CGRect rect = self.view.frame;
    rect.origin.x = 0;
    rect.origin.y = 0;
    YBaseViewController* ctrl = nil;
    switch (curTag)
    {
        case eMainControlTabbarTag_Resource:
        {
            ctrl = [[HDResourceViewController alloc] init];
        }
            break;
        case eMainControlTabbarTag_Photo:
        {
            ctrl = [[PhotoViewController alloc]init];
        }
            break;
        case eMainControlTabbarTag_AnswerQuestion:
        {
            ctrl = [[HD_SolveProblemVC alloc] init];
        }
            break;
        case eMainControlTabbarTag_Vote:
        {
            ctrl = [[HD_VoteVCViewController alloc] init];
        }
            break;
        case eMainControlTabbarTag_Name:
        {
            ctrl = [[HD_CallNameVC alloc] init];
        }
            break;
        case eMainControlTabbarTag_AllAnswer:
        {
            ctrl = [[HD_AllMemerAnswerVC alloc] init];
        }
            break;
        case eMainControlTabbarTag_Competition:
        {
            ctrl = [[HD_TeamCompetionVC alloc] init];
        }
            break;
        case eMainControlTabbarTag_Test:
        {
            ctrl = [[HD_OfflineTestVC alloc] init];
        }
            break;
        case eMainControlTabbarTag_Close:
        {
            ctrl = nil;
        }
            break;
        default:
            break;
    }
    return ctrl;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
