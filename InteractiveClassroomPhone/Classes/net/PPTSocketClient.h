//
//  PPTSocketClient.h
//  InteractiveClassroom
//
//  Created by 姚永波 on 2018/4/12.
//  Copyright © 2018年 姚永波. All rights reserved.
//

#import <Foundation/Foundation.h>

#define PPT_PORT 9998

@interface PPTSocketClient : NSObject<NSStreamDelegate>

+ (PPTSocketClient *)shareInstance;

- (void)sendSynData:(NSData *)data;

@end
