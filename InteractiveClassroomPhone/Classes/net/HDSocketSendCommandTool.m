//
//  HDSocketSendCommandTool.m
//  InteractiveClassroom
//
//  Created by 姚永波 on 2018/3/18.
//  Copyright © 2018年 姚永波. All rights reserved.
//

#import "HDSocketSendCommandTool.h"

@implementation HDSocketSendCommandTool

+ (void)sendSocketParam:(NSDictionary *)dic commandCode:(NSNumber *)code successBlock:(void (^)(id result)) myBlock;
{
    NSMutableDictionary *dicM = [NSMutableDictionary dictionaryWithDictionary:dic];
    dicM[@"token"] = [[NSUserDefaults standardUserDefaults] objectForKey:kUseDefaultToken];
    dicM[@"code"] = code;
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dicM
                                                       options:NSJSONWritingPrettyPrinted
                                                         error:&error];
    [[HDClient shareInstance] sendData:jsonData complete:^(id result) {
        myBlock(result);
    }];
}
@end
