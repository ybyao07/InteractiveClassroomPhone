//
//  HDSocketConst.m
//  InteractiveClassroom
//
//  Created by 姚永波 on 2018/3/10.
//  Copyright © 2018年 姚永波. All rights reserved.
//

#import "HDSocketConst.h"

@implementation HDSocketConst

NSString * const kHDNotificationCenterSocket = @"kHDNotificationCenterSocket";

NSString * const kHDNotificationCenterSocketStream = @"kHDNotificationCenterSocketStream"; //单独处理二进制流

NSString * const kHDNotificationCenterSocketPPTStream = @"kHDNotificationCenterSocketPPTStream"; //单独处理PPT大图

//NSString * const kHDuserInfoKey = @"kHDuserInfoKey";


@end
