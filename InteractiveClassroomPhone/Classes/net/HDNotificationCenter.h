//
//  HDNotificationCenter.h
//  InteractiveClassroom
//
//  Created by 姚永波 on 2018/3/10.
//  Copyright © 2018年 姚永波. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HDNotificationCenter : NSObject

/** 与互联网断开连接 */
extern NSString * const kUseDisconnectInternet;
/** 使用 WiFi 连接互联网 */
extern NSString * const kUseWiFiConnectInternet;

/** 使用移动蜂窝数据连接互联网 */
extern NSString * const kUseMobileNetworkConnectInternet;


extern NSString * const kUseWifiInternetStatus;

extern NSString * const kOpenOrCloseViewNotification;
extern NSString * const kMainClassOpenOrCloseViewNotification;
extern NSString * const kLinkNotification;
extern NSString * const kCloseVoteNotification;

extern NSString * const kUseDefaultIP;
extern NSString * const kUseDefaultPort;
extern NSString * const kUseDefaultToken;

@end
