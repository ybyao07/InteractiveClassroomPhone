//
//  HDSocketSendCommandTool.h
//  InteractiveClassroom
//
//  Created by 姚永波 on 2018/3/18.
//  Copyright © 2018年 姚永波. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HDSocketSendCommandTool : NSObject

+ (void)sendSocketParam:(NSDictionary *)dic commandCode:(NSNumber *)code successBlock:(void (^)(id result)) myBlock;

@end
