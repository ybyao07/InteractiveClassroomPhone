//
//  HDNetTools.m
//  InteractiveClassroom
//
//  Created by 姚永波 on 2018/3/11.
//  Copyright © 2018年 姚永波. All rights reserved.
//

#import "HDNetTools.h"
#import <sys/socket.h>
#import <sys/sockio.h>
#import <sys/ioctl.h>
#import <net/if.h>
#import <arpa/inet.h>
#include <ifaddrs.h>
#import <SystemConfiguration/CaptiveNetwork.h>
#import "wwanconnect.h"
#import "Reachability.h"

@implementation HDNetTools

+ (NSString *)wifiName{
    NSString *wifiName = nil;
    CFArrayRef wifiInterfaces =CNCopySupportedInterfaces();
    if (!wifiInterfaces) {
        return nil;
    }
    NSArray *interfaces = (__bridge NSArray *)wifiInterfaces;
    for (NSString *interfaceName in interfaces) {
        CFDictionaryRef dictRef =CNCopyCurrentNetworkInfo((__bridge CFStringRef)(interfaceName));
        if (dictRef) {
            NSDictionary *networkInfo = (__bridge NSDictionary *)dictRef;
            wifiName = [networkInfo objectForKey:(__bridge NSString *)kCNNetworkInfoKeySSID];
            CFRelease(dictRef);
        }
    }
    CFRelease(wifiInterfaces);
    return wifiName;
}

+ (NSString *) hostname{
    char baseHostName[255];
    int success = gethostname(baseHostName, 255);
    if (success != 0) return nil;
    baseHostName[255] = '\0';
#if !TARGET_IPHONE_SIMULATOR
    return [NSString stringWithFormat:@"%s.local", baseHostName];
#else
    return [NSString stringWithFormat:@"%s", baseHostName];
#endif
}


// Direct from Apple. Thank you Apple

+ (BOOL)addressFromString:(NSString *)IPAddress address:(struct sockaddr_in *)address{
    if (!IPAddress || ![IPAddress length]) {
        return NO;
    }
    memset((char *) address,sizeof(struct sockaddr_in), 0);
    address->sin_family = AF_INET;
    address->sin_len = sizeof(struct sockaddr_in);
    int conversionResult = inet_aton([IPAddress UTF8String], &address->sin_addr);
    if (conversionResult == 0) {
        NSAssert1(conversionResult != 1, @"Failed to convert the IP address string into a sockaddr_in: %@", IPAddress);
        return NO;
    }
    return YES;
}

+ (BOOL) activeWLAN{
    return ([self localWiFiIPAddress] != nil);
}

+ (NSString *)getDeviceIPIpAddresses
{
    int sockfd = socket(AF_INET, SOCK_DGRAM, 0);
    if (sockfd < 0) return nil;
    NSMutableArray *ips = [NSMutableArray array];
    
    int BUFFERSIZE = 4096;
    struct ifconf ifc;
    char buffer[BUFFERSIZE], *ptr, lastname[IFNAMSIZ], *cptr;
    struct ifreq *ifr, ifrcopy;
    ifc.ifc_len = BUFFERSIZE;
    ifc.ifc_buf = buffer;
    if (ioctl(sockfd, SIOCGIFCONF, &ifc) >= 0){
        for (ptr = buffer; ptr < buffer + ifc.ifc_len; ){
            ifr = (struct ifreq *)ptr;
            int len = sizeof(struct sockaddr);
            if (ifr->ifr_addr.sa_len > len) {
                len = ifr->ifr_addr.sa_len;
            }
            ptr += sizeof(ifr->ifr_name) + len;
            if (ifr->ifr_addr.sa_family != AF_INET) continue;
            if ((cptr = (char *)strchr(ifr->ifr_name, ':')) != NULL) *cptr = 0;
            if (strncmp(lastname, ifr->ifr_name, IFNAMSIZ) == 0) continue;
            memcpy(lastname, ifr->ifr_name, IFNAMSIZ);
            ifrcopy = *ifr;
            ioctl(sockfd, SIOCGIFFLAGS, &ifrcopy);
            if ((ifrcopy.ifr_flags & IFF_UP) == 0) continue;
            
            NSString *ip = [NSString stringWithFormat:@"%s", inet_ntoa(((struct sockaddr_in *)&ifr->ifr_addr)->sin_addr)];
            [ips addObject:ip];
        }
    }
    close(sockfd);
    NSString *address=[ips lastObject];
    NSLog(@"deviceIP========%@",address);
    return address;
}

+ (NSString *)localWiFiIPAddress
{
    NSString *address = @"an error occurred when obtaining ip address";
    struct ifaddrs *interfaces = NULL;
    struct ifaddrs *temp_addr = NULL;
    int success = 0;
    success = getifaddrs(&interfaces);
    if (success == 0) { // 0 表示获取成功
        temp_addr = interfaces;
        while (temp_addr != NULL) {
            if( temp_addr->ifa_addr->sa_family == AF_INET) {
                // 判断是否是wifi连接
                if ([[NSString stringWithUTF8String:temp_addr->ifa_name] isEqualToString:@"en0"]) {
                    // C String 转 NSString
                    address = [NSString stringWithUTF8String:inet_ntoa(((struct sockaddr_in *)temp_addr->ifa_addr)->sin_addr)];
                }
            }
            temp_addr = temp_addr->ifa_next;
        }
    }
    freeifaddrs(interfaces);
    return address;
}


+(void)startNetWrokWithURL:(NSString *)url
{
    Reachability *reach = [Reachability reachabilityWithHostname:url];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reachabilityChanged:) name:kReachabilityChangedNotification object:nil];
    [reach startNotifier];
}

+ (void) reachabilityChanged: (NSNotification*)note {
    Reachability * reach = [note object];
    if(![reach isReachable])
    {
        NSDictionary *dicWifi = @{@"isWifi":@(0)};
        [[NSNotificationCenter defaultCenter] postNotificationName:kUseWifiInternetStatus object:nil userInfo:dicWifi];
        return;
    }
    if (reach.isReachableViaWiFi) {
        NSLog(@"~~~~~~~~~~~~~RealStatusViaWiFi");
        NSDictionary *dicWifi = @{@"isWifi":@(1)};
        [[NSNotificationCenter defaultCenter] postNotificationName:kUseWifiInternetStatus object:nil userInfo:dicWifi];
        return;
    }
    
    if (reach.isReachableViaWWAN) {
        NSLog(@"~~~~~~~~~~~~~RealStatusViaWWAN 流量上网");
        NSDictionary *dicWifi = @{@"isWifi":@(0)};
        [[NSNotificationCenter defaultCenter] postNotificationName:kUseWifiInternetStatus object:nil userInfo:dicWifi];
        return;
    }
}

+ (BOOL)isNotConnectNetWork
{
    return ([[Reachability reachabilityForLocalWiFi] currentReachabilityStatus] == NotReachable);
}

+ (BOOL)isConnectWIFI
{
    return ([[Reachability reachabilityForLocalWiFi] currentReachabilityStatus] == ReachableViaWiFi);
}

+ (BOOL)isConnect3G4G
{
    return ([[Reachability reachabilityForInternetConnection] currentReachabilityStatus] == ReachableViaWWAN);
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}



@end
