//
//  HDSocketService.m
//  InteractiveClassroom
//
//  Created by 姚永波 on 2018/3/10.
//  Copyright © 2018年 姚永波. All rights reserved.
//

#import "HDSocketService.h"

@interface HDSocketService()<GCDAsyncSocketDelegate>
{
    long needDataLen;
    long readLen;
}
@property (strong, nonatomic) GCDAsyncSocket *socket;
@property (nonatomic, retain) NSMutableData *receiveData;
@property (nonatomic, strong) NSMutableArray *clientSockets;

@end

@implementation HDSocketService


static HDSocketService *__servie = nil;

HDSocketService *getService(void){
    return __servie;
}

- (instancetype)init
{
    if (self = [super init]) {
        //创建全局queue
        //创建服务端的socket，注意这里的是初始化的同时已经指定了delegate
        //1.创建scoket对象
        __servie = self;
        _socket = [[GCDAsyncSocket alloc] initWithDelegate:self delegateQueue:dispatch_get_main_queue()];
    }
    return self;
}

- (void)start
{
    //端口任意，但遵循有效端口原则范围：0~65535，其中0~1024由系统使用或者保留端口，开发中建议使用1024以上的端口
    NSError *error = nil;
    [_socket acceptOnPort:9999 error:&error];
    //3.开启服务(实质第二步绑定端口的同时默认开启服务)
    if (error == nil){
        NSLog(@"开启成功");
    }else{
        NSLog(@"开启失败");
    }
}
- (void)stop
{
    if (_socket != nil) {
        [_socket disconnect];
        _socket = nil;
    }
}

#pragma mark GCDAsyncSocketDelegate
//连接到服务器端socket
- (void)socket:(GCDAsyncSocket *)sock didAcceptNewSocket:(GCDAsyncSocket *)newSocket
{
    //sock 服务端的socket
    //newSocket 客户端连接的socket
    NSLog(@"======== 新建Socket==");
    // 1.保存客户端的socket
    [self.clientSockets addObject:newSocket];
    [newSocket readDataWithTimeout:-1 tag:0];
}

//接收到服务器端数据
- (void)socket:(GCDAsyncSocket *)sock didReadData:(NSData *)data withTag:(long)tag
{
//    HDLog(@"- 原始Data数据 - :%@",[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]);
    if (_receiveData == nil) {
        _receiveData = [[NSMutableData alloc] init];
        Byte *dataByte = (Byte *)[data bytes];
        Byte firstFourByte[4];
        memcpy(&firstFourByte, &dataByte[0], 4);
        long dataLen  =  [self heightBytesToInt:firstFourByte]; //单个包的大小
        needDataLen = dataLen;
        if (data.length > needDataLen + 4) {
            [_receiveData appendData:[data subdataWithRange:NSMakeRange(4, needDataLen)]];
            // 一次性读取完
            NSData *tmpData = [_receiveData copy];
            // PostNotification
            NSData *pptData = [tmpData subdataWithRange:NSMakeRange(0,6)];
            NSString *pptStr = [[NSString alloc] initWithData:pptData encoding:NSUTF8StringEncoding];
            if ([pptStr isEqualToString:@"pptinf"]){
                NSDictionary *resultDic = @{@"data":_receiveData};
                [[NSNotificationCenter defaultCenter] postNotificationName:kHDNotificationCenterSocketStream object:nil userInfo:resultDic];
            }else if ([pptStr isEqualToString:@"pptpic"]){
                NSDictionary *resultDic = @{@"data":_receiveData};
                [[NSNotificationCenter defaultCenter] postNotificationName:kHDNotificationCenterSocketPPTStream object:nil userInfo:resultDic];
            }else{
                NSDictionary *jsonDict = [NSJSONSerialization JSONObjectWithData:tmpData options:NSJSONReadingMutableLeaves error:nil];
                HDLog(@"=======Json Message :%@",[[NSString alloc] initWithData:tmpData encoding:NSUTF8StringEncoding]);
                [[NSNotificationCenter defaultCenter] postNotificationName:kHDNotificationCenterSocket object:nil userInfo:jsonDict];
            }
            _receiveData = nil;
        }else{
            [_receiveData appendData:[data subdataWithRange:NSMakeRange(4, data.length - 4)]];
            readLen = data.length - 4;
            needDataLen = needDataLen - readLen;
            if (needDataLen == 0) {
                //一次Post一个完整的包
                NSData *tmpData = [_receiveData copy];
                NSData *pptData = [tmpData subdataWithRange:NSMakeRange(0,6)];
                NSString *pptStr = [[NSString alloc] initWithData:pptData encoding:NSUTF8StringEncoding];
                if ([pptStr isEqualToString:@"pptinf"]){
                    NSDictionary *resultDic = @{@"data":_receiveData};
                    [[NSNotificationCenter defaultCenter] postNotificationName:kHDNotificationCenterSocketStream object:nil userInfo:resultDic];
                }else if ([pptStr isEqualToString:@"pptpic"]){
                    NSDictionary *resultDic = @{@"data":_receiveData};
                    [[NSNotificationCenter defaultCenter] postNotificationName:kHDNotificationCenterSocketPPTStream object:nil userInfo:resultDic];
                }else{
                    NSDictionary *jsonDict = [NSJSONSerialization JSONObjectWithData:tmpData options:NSJSONReadingMutableLeaves error:nil];
                    HDLog(@"=======Json Message :%@",[[NSString alloc] initWithData:tmpData encoding:NSUTF8StringEncoding]);
                    [[NSNotificationCenter defaultCenter] postNotificationName:kHDNotificationCenterSocket object:nil userInfo:jsonDict];
                }
                _receiveData = nil;
            }
        }
    }else{
        if(needDataLen > 0){
            [_receiveData appendData:data];
            readLen = data.length;
            needDataLen = needDataLen - readLen;
            if (needDataLen == 0) {
                //一次Post一个完整的包
                NSData *tmpData = [_receiveData copy];
                NSData *pptData = [tmpData subdataWithRange:NSMakeRange(0,6)];
                NSString *pptStr = [[NSString alloc] initWithData:pptData encoding:NSUTF8StringEncoding];
                if ([pptStr isEqualToString:@"pptinf"]){
                    NSDictionary *resultDic = @{@"data":_receiveData};
                    [[NSNotificationCenter defaultCenter] postNotificationName:kHDNotificationCenterSocketStream object:nil userInfo:resultDic];
                }else if ([pptStr isEqualToString:@"pptpic"]){
                    NSDictionary *resultDic = @{@"data":_receiveData};
                    [[NSNotificationCenter defaultCenter] postNotificationName:kHDNotificationCenterSocketPPTStream object:nil userInfo:resultDic];
                }else{
                    NSDictionary *jsonDict = [NSJSONSerialization JSONObjectWithData:tmpData options:NSJSONReadingMutableLeaves error:nil];
                    HDLog(@"=======Json Message :%@",[[NSString alloc] initWithData:tmpData encoding:NSUTF8StringEncoding]);
                    [[NSNotificationCenter defaultCenter] postNotificationName:kHDNotificationCenterSocket object:nil userInfo:jsonDict];
                }
                _receiveData = nil;
            }
        }else{//如果结束则
            //一次Post一个完整的包
            NSData *tmpData = [_receiveData copy];
            NSData *pptData = [tmpData subdataWithRange:NSMakeRange(0,6)];
            NSString *pptStr = [[NSString alloc] initWithData:pptData encoding:NSUTF8StringEncoding];
            if ([pptStr isEqualToString:@"pptinf"]){
                NSDictionary *resultDic = @{@"data":_receiveData};
                [[NSNotificationCenter defaultCenter] postNotificationName:kHDNotificationCenterSocketStream object:nil userInfo:resultDic];
            }else if ([pptStr isEqualToString:@"pptpic"]){
                NSDictionary *resultDic = @{@"data":_receiveData};
                [[NSNotificationCenter defaultCenter] postNotificationName:kHDNotificationCenterSocketPPTStream object:nil userInfo:resultDic];
            }else{
                NSDictionary *jsonDict = [NSJSONSerialization JSONObjectWithData:tmpData options:NSJSONReadingMutableLeaves error:nil];
                HDLog(@"=======Json Message :%@",[[NSString alloc] initWithData:tmpData encoding:NSUTF8StringEncoding]);
                [[NSNotificationCenter defaultCenter] postNotificationName:kHDNotificationCenterSocket object:nil userInfo:jsonDict];
            }
            _receiveData = nil;
        }
    }
    //CocoaAsyncSocket每次读取完成后必须调用一次监听数据方法
    [sock readDataWithTimeout:-1 tag:0];
}
-(long)heightBytesToInt:(Byte[]) byte
{
    long one = byte[0]&0xFF;
    long two = byte[1]&0xFF;  // 03
    long twoHext = two * 16 * 16;
    
    long three = byte[2]&0xFF;
    long threeHex = three * 16 * 16 * 16;
    
    long four = byte[3]&0xFF;
    long fourHex = four * 16 * 16 * 16 * 16;
    long total = fourHex + threeHex +  twoHext + one;
    return total;
}


- (void)socket:(GCDAsyncSocket *)sock didWriteDataWithTag:(long)tag
{
    
}
- (void)socket:(GCDAsyncSocket *)sock didConnectToUrl:(NSURL *)url
{
    
}
- (void)socket:(GCDAsyncSocket *)sock didConnectToHost:(NSString *)host port:(uint16_t)port
{
    
}
- (void)socketDidDisconnect:(GCDAsyncSocket *)sock withError:(nullable NSError *)err
{
    NSLog(@"socketDidDisconnect port:%d:%@",[sock localPort],err);
}

- (NSMutableArray *)clientSockets
{
    if (!_clientSockets) {
        _clientSockets = [NSMutableArray array];
    }
    return _clientSockets;
}


@end
