//
//  HDClient.m
//  InteractiveClassroom
//
//  Created by 姚永波 on 2018/3/10.
//  Copyright © 2018年 姚永波. All rights reserved.
//

#import "HDClient.h"
#import <sys/socket.h>
#import <netinet/in.h>
#import <arpa/inet.h>

@interface HDClient()
{
    NSOutputStream *_outputStream;//对应输出流
}
@end

@implementation HDClient

+ (HDClient *)shareInstance
{
    static HDClient *client = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        client = [[HDClient alloc] init];
    });
    return client;
}
//#pragma mark === 建立连接 ==
//- (void)createConnection
//{
//    self.socket = [[GCDAsyncSocket alloc] initWithDelegate:self delegateQueue:dispatch_get_main_queue()];
//    NSError *error =nil;
//    [self.socket connectToHost:IP onPort:PORT error:&error];
//    if(error) {
//        NSLog(@"连接错误%@", error);
//        return;
//    }
//    [self.socket readDataWithTimeout:-1 tag:0];
//}
// 发送数据
- (void)sendMessage:(NSString *)str complete:(BLOCK)block {
    HDLog(@"===== 发送的指令为======%@",str);
    self.block = block;
    [self.socket writeData:[str dataUsingEncoding:NSUTF8StringEncoding] withTimeout:-1 tag:0];
    [self.socket readDataWithTimeout:-1 tag:0];
}
// 发送字节
- (void)sendData:(NSData *)data complete:(BLOCK)block{
    HDLog(@"sendMessage:%@",[NSJSONSerialization JSONObjectWithData:data options:0 error:nil]);
    self.socket = [[GCDAsyncSocket alloc] initWithDelegate:self delegateQueue:dispatch_get_main_queue()];
    NSError *error =nil;
    [self.socket connectToHost:[[NSUserDefaults standardUserDefaults] objectForKey:kUseDefaultIP] onPort:[[[NSUserDefaults standardUserDefaults] objectForKey:kUseDefaultPort] integerValue] error:&error];
    if(error) {
        NSLog(@"连接错误%@", error);
        return;
    }
    self.block = block;
    dispatch_queue_t queue=dispatch_queue_create("socketPhoto", DISPATCH_QUEUE_SERIAL);
    dispatch_block_t task1=^{
        [self.socket writeData:data withTimeout:-1 tag:0];
    };
    //同步的方式 执行队列
    dispatch_sync(queue, task1);
    [self.socket readDataWithTimeout:-1 tag:0];
}


#pragma mark =======  GCDAsyncSocketDelegate =====
// tcp连接
- (void)socket:(GCDAsyncSocket *)sock didConnectToHost:(NSString *)host port:(uint16_t)port {
    NSLog(@"连接成功");
    [self.socket readDataWithTimeout:-1 tag:0];
//    [self.socket disconnect];
}

// 服务器返回数据
- (void)socket:(GCDAsyncSocket*)sock didReadData:(NSData *)data withTag:(long)tag {
    NSString *str = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    NSLog(@"str = %@", str);
    self.block(str);
    [self.socket readDataWithTimeout:-1 tag:0];
}


- (void)socket:(GCDAsyncSocket *)sock didWriteDataWithTag:(long)tag
{
    NSLog(@"didWriteDataWithTag");
    [self.socket disconnect];
}
- (void)socketDidDisconnect:(GCDAsyncSocket *)sock withError:(NSError *)err {
    NSLog(@"已经断开连接!");
    [self.socket readDataWithTimeout:-1 tag:0];
}

#pragma mark =======  同步发送 =====
- (void)sendSynData:(NSData *)data
{
//    if (_outputStream == nil) {
        //1.建立连接
        NSString *host = [[NSUserDefaults standardUserDefaults] objectForKey:kUseDefaultIP];
        int port = (int)[[[NSUserDefaults standardUserDefaults] objectForKey:kUseDefaultPort] integerValue];
        CFReadStreamRef readStream;
        CFWriteStreamRef writeStream;
        CFStreamCreatePairWithSocketToHost(NULL, (__bridge CFStringRef)host, port, &readStream, &writeStream);
        _outputStream = (__bridge NSOutputStream *)(writeStream);
        _outputStream.delegate=self;
        [_outputStream scheduleInRunLoop:[NSRunLoop mainRunLoop] forMode:NSDefaultRunLoopMode];
        [_outputStream open];
//    }
    [_outputStream write:data.bytes maxLength:data.length];
}

-(void)stream:(NSStream *)aStream handleEvent:(NSStreamEvent)eventCode{
    NSLog(@"%@",[NSThread currentThread]);
    //NSStreamEventOpenCompleted = 1UL << 0,//输入输出流打开完成//NSStreamEventHasBytesAvailable = 1UL << 1,//有字节可读//NSStreamEventHasSpaceAvailable = 1UL << 2,//可以发放字节//NSStreamEventErrorOccurred = 1UL << 3,//连接出现错误//NSStreamEventEndEncountered = 1UL << 4//连接结束
    switch(eventCode) {
        case NSStreamEventOpenCompleted:
            HDLog(@"输入输出流打开完成");
            break;
        case NSStreamEventHasBytesAvailable:
            HDLog(@"有字节可读");
            //            [self readData];
            break;
        case NSStreamEventHasSpaceAvailable:
            HDLog(@"可以发送字节");
            break;
        case NSStreamEventErrorOccurred:
            HDLog(@"连接出现错误");
            break;
        case NSStreamEventEndEncountered:
        {
            HDLog(@"连接结束");
            [aStream close];
            aStream = nil;
            //从主运行循环移除
            [aStream removeFromRunLoop:[NSRunLoop mainRunLoop] forMode:NSDefaultRunLoopMode];
        }
            break;
        default:
            break;
    }
}

@end
