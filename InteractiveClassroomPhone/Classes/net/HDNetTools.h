//
//  HDNetTools.h
//  InteractiveClassroom
//
//  Created by 姚永波 on 2018/3/11.
//  Copyright © 2018年 姚永波. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HDNetTools : NSObject

+ (BOOL)activeWLAN;
/// 连接的wifi名字
+ (NSString *) wifiName;
// 公网IP地址
+ (NSString *)getDeviceIPIpAddresses;
// 局域网IP地址
+ (NSString *)localWiFiIPAddress;


/**
 *  开始监控网络连接方式改变
 *
 *  @param url 传入监控的URL
 */
+ (void) startNetWrokWithURL:(NSString *) url;

/**
 *  是否连接上互联网
 *
 *  @return YES：未连接  NO：连接上
 */
+ (BOOL)isNotConnectNetWork;

/**
 *  是否WIFI
 */
+ (BOOL)isConnectWIFI;

/**
 *  是否3G/4G
 */
+ (BOOL)isConnect3G4G;




@end
