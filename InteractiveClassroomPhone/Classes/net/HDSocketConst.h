//
//  HDSocketConst.h
//  InteractiveClassroom
//
//  Created by 姚永波 on 2018/3/10.
//  Copyright © 2018年 姚永波. All rights reserved.
//

#import <Foundation/Foundation.h>


typedef NS_OPTIONS(NSInteger, HDSocketCommandType) {
    ScanCode = 1,                   //扫码
    Connected = 2,                  //移动端连接上授课端
    Material = 3,                   //资源UI
    MaterialList = 4,               //资源列表
    OpenMaterial = 5,               //打开资源
    MaterialInfo = 6,               //资源信息
    OpenSign = 7,                   //打开标注
    CloseSign = 8,                  //关闭标注
    SignSettings = 9,               //标注笔和橡皮设置
    SignOperation = 10,             //标注操作
    SignDrawLine = 11,              //批注画线
    Photo = 12,                     //拍照图片
    ClassroomTest = 13,             //随堂测UI
    ClassroomTestList = 14,         //随堂测列表
    ClassroomTestOperation = 15,    //随堂测操作
    QuickAnswer = 16,               //全员抢答
    StartQuickAnswer = 17,          //开始全员抢答
    QuickAnswerState = 18,          //全员抢答状态
    QuickAnswerPraise = 19,         //全员抢答表扬
    Vote = 20,                      //投票
    VoteList = 21,                  //投票列表
    VoteDetails = 22,               //投票详情
    VoteOption = 23,                //投票选项
    StartVote = 24,                 //开始选项
    VoteCountDetails = 25,          //投票人数详情
    VoteResult = 26,                //返回投票结果
    VoteClose = 27,                 //退出投票界面
    CreateVote = 28,                //新建投票
    CreateVoteResponse = 29,        //新建投票是否成功
    RollCall = 30,                  //点名
    StudentsList = 31,              //学生列表
    SelectedStudent = 32,           //点名选中学生
    RaiseHandsStudentInfo = 33,     //返回举手点名学生信息
    RollCallPraise = 34,            //点名表扬
    StartRandom = 35,               //开始随机点名
    GroupCompetition = 36,          //团队竞猜
    GroupsInfo = 37,                //团队信息
    CurrentGroupInfo = 38,          //当前团队信息
    StartGroupAnswer = 39,          //开始团队答题
    GroupAnswerState = 40,          //团队答题状态
    AddStar = 41,                   //加星值
    GroupAnswerPraise = 42,         //团队抢答表扬
    GroupAnswerRanking = 43,        //团队抢答排行榜
    GroupAnswerRankingInfo = 44,    //团队抢答排行榜信息
    CloseGroupAnswer = 45,          //关闭团队抢答
    TestPaper = 46,                 //线下测评
    TestPaperList = 47,             //线下测评列表
    StartTestPaper = 48,            //开始线下测评
    TestPaperProgress = 49,         //测评进度
    TestPaperDetails = 50,          //登分详情
    TestPaperDetailsList = 51,      //登分详情
    CloseTestPaperDetails = 52,     //关闭登分详情
    EndTestPaper = 53,              //结束登分
    TestPaperResult = 54,           //登分结果
    ContinueTestPaper = 55,         //继续登分
    SaveTestPaper = 56,             //结束并保存登分
    CloseConnect = 57,              //断开连接
    ChangePage = 58,                //PPT翻页
    VoteEnd = 59,                   //结束投票
    StartRaiseHands = 60,           //开始举手答题
    OpenClassroomTest = 61,         //打开题目
    ClassroomTestInfo = 62,         //题目信息
    AnswerAnalyses = 63,            //答题分析
    AnswerResult = 64,              //答题结果
    SwitchNextQuestion = 65,         //切换下一题
    

    ClassroomTestOperationCastScreen = 66, //投屏获取题目 --- 请求数据 -----  ClassroomTestOperation 真正投屏
    Praise = 67,
    CloseWindow = 68,         //关闭抢答、点名、投票界面
    CloseUI = 69,          //关闭我的资源页面 -- 拍照界面 - 团队抢答  - 测评
    
    TestPaperDetailsCastScreen = 70,        //----- 登分详情手机---- TestPaperDetails ---
    RefreshTestPaperProgress = 71,          //刷新登分详情
    StartVoteSuccess = 72,                   //开始投票成功
    ReturnGroupAnswer = 73

};

@interface HDSocketConst : NSObject

extern NSString * const kHDNotificationCenterSocket; //监听的Socket收到通知
extern NSString * const kHDNotificationCenterSocketStream;
extern NSString * const kHDNotificationCenterSocketPPTStream;
@end
