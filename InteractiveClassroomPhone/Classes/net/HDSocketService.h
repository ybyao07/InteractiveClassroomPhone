//
//  HDSocketService.h
//  InteractiveClassroom
//
//  Created by 姚永波 on 2018/3/10.
//  Copyright © 2018年 姚永波. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GCDAsyncSocket.h"

@interface HDSocketService : NSObject

HDSocketService *getService(void);

- (void)start;
- (void)stop;

@end
