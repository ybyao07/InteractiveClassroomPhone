//
//  YBaseViewController.h
//  InteractiveClassroom
//
//  Created by 姚永波 on 2018/2/10.
//  Copyright © 2018年 姚永波. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YNavigationViewController.h"
#import "JXNavigationBar.h"
#import "MBProgressHUD.h"


#define gNavigationbarHeightOnly        (44)     //导航栏
#define gNavigationbarHeight            ([UIApplication sharedApplication].statusBarFrame.size.height+44)  //导航

@interface YBaseViewController : UIViewController

@property (strong,nonnull,nonatomic) JXNavigationBar *hd_navigationBar;
@property (strong, nonatomic) MBProgressHUD *progressHUD;

/**
 *  添加自定义导航栏，子类复写后_jx_navigationBar将不可用
 */
- (void)addNavigationBar;
/**
 *  添加自定义导航条左边按钮，子类复写后JXNavigationBar的leftButton将不可用
 */
- (void)addLeftButtons;
/**
 *  添加自定义导航条右边按钮，子类复写后JXNavigationBar的rightButton将不可用
 */
- (void)addRightButtons;
/**
 *  添加自定义titleView，子类复写后JXNavigationBar的titleView将不可用
 */
- (void)addTitleView;

- (void)addTitleLabel;

/**
 *  create Main Navigation Controller
 *
 *  @return  one new Main Navigation
 */
- (YNavigationViewController * _Nullable) createMainNavigationController;

/**
 *  get Current root Controller
 *
 *  @return  当前的root Controller
 */
-(id _Nullable) getCurrentRootPage;


- (void)showLoadAnimation;

- (void)showLoadAnimationWithTip:(NSString *)tipStr;

- (void)removeLoadAnimation;




@end
