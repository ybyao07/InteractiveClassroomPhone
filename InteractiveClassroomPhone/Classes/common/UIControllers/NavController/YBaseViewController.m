//
//  YBaseViewController.m
//  InteractiveClassroom
//
//  Created by 姚永波 on 2018/2/10.
//  Copyright © 2018年 姚永波. All rights reserved.
//

#import "YBaseViewController.h"
#import "Masonry.h"

@interface YBaseViewController ()

@end

@implementation YBaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.automaticallyAdjustsScrollViewInsets = NO;
    [self addNavigationBar];
    self.view.backgroundColor = JUIColorFromRGB_Major2;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
    self.navigationController.navigationBar.hidden = YES;
}

- (void)addNavigationBar{
    _hd_navigationBar = [[JXNavigationBar alloc] initWithFrame:CGRectZero];
    [self.view addSubview:_hd_navigationBar];
    __weak typeof(self) weakSelf = self;
    [_hd_navigationBar mas_makeConstraints:^(MASConstraintMaker *make) {
        __strong typeof(weakSelf) strongSelf = weakSelf;
        make.left.equalTo(strongSelf.view.mas_left).offset(0);
        make.top.equalTo(strongSelf.view.mas_top).offset(0);
        make.right.equalTo(strongSelf.view.mas_right);
        make.height.mas_equalTo(64);
    }];
    [self addTitleView];
    [self addTitleLabel];
    [self addLeftButtons];
    [self addRightButtons];
    [self addGoBackBtn];
}

- (void)addLeftButtons{
    [_hd_navigationBar setupLeftButton];
}

- (void)addRightButtons{
    [_hd_navigationBar setupRightButton];
}

- (void)addTitleView{
    [_hd_navigationBar setupTitleView];
}

- (void)addTitleLabel{
    if (!_hd_navigationBar.titleView) {
        return;
    }
    [_hd_navigationBar setupTitleLabel];
}

- (void)addGoBackBtn {
//    if ([self isCanBack]) {
        [_hd_navigationBar.leftButton setImage:[UIImage imageNamed:@"jxNav_left_back"] forState:UIControlStateNormal];
        [_hd_navigationBar.leftButton addTarget:self action:@selector(goBackPage) forControlEvents:UIControlEventTouchUpInside];
//    }
}

//- (BOOL)isCanBack {
//    if (self.navigationController&&[self.navigationController isKindOfClass:[YNavigationViewController class]]) {
//        return self.navigationController != nil && [[self.navigationController viewControllers] count]>1;
//    }
//    return NO;
//}


- (void)goBackPage {
    if ([self.navigationController isKindOfClass:[YNavigationViewController class]]) {
        NSArray *pages = [self.navigationController viewControllers];
        if ([pages count] > 1) {
            [self.navigationController popViewControllerAnimated:YES];
        }
        else {
            [self dismissViewControllerAnimated:YES
                                     completion:^{
                                     }];
        }
    }
}

- (void)setTitle:(NSString *)title{
    self.hd_navigationBar.title = title;
}


-(id) getCurrentRootPage;
{
    return self;
}


/**
 *  create Main Navigation Controller
 *
 *  @return  one new Main Navigation
 */
- (YNavigationViewController*) createMainNavigationController
{
    return [YBaseViewController createMainNavigationController:self];
}

/**
 *  create Main Navigation Controller
 *
 *  @return  one new Main Navigation
 */
+(YNavigationViewController *) createMainNavigationController:(UIViewController*)controll
{
    if (!controll) {
        return nil;
    }
    YNavigationViewController* navController   = [[YNavigationViewController alloc] initWithRootViewController:controll];
    navController.navigationBar.translucent = NO;
    navController.navigationBar.barTintColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"indexbj1"]];
    navController.navigationBar.titleTextAttributes = @{NSFontAttributeName:[UIFont boldSystemFontOfSize:20], NSForegroundColorAttributeName:JUIColorFromRGB_White};
    return navController;
}

- (void)showLoadAnimation {
    [self removeLoadAnimation];
    _progressHUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    _progressHUD.bezelView.backgroundColor = [UIColor blackColor];
    _progressHUD.contentColor = [UIColor whiteColor];
    _progressHUD.label.text = NSLocalizedString(@"请求数据中...", @"请求数据中...");
    _progressHUD.label.textColor = [UIColor whiteColor];
    _progressHUD.label.font = kFont16;
}

- (void)showLoadAnimationWithTip:(NSString *)tipStr {
    [self removeLoadAnimation];
    _progressHUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    _progressHUD.bezelView.backgroundColor = [UIColor blackColor];
    _progressHUD.contentColor = [UIColor whiteColor];
    _progressHUD.label.text = tipStr;
    _progressHUD.label.textColor = [UIColor whiteColor];
    _progressHUD.label.font = kFont16;
}

- (void)removeLoadAnimation {
    if (_progressHUD) {
        [UIView animateWithDuration:1.5
                         animations:^{
                             _progressHUD.alpha = 0;
                             [_progressHUD removeFromSuperview];
                         }];
        _progressHUD = nil;
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
