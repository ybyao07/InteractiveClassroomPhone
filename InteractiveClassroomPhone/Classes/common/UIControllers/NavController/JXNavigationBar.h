//
//  JXNavigationBar.h
//  jiuxian
//
//  Created by 万昌军 on 16/1/26.
//  Copyright © 2016年 jiuxian.com. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@interface JXNavigationBar : UIView

@property (strong,nullable,nonatomic) UIButton *leftButton;

@property (strong,nullable,nonatomic) UIButton *rightButton;

@property (strong,nullable,nonatomic) UIView *titleView;

@property (strong,nullable,nonatomic) NSString *title;

@property (strong,nullable,nonatomic) UILabel *titleLabel;

@property (strong,nullable,nonatomic) UIView *separationLineView;

- (void)setupLeftButton;

- (void)setupRightButton;

- (void)setupTitleView;

- (void)setupTitleLabel;

- (void)resetNavigationBar;



@end
NS_ASSUME_NONNULL_END
