//
//  JXNavigationBar+JXAdditions.m
//  jiuxian
//
//  Created by 万昌军 on 16/1/27.
//  Copyright © 2016年 jiuxian.com. All rights reserved.
//

#import "JXNavigationBar+JXAdditions.h"
#import <objc/runtime.h>

@implementation JXNavigationBar (JXAdditions)

static char overlayKey;

- (UIView *)overlay {
    return objc_getAssociatedObject(self, &overlayKey);
}

- (void)setOverlay:(UIView *)overlay {
    objc_setAssociatedObject(self, &overlayKey, overlay, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (void)jx_setBackgroundColor:(UIColor *)backgroundColor {
    if (!self.overlay) {
        self.overlay = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, CGRectGetHeight(self.bounds))];
        self.overlay.userInteractionEnabled = NO;
        self.overlay.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        [self insertSubview:self.overlay atIndex:0];
    }
    self.overlay.backgroundColor = backgroundColor;
}

- (void)jx_setTranslationY:(CGFloat)translationY {
    self.transform = CGAffineTransformMakeTranslation(0, translationY);
}

- (void)jx_reset {
    [self.overlay removeFromSuperview];
    self.overlay = nil;
}


@end
