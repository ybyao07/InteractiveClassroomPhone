//
//  TabBarButton.m
//  PgtComm
//
//  Created by jeffery zhao on 2017/7/25.
//  Copyright © 2017年 Pugutang. All rights reserved.
//

#import "TabBarButton.h"
#import "ConstDef.h"

@interface TabBarButton()

@property (nonatomic, strong) NSString *normal, *selected, *highlight;

@end

@implementation TabBarButton

@synthesize normal = _normal, selected = _selected, highlight = _highlight;
@synthesize text= _text;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        _state = UIControlStateNormal;
        self.backgroundColor = [UIColor clearColor];
        self.exclusiveTouch = YES;
    }
    return self;
}

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    NSString* imgUrl    = nil;
    UIColor *textColor  = nil;
    if (_state == UIControlStateNormal)
    {
        textColor = UIColorFromRGB(0x666666);
        imgUrl = _normal;
    }
    else if (_state == UIControlStateHighlighted)
    {
        textColor = UIColorFromRGB(0xe26b2d);
        imgUrl = _highlight;
    }
    else
    {
        imgUrl      = _selected ? _selected : _normal;
        textColor   =(_selected)?UIColorFromRGB(0xe26b2d):UIColorFromRGB(0x666666);
    }
    UIImage* img = [UIImage imageNamed:imgUrl];
    
    CGSize sz = rect.size, szImg = img.size;
    CGPoint pt = rect.origin;
    CGRect rcImg = rect;
    
//    if (sz.width > szImg.width)
    {
        rcImg.size = szImg;
        rcImg.origin = CGPointMake(pt.x + (sz.width - szImg.width)/2, pt.y + 4);
    }
    [img drawInRect:rcImg];
    
    //text
    CGRect rc = CGRectMake(pt.x, (int)( pt.y + sz.height*2/3 + 1 ), sz.width, sz.height/3);
    [JUIColorFromRGB_White set];
    [_text drawInRect:rc withFont:[UIFont systemFontOfSize:11] lineBreakMode:NSLineBreakByTruncatingTail alignment:NSTextAlignmentCenter];
    [textColor set];
    rc.origin.y = rc.origin.y - 1;
    [_text drawInRect:rc withFont:[UIFont systemFontOfSize:11] lineBreakMode:NSLineBreakByTruncatingTail alignment:NSTextAlignmentCenter];
}

-(void)setState:(UIControlState)state
{
    _state = state;
    [self setNeedsDisplay];
}

-(void)setText:(NSString *)aText
{
    if (![_text isEqualToString:aText]) {
        _text = aText;
        [self setNeedsDisplay];
    }
}

-(void)setImage:(NSString *)imgUrl forState:(UIControlState)state
{
    if (state == UIControlStateNormal) {
        self.normal = imgUrl;
    } else if (state == UIControlStateHighlighted) {
        self.highlight = imgUrl;
    } else {
        self.selected = imgUrl;
    }
}

-(void)addTarget:(id)target action:(SEL)action
{
    _target = target;
    _selector = action;
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    _lastState = _state;
    [self setState:UIControlStateHighlighted];
//    [_target performSelector:_selector withObject:self];
}

-(void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
//    UITouch* touch = [touches anyObject];
//    CGPoint pt = [touch locationInView:self];
//    
//    if (pt.y < 0) {
//        [self setState:_lastState];
//        return;
//    }
    [self setState:UIControlStateSelected];
    [_target performSelector:_selector withObject:self];
}

- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self setState:_lastState];
}

-(void)dealloc
{
    self.normal = nil;
    self.highlight = nil;
    self.selected = nil;
    self.text = nil;
//    [super dealloc];
}

@end
