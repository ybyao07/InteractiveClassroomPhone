//
//  TabToolBarProtocol.h
//  PgtComm
//
//  Created by jeffery zhao on 2017/7/25.
//  Copyright © 2017年 Pugutang. All rights reserved.
//

#import <Foundation/Foundation.h>


#define gJDefaultTabbarHeight       49


@protocol TabToolBarProtocol <NSObject>

@required

/**
 * perSender:之前选中的Item，用于区别是否单击当前tab
 */
-(void)tabItemClicked:(id)sender withPerSelected:(id)perSender;

@end
