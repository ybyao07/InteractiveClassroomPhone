//
//  TabBarButton.h
//  PgtComm
//
//  Created by jeffery zhao on 2017/7/25.
//  Copyright © 2017年 Pugutang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TabBarButton : UIView
{
    NSString* _selected;
    NSString* _highlight;
    NSString* _normal;
    NSString* _text;
    id       _target;
    SEL      _selector;
    UIControlState _state, _lastState;
}

-(void)addTarget:(id)target action:(SEL)action;
-(void)setState:(UIControlState)state;
-(void)setImage:(NSString*)imgUrl forState:(UIControlState)state;
-(void)setText:(NSString*)text;

@property (nonatomic,strong) NSString* text;

@end
