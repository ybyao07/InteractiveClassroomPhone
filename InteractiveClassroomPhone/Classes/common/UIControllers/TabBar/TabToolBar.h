//
//  TabToolBar.h
//  InteractiveClassroom
//
//  Created by 姚永波 on 2018/2/10.
//  Copyright © 2018年 姚永波. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TabToolBarProtocol.h"

@class TabBarButton;

@interface TabToolBar : UIView
{
    NSInteger _selected;
    UIImageView *_topShade;
}

@property (nonatomic, strong) NSArray *items;
@property (nonatomic, assign) BOOL fixedSpace;
@property (nonatomic, assign) UIImageView *backgroundView;
@property (nonatomic, weak) id<TabToolBarProtocol> clickDelegate;

- (TabBarButton *)getSelectedItem;
- (void)setTabBarSelected:(int)nSelTag;


@end
