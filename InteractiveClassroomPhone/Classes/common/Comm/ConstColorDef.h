//
//  constColorDef.h
//  PgtComm
//
//  Created by jeffery zhao on 2017/7/25.
//  Copyright © 2017年 Pugutang. All rights reserved.
//

#ifndef constColorDef_h
#define constColorDef_h


#define UIColorFromRGB(rgbValue)  [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]
#define UIColorFromRGBA(rgbValue, alphaValue)   [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:alphaValue]
/**
 *
 *  ******************************************* 颜色 *******************************************
 *
 */
/// ----------主色调 三种颜色
/// 主色调 黑色
#define     JUIColorFromRGB_Main1               UIColorFromRGB(0x33333)
/// 辅色调 orang
#define     JUIColorFromRGB_Main2               UIColorFromRGB(0xe26b2d)
/// 辅色调  绿色
#define     JUIColorFromRGB_Main3               UIColorFromRGB(0x8bc34a)
// 副标题颜色
#define     JUIColorFromRGB_Main4               UIColorFromRGB(0x666666)


/// ----------重要  五种颜色
/// 重要文字、标题 -- 蓝色
#define     JUIColorFromRGB_Major1              UIColorFromRGB(0x3b94ff)
/// 特别强调的文字、按钮  -- 浅蓝
#define     JUIColorFromRGB_Major2              UIColorFromRGB(0xe7effa)
/// 特别强调的文字、按钮 -- 橘黄
#define     JUIColorFromRGB_Major3              UIColorFromRGB(0xff6801)



/// ---------- 较弱  三种颜色
/// 分割线 标签描边
#define     JUIColorFromRGB_Gray1               UIColorFromRGB(0xf2f2f2)
/// 视图默认背景色 内容底色 --- cell背景色
#define     JUIColorFromRGB_Gray2               UIColorFromRGB(0xc6c6c6)
/// 主绿或者主橙按钮上的文字
#define     JUIColorFromRGB_White               UIColorFromRGB(0xffffff)

///视图默认背景色
#define     JUIColorFromRGB_ViewBg               JUIColorFromRGB_Gray2

///导航栏底色
#define     JUIColorFromRGB_NavBg                JUIColorFromRGB_Major1

/// 边框
#define     JUIColorFromRGB_Border               UIColorFromRGBA(0xd9d9d9, 0.8f)


//分割线
#define     JUIColorFromRGB_line UIColorFromRGB(0xf1f1f1)


///背景色
#define     JUIColorFromRGB_bg UIColorFromRGB(0x0753b7)
//背景色2
#define     JUIColorFromRGB_bgGrayBlue UIColorFromRGB(0xE7EEF8)

//选中UIbutton 颜色
#define JUIColorFromRGB_ButtonSelected UIColorFromRGB(0x3b94ff)


#endif /* constColorDef_h */
