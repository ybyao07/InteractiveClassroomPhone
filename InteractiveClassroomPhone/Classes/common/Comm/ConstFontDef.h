//
//  ConstFontDef.h
//  InteractiveClassroom
//
//  Created by 姚永波 on 2018/2/10.
//  Copyright © 2018年 姚永波. All rights reserved.
//

#ifndef ConstFontDef_h
#define ConstFontDef_h

#pragma mark - 字体函数
#define JSIZE_Expent(size)                          (size*1.17)
/**
 *
 *  ******************************************* 字体 *******************************************
 *
 */
#pragma mark - 字体函数
#define JFONT(size)                          ([UIFont systemFontOfSize:(size)])
#define JFONT_WITH_PS_SIZE(size)             ([UIFont systemFontOfSize:(size/2 + 1.5)])
#define JFONT_BOLD(size)                     ([UIFont boldSystemFontOfSize:(size)])
#define JFONT_Name(fontName, sz)             ([UIFont fontWithName:fontName size:(sz)])

#define kFont14 [UIFont systemFontOfSize:14]
#define kFont13 [UIFont systemFontOfSize:13]
#define kFont12 [UIFont systemFontOfSize:12]
#define kFont11 [UIFont systemFontOfSize:11]

#define kFont9 [UIFont systemFontOfSize:9]
#define kFont10 [UIFont systemFontOfSize:10]
#define kFont15 [UIFont systemFontOfSize:15]
#define kFont16 [UIFont systemFontOfSize:16]
#define kFont18 [UIFont systemFontOfSize:18]

/// 大标题
#define     JFONT_Size_H1                    20
/// 小标题
#define     JFONT_Size_H2                    16
/// 标题1
#define     JFONT_Size_B1                    14
/// 标题2
#define     JFONT_Size_B2                    13
/// 正文1
#define     JFONT_Size_T1                    12
/// 正文2
#define     JFONT_Size_T2                    11
/// 辅助
#define     JFONT_Size_F1                    10


#endif /* ConstFontDef_h */
