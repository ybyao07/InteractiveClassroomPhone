//
//  UtilTools.m
//  InteractiveClassroom
//
//  Created by 姚永波 on 2018/3/25.
//  Copyright © 2018年 姚永波. All rights reserved.
//

#import "UtilTools.h"

@implementation UtilTools

+ (NSString *)timeStamp:(NSString *)timeStampString
{
    // iOS 生成的时间戳是10位
    NSTimeInterval interval    =[timeStampString doubleValue] / 1000.0;
    NSDate *date               = [NSDate dateWithTimeIntervalSince1970:interval];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    NSString *dateString       = [formatter stringFromDate: date];
    return dateString;
}
@end
