//
//  JUITools.m
//  InteractiveClassroom
//
//  Created by 姚永波 on 2018/2/12.
//  Copyright © 2018年 姚永波. All rights reserved.
//

#import "JUITools.h"
#import "AppDelegate.h"

@implementation JUITools


/**
 *  get current root controller
 */
+(UINavigationController*)mainRootController
{
    UIWindow *window = [[UIApplication sharedApplication] keyWindow];
    //UIWindow *window = [[UIApplication sharedApplication].windows firstObject];
    NSObject* root = (UINavigationController *)window.rootViewController;
    return (UINavigationController *)root;
}

/**
 *  get current root controller
 */
+(UINavigationController*)currentRootController
{
    AppDelegate *appdelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    UIViewController *root = (UINavigationController *)appdelegate.window.rootViewController;
    //    UIWindow *window = [[UIApplication sharedApplication] keyWindow];
    //UIWindow *window = [[UIApplication sharedApplication].windows firstObject];
    //    NSObject* root = (UINavigationController *)window.rootViewController;
    if ([root respondsToSelector:@selector(getCurrentRootPage)]) {
        root = [root performSelector:@selector(getCurrentRootPage) withObject:nil];
    }
    return (UINavigationController *)root;
}

/*
 +(AppDelegate*)currentDelegate
 {
 return [UIApplication sharedApplication].delegate;
 }
 */

/**
 *  create back button
 *
 *  @param target   target
 *  @param selector selector
 *
 *  @return back button
 */
+ (UIBarButtonItem*)navButtonBack:(id)target selector:(SEL)selector
{
    UIImage *bgImage = [UIImage imageNamed:@"pgtNav_btn_back"];
    return [JUITools navButtonWithTarget:target selector:selector title:@"" image:bgImage
                                  hAlign:UIControlContentHorizontalAlignmentLeft];
}


/**
 *  create navegation button
 *
 *  @param target   target
 *  @param selector selector
 *  @param title    title
 *  @param aImage   image
 *
 *  @return navegation button
 */
+ (UIBarButtonItem*)navButtonWithTarget:(id)target selector:(SEL)selector title:(NSString*)title
                                  image:(UIImage*)aImage
                                 hAlign:(UIControlContentHorizontalAlignment)hAlign
{
    CGRect rectF = CGRectMake(0, 0, 70, 44);
    UIView* baseView = [[UIView alloc] initWithFrame:rectF];
    
    //    baseView.backgroundColor=[UIColor greenColor];
    //    rectF.size.width = (aImage)?aImage.size.width:rectF.size.width;
    //    if (UIControlContentHorizontalAlignmentRight == hAlign ) {
    //        rectF.origin.x   = baseView.frame.size.width - rectF.size.width;
    //    }
    //    else{
    //        rectF.origin.x   = 0;
    //    }
    
    UIButton* button = [[UIButton alloc] initWithFrame:rectF];
    [button setImage:aImage forState:UIControlStateNormal];
    //    [button setBackgroundColor:[UIColor redColor]];
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [button setTitle:title forState:UIControlStateNormal];
    button.titleLabel.font = [UIFont systemFontOfSize:15];
    button.contentHorizontalAlignment = hAlign;
    [button addTarget:target action:selector forControlEvents:UIControlEventTouchUpInside];
    [baseView addSubview:button];
    
    //nav button
    UIBarButtonItem* item = [[UIBarButtonItem alloc] initWithCustomView:baseView];
    return item;
}


+ (void) showMessageBox:(NSString *) strTip
{
    if(!strTip || strTip.length <=0) return;
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示" message:strTip delegate:nil cancelButtonTitle:@"好的" otherButtonTitles:nil, nil];
    [alert show];
}

+ (UISearchBar *)getClearSearchBar:(UISearchBar *)oldSearchBar{
    
    CGRect r= CGRectMake(0.0f, 0.0f, 1.0f, oldSearchBar.frame.size.height);
    UIGraphicsBeginImageContext(r.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [[UIColor clearColor] CGColor]);
    CGContextFillRect(context, r);
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    [oldSearchBar setBackgroundImage:img];
    return oldSearchBar;
}
@end
