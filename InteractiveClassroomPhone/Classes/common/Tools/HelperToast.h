//
//  HelperToast.h
//  InteractiveClassroom
//
//  Created by 姚永波 on 2018/4/3.
//  Copyright © 2018年 姚永波. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HelperToast : NSObject
//显示loading
+(void)showLoadingWithView:(UIView *)aView;
//影藏loading
+(void)hiddonLoadingWithView:(UIView *)aView;

//显示提示框
+ (void)showMessageWithHud:(NSString*)message
                     addTo:(UIViewController*)controller
                   yOffset:(CGFloat)yoffset;
@end
