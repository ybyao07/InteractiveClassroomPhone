//
//  RandomGeneratorTool.m
//  InteractiveClassroom
//
//  Created by 姚永波 on 2018/3/18.
//  Copyright © 2018年 姚永波. All rights reserved.
//

#import "RandomGeneratorTool.h"

@implementation RandomGeneratorTool

//获取一个随机整数，范围在[from,to]，包括from，包括to
+(int)getRandomNumber:(int)from to:(int)to
{
    return (int)(from + (arc4random() % (to - from + 1)));
}
@end
