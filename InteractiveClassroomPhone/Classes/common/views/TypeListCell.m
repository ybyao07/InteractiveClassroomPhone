//
//  TypeListCell.m
//  InteractiveClassroomPhone
//
//  Created by 姚永波 on 2018/4/15.
//  Copyright © 2018年 姚永波. All rights reserved.
//

#import "TypeListCell.h"

@interface TypeListCell()

@property (strong, nonatomic) UILabel *title;
@property (strong, nonatomic) UILabel *line;
@end

@implementation TypeListCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self setup];
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
}

- (void)setModel:(id)model
{
    _model = model;
    if ([model isKindOfClass:[NSString class]]) {
        _title.text = model;
    }
}

- (void)setup {
    _title = ({
        UILabel *lb  = [UILabel new];
        lb.font = kFont14;
        lb.textColor = JUIColorFromRGB_Main4;
        lb;
    });
    [self.contentView addSubview:_title];
    _line = ({
        UILabel *lb = [UILabel new];
        lb.backgroundColor = JUIColorFromRGB_line;
        lb;
    });
    [self.contentView addSubview:_line];
    __weak __typeof__(self) weakSelf = self;
    [_title mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(weakSelf.mas_centerY);
        make.centerX.mas_equalTo(weakSelf.mas_centerX);
        make.height.mas_equalTo(@46);
    }];
    [_line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.mas_left).with.offset(0);
        make.right.equalTo(weakSelf.mas_right).with.offset(0);
        make.bottom.equalTo(weakSelf.mas_bottom).with.offset(1);
        make.height.mas_equalTo(@1);
    }];
}


@end
