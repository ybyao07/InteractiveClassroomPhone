//
//  TypeListView.h
//  InteractiveClassroomPhone
//
//  Created by 姚永波 on 2018/4/15.
//  Copyright © 2018年 姚永波. All rights reserved.
//

#import <UIKit/UIKit.h>


@class TypeListView;
@protocol TypeListViewDelegate <NSObject>

- (void)typeListView:(TypeListView *)view didSelectIndex:(int)typeIndex;

@end

@interface TypeListView : UIView

@property (weak, nonatomic)  id <TypeListViewDelegate> delegate;

- (void)updateViewWithDatas:(NSArray *)datas;

@end
