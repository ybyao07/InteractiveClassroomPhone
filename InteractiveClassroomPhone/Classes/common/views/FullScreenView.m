//
//  BaseFullScreenView.m
//  WKCarInsurance
//
//  Created by 姚永波 on 2017/6/17.
//  Copyright © 2017年 wkbins. All rights reserved.
//

#import "FullScreenView.h"

@interface FullScreenView ()


@end

@implementation FullScreenView

- (instancetype)init{
    if (self = [super init]) {
        [self setupSubViews];
    }
    return self;
}
//子类必须实现的方法
- (void)setupSubViews
{
    self.backgroundColor = [UIColor whiteColor];
    self.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    self.layer.borderWidth = 0.5f;
    self.layer.cornerRadius = 10.0f;
    self.clipsToBounds = YES;
}

#pragma mark - Animated Mthod
- (void)animatedIn
{
    self.transform = CGAffineTransformMakeScale(1.3, 1.3);
    self.alpha = 0;
    [UIView animateWithDuration:.35 animations:^{
        self.alpha = 1;
        self.transform = CGAffineTransformMakeScale(1, 1);
    }];
}

- (void)animatedOut
{
    [UIView animateWithDuration:.35 animations:^{
        self.transform = CGAffineTransformMakeScale(0.5, 0.5);
        self.alpha = 0.0;
    } completion:^(BOOL finished) {
        if (finished) {
            if (self.controlForDismiss)
            {
                [self.controlForDismiss removeFromSuperview];
            }
            [self removeFromSuperview];
        }
    }];
}


#pragma mark - show or hide self
- (void)show
{
    [self refreshTheUserInterface];
    UIWindow *keywindow = [[UIApplication sharedApplication] keyWindow];
    if (self.controlForDismiss)
    {
        [keywindow addSubview:self.controlForDismiss];
    }
    [keywindow addSubview:self];
    self.center = CGPointMake(keywindow.bounds.size.width/2.0f,
                              keywindow.bounds.size.height/2.0f);
    [self animatedIn];
}

- (void)dismiss
{
    [self animatedOut];
}


- (void)refreshTheUserInterface
{
    if (nil == _controlForDismiss)
    {
        _controlForDismiss = [[UIControl alloc] initWithFrame:[UIScreen mainScreen].bounds];
        _controlForDismiss.backgroundColor = [UIColor colorWithRed:.16 green:.17 blue:.21 alpha:.5];
        [_controlForDismiss addTarget:self action:@selector(touchForDismissSelf:) forControlEvents:UIControlEventTouchUpInside];
    }
}

- (void)touchForDismissSelf:(id)sender
{
    [self animatedOut];
}

#pragma mark -- 
- (UIButton *)buttonStyle:(ButtonStyle)style
{
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    switch (style) {
        case  AlertBtnCancelStyle:
        {
            [button setTitleColor:JUIColorFromRGB_Major1 forState:UIControlStateNormal];
            [button setBackgroundImage:[UIImage imageNamed:@"icon_btn_unSelected"] forState:UIControlStateNormal];
            button.titleLabel.font = kFont18;
        }
            break;
            case AlertBtnSureStyle:
        {
            [button setTitleColor:JUIColorFromRGB_White forState:UIControlStateNormal];
            [button setBackgroundImage:[UIImage imageNamed:@"icon_btn_selected"] forState:UIControlStateNormal];
            button.titleLabel.font = kFont18;
        }
            break;
        default:
            
            break;
    }
    return button;
}


@end
