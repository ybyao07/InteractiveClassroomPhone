//
//  YBBarCharVerticalView.m
//  InteractiveClassroom
//
//  Created by 姚永波 on 2018/4/11.
//  Copyright © 2018年 姚永波. All rights reserved.
//

#import "YBBarCharVerticalView.h"

@interface YBBarCharVerticalView()
@property (nonatomic) CGContextRef context;
@property (nonatomic, strong) NSMutableArray *textIndicatorsLabels;
@property (nonatomic, strong) NSMutableArray *digitIndicatorsLabels;

@property (nonatomic) float barTotalHeight;

@end

@implementation YBBarCharVerticalView

- (id)initWithFrame:(CGRect)frame
         startPoint:(CGPoint)startPoint
             values:(NSMutableArray *)values
             colors:(NSMutableArray *)colors
           maxValue:(float)maxValue
     textIndicators:(NSMutableArray *)textIndicators
          textColor:(UIColor *)textColor
           barWidth:(float)barWidth
       barMaxHeight:(float)barMaxHeight
{
    self = [super initWithFrame:frame];
    if (self) {
        _values = values;
        _colors = colors;
        _maxValue = maxValue;
        _textIndicatorsLabels = [[NSMutableArray alloc] initWithCapacity:[values count]];
        _digitIndicatorsLabels = [[NSMutableArray alloc] initWithCapacity:[values count]];
        _textIndicators = textIndicators;
        _startPoint = startPoint;
        _textColor = textColor ? textColor : [UIColor orangeColor];
        _barWidth = barWidth;
        _barMaxHeight = barMaxHeight;
        _barTotalHeight = barMaxHeight + startPoint.y;
        self.backgroundColor = [UIColor whiteColor];
    }
    return self;
}


- (void)setLabelDefaults:(UILabel *)label
{
    label.textColor = self.textColor;
    [label setTextAlignment:NSTextAlignmentCenter];
    label.adjustsFontSizeToFitWidth = YES;
    //label.adjustsLetterSpacingToFitWidth = YES;
    label.backgroundColor = [UIColor clearColor];
}

- (void)drawRectangle:(CGRect)rect context:(CGContextRef)context color:(UIColor *)color
{
    CGContextSetFillColorWithColor(self.context,color.CGColor);
    CGContextFillRect(context, rect);
}

- (void)drawRect:(CGRect)rect
{
    self.context = UIGraphicsGetCurrentContext();
    int count = (int)[self.values count];
    float startx = self.startPoint.x;
//    float starty = self.barMaxHeight ;
    float barMargin = 30;
    float marginOfBarAndDigit = 20;
    float marginOfTextAndBar = 2;
    float textHeigt = 20;
    for (int i = 0; i < count; i++) {
        //handle textlabel
        float textMargin_x = (i * (self.barWidth + barMargin)) + startx;
        UILabel *textLabel = [[UILabel alloc] initWithFrame:CGRectMake(textMargin_x, self.barTotalHeight + marginOfTextAndBar + textHeigt, self.barWidth, textHeigt)];
        textLabel.text = self.textIndicators[i];
        [self setLabelDefaults:textLabel];
        @try {
            UILabel *originalTextLabel = self.textIndicatorsLabels[i];
            if (originalTextLabel) {
                [originalTextLabel removeFromSuperview];
            }
        }
        @catch (NSException *exception) {
            [self.textIndicatorsLabels insertObject:textLabel atIndex:i];
        }
        [self addSubview:textLabel];
        
        //handle bar
        float barMargin_x = (i * (self.barWidth + barMargin)) + startx;
        float v = [self.values[i] floatValue] <= self.maxValue ? [self.values[i] floatValue]: self.maxValue;
        float rate = 0;
        if (self.maxValue == 0) {
            rate = 0;
        }else{
            rate = v / self.maxValue;
        }
        float barHeight = rate * self.barMaxHeight;
        CGRect barFrame = CGRectMake(barMargin_x, self.barTotalHeight - barHeight + marginOfTextAndBar + textHeigt , self.barWidth, barHeight);
        [self drawRectangle:barFrame context:self.context color:self.colors[i]];
        
        //handle digitlabel
        UILabel *digitLabel = [[UILabel alloc] initWithFrame:CGRectMake(textMargin_x, self.barTotalHeight - barHeight - marginOfBarAndDigit, self.barWidth, textHeigt)];
        digitLabel.text = self.values[i];
        [self setLabelDefaults:digitLabel];
        @try {
            UILabel *originalDigitLabel = self.digitIndicatorsLabels[i];
            if (originalDigitLabel) {
                [originalDigitLabel removeFromSuperview];
            }
        }
        @catch (NSException *exception) {
            [self.digitIndicatorsLabels insertObject:digitLabel atIndex:i];
        }
        [self addSubview:digitLabel];
    }
}

- (void)setValues:(NSMutableArray *)values
{
    for (int i = 0; i < [values count]; i++) {
        _values[i] = values[i];
    }
    [self setNeedsDisplay];
}

@end
