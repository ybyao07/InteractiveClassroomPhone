//
//  TypeListCell.h
//  InteractiveClassroomPhone
//
//  Created by 姚永波 on 2018/4/15.
//  Copyright © 2018年 姚永波. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TypeListCell : UITableViewCell

@property (nonatomic, strong) id model;

@end
