//
//  YBBarCharVerticalView.h
//  InteractiveClassroom
//
//  Created by 姚永波 on 2018/4/11.
//  Copyright © 2018年 姚永波. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YBBarCharVerticalView : UIView

@property (nonatomic, strong) NSMutableArray *values;
@property (nonatomic, strong) NSMutableArray *colors;
@property (nonatomic) float maxValue;
@property (nonatomic, strong) NSMutableArray *textIndicators;
@property (nonatomic, strong) UIColor *textColor;
@property (nonatomic) float barWidth;
@property (nonatomic) float barMaxHeight;
@property (nonatomic) CGPoint startPoint;

- (id)initWithFrame:(CGRect)frame
         startPoint:(CGPoint)startPoint
             values:(NSMutableArray *)values
             colors:(NSMutableArray *)colors
           maxValue:(float)maxValue
     textIndicators:(NSMutableArray *)textIndicators
          textColor:(UIColor *)textColor
          barWidth:(float)barWidth
        barMaxHeight:(float)barMaxHeight;

@end
