//
//  BaseFullScreenView.h
//  WKCarInsurance
//
//  Created by 姚永波 on 2017/6/17.
//  Copyright © 2017年 wkbins. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, ButtonStyle) {
    AlertBtnCancelStyle,
    AlertBtnDefaultStyle,
    AlertBtnSureStyle
};

@interface FullScreenView : UIView

@property (strong, nonatomic) UIControl *controlForDismiss;
/*
 * 子类需要override此方法
 */
- (void)setupSubViews;
/*
 * 显示
 */
- (void)show;
/*
 * 隐藏
 */
- (void)dismiss;

/**
 * 根据UIButton样式返回指定样式的UIButton
 */
- (UIButton *)buttonStyle:(ButtonStyle)style;

@end
