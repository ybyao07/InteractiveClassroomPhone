//
//  TypeListView.m
//  InteractiveClassroomPhone
//
//  Created by 姚永波 on 2018/4/15.
//  Copyright © 2018年 姚永波. All rights reserved.
//

#import "TypeListView.h"
#import "TypeListCell.h"

@interface TypeListView()<UITableViewDelegate, UITableViewDataSource,UIGestureRecognizerDelegate>

@property (strong, nonatomic) UITableView *tableView;
@property (strong, nonatomic) NSMutableArray *datas;

@end
@implementation TypeListView

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.frame= CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
        self.backgroundColor = [UIColor colorWithRed:.16 green:.17 blue:.21 alpha:.0];
        UIWindow *keywindow = [[[UIApplication sharedApplication] delegate] window];
        [keywindow addSubview:self];
        UITapGestureRecognizer *tapGestureTel = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                        action:@selector(removeSelf)];
        tapGestureTel.delegate = self;
        [tapGestureTel setNumberOfTapsRequired:1];
        [tapGestureTel setNumberOfTouchesRequired:1];
        [self setUserInteractionEnabled:YES];
        [self addGestureRecognizer:tapGestureTel];
        [self setup];
    }
    return self;
}
- (void)setup {
    _tableView = [[UITableView  alloc] initWithFrame:CGRectMake(0, 20, SCREEN_WIDTH - 40, 500) style:UITableViewStylePlain];
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    _tableView.layer.cornerRadius = 5;
    _tableView.scrollEnabled = NO;
    [self addSubview:_tableView];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.rowHeight = 48;
    [_tableView registerClass:[TypeListCell class] forCellReuseIdentifier:@"TypeListCell"];
}


- (void)updateViewWithDatas:(NSArray *)datas {
    _datas = [NSMutableArray arrayWithArray:datas];
    _tableView.frame = CGRectMake(20, 64 + 10 + 50, SCREEN_WIDTH - 40, datas.count *48);
    [_tableView reloadData];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.datas.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    TypeListCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TypeListCell" forIndexPath:indexPath];
    cell.model = self.datas[indexPath.row];
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 48;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self removeSelf];
//    TypeListCell *model = [_datas objectAtIndex:indexPath.row];
    if (self.delegate && [self.delegate respondsToSelector:@selector(typeListView:didSelectIndex:)]) {
        [self.delegate typeListView:self didSelectIndex:(int)indexPath.row];
    }
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    if ([NSStringFromClass([touch.view class])    isEqualToString:@"UITableViewCellContentView"]) {
        return NO;
    }
    return YES;
}
- (void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [self removeSelf];
}

- (void)removeSelf {
    [self removeFromSuperview];
}


@end
