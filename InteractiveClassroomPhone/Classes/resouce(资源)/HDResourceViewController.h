//
//  HDResourceViewController.h
//  InteractiveClassroomPhone
//
//  Created by 姚永波 on 2018/2/26.
//  Copyright © 2018年 姚永波. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YBaseViewController.h"

typedef NS_ENUM(NSUInteger,ResourceTag)
{
    ResourceTab_Courseware = 2,          //课件
    ResourceTab_Materials = 3,          //素材
    ResourceTab_Upload = 5,          //我的上传
    ResourceTab_Recommand           //资源推荐
} ;
@interface HDResourceViewController : YBaseViewController

@end
