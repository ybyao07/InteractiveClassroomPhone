//
//  ResourceTypeModel.m
//  InteractiveClassroomPhone
//
//  Created by 姚永波 on 2018/3/27.
//  Copyright © 2018年 姚永波. All rights reserved.
//

#import "ResourceTypeModel.h"

@implementation ResourceTypeModel

+ (instancetype)modelWithType:(ResourceCellType)type
{
    ResourceTypeModel *model  = [[ResourceTypeModel alloc] init];
    model.type = type;
    return model;
}
@end
