//
//  ResourceTypeModel.h
//  InteractiveClassroomPhone
//
//  Created by 姚永波 on 2018/3/27.
//  Copyright © 2018年 姚永波. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSUInteger, ResourceCellType) {
    ResourceTypeModelCellCourseware=0,  // 课件
    ResourceTypeModelCellMaterials,       //素材
    ResourceTypeModelCellUpload,        // 我的上传
    ResourceTypeModelCellRecommand,   //资源推荐
};
@interface ResourceTypeModel : NSObject

@property (assign, nonatomic) ResourceCellType type;

+ (instancetype)modelWithType:(ResourceCellType)type;

@end
