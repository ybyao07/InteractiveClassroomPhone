//
//  PPTIconCell.h
//  InteractiveClassroom
//
//  Created by 姚永波 on 2018/3/30.
//  Copyright © 2018年 姚永波. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PPTThumbModel.h"

@interface PPTIconCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imgView;


@property (strong, nonatomic) PPTThumbModel *model;

@end
