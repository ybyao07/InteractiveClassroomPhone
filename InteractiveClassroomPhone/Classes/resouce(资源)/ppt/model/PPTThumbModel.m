//
//  PPTThumbModel.m
//  InteractiveClassroom
//
//  Created by 姚永波 on 2018/3/30.
//  Copyright © 2018年 姚永波. All rights reserved.
//

#import "PPTThumbModel.h"

@implementation PPTThumbModel
+ (instancetype)initWithImage:(NSData *)data selected:(BOOL)selected
{
    PPTThumbModel *model = [[PPTThumbModel alloc] init];
    model.img = [UIImage imageWithData:data];
    model.isSelected = selected;
    return model;
}
@end
