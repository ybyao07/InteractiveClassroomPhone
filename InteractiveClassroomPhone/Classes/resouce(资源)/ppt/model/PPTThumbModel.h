//
//  PPTThumbModel.h
//  InteractiveClassroom
//
//  Created by 姚永波 on 2018/3/30.
//  Copyright © 2018年 姚永波. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PPTThumbModel : NSObject

@property (strong, nonatomic) UIImage *img;
@property (nonatomic, assign) BOOL isSelected;

+ (instancetype)initWithImage:(NSData *)data selected:(BOOL)selected;

@end
