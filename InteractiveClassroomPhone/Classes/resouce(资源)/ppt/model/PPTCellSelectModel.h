//
//  PPTCellSelectModel.h
//  InteractiveClassroomPhone
//
//  Created by 姚永波 on 2018/3/28.
//  Copyright © 2018年 姚永波. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PPTCellSelectModel : NSObject

@property (nonatomic, strong) NSData *data;
@property (nonatomic, assign) BOOL isSelected;

@end
