//
//  PPTViewController.m
//  InteractiveClassroomPhone
//
//  Created by 姚永波 on 2018/3/27.
//  Copyright © 2018年 姚永波. All rights reserved.
//

#import "PPTViewController.h"
#import "PPTModel.h"
#import "PPTThumbViewCell.h"
#import "PPTThumbModel.h"
#import "PPTIconCell.h"

@interface PPTViewController ()<UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout>
{
    //缩略图
    NSDictionary *dicJson;
    NSData *thumbData;
    NSMutableData *multiData;
    long multiJsonLength;
    NSArray *thumbSizes;
}
@property (strong, nonatomic) PPTModel *pptM;
@property (weak, nonatomic) IBOutlet UILabel *lblPagehint;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UIButton *preBtn;
@property (weak, nonatomic) IBOutlet UIButton *nextBtn;
//@property (strong, nonatomic) UIImageView *currentImageView;
@property (strong, nonatomic) IBOutlet UIImageView *showImageView;
@property (strong, nonatomic) NSData *imgData;
@property (strong, nonatomic) NSMutableArray <PPTThumbModel *> *arrIcons;

@property (nonatomic, assign) NSInteger totalPage;
@property (nonatomic, assign) NSInteger currentIndex;
@end

@implementation PPTViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = self.pptTitle;
    [self.collectionView registerNib:[UINib nibWithNibName:@"PPTIconCell" bundle:nil] forCellWithReuseIdentifier:@"PPTIconCell"];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshOnSocketData:) name:kHDNotificationCenterSocketStream object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshViewSockentData:) name:kHDNotificationCenterSocketPPTStream object:nil];
    _preBtn.enabled = NO;
    [self showLoadAnimation];
    self.currentIndex = 1;
//    [self addTestData];
}

- (void)addTestData
{
    NSDictionary *dic = @{
                          @"fileId":@"123",
                          @"documentName":@"测试.ppt",
                          @"fileType":@"ppt",
                          @"totalPage":@(10),
                          @"currentPage":@(0)
                          };
    self.pptM = [PPTModel mj_objectWithKeyValues:dic];
}

//================= 缩略图 ===========
- (void)refreshOnSocketData:(NSNotification *)notif
{
    HDLog(@" ====== 缩略图回调 ==========");
    NSDictionary *dicOrigin = notif.userInfo;
    NSData *data = dicOrigin[@"data"];
    // ======= 整合数据，显示 ======
    // pptinfo0x:xxxx
    // pptinfo1x:数据
    // pptinfo2x:数据
    // pptinfo3x:xxxx数据
    Byte *dataByte = (Byte *)[data bytes];
    Byte judgeByte[1],pageByte[1];
    memcpy(&judgeByte, &dataByte[7], 1);
    long status = judgeByte[0]&0xFF;
    if (status == 3) {
        //先Json再Thumb
        Byte jsonFourByte[4];
        memcpy(&jsonFourByte, &dataByte[9], 4);
        long jsonLength = [self heightBytesToInt:jsonFourByte];
        NSData *jsonData = [data subdataWithRange:NSMakeRange(13, jsonLength)];
        dicJson = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableLeaves error:nil];
        thumbData = [data subdataWithRange:NSMakeRange(13 + jsonLength , data.length - jsonLength - 13)];
        thumbSizes =  dicJson[@"icons"];
    }else if (status == 0){
        Byte jsonFourByte[4];
        memcpy(&jsonFourByte, &dataByte[9], 4);
        multiJsonLength = [self heightBytesToInt:jsonFourByte];
        multiData  = [NSMutableData data];
        [multiData appendData:[data subdataWithRange:NSMakeRange(13, data.length - 13)]];
        //        [multiData appendData:data];
    }else if (status == 1){
        NSData *pptData = [data subdataWithRange:NSMakeRange(0,6)];
        NSString *pptStr = [[NSString alloc] initWithData:pptData encoding:NSUTF8StringEncoding];
        NSLog(@"======= pptString is %@",pptStr);
        [multiData appendData:[data subdataWithRange:NSMakeRange(9 , data.length - 9)]];
    }else if (status == 2){
        [multiData appendData:[data subdataWithRange:NSMakeRange(9 , data.length - 9)]];
        NSData *jsonData = [multiData subdataWithRange:NSMakeRange(0, multiJsonLength)];
        NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        NSLog(@"======= jsonString is %@",jsonString);
        dicJson = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableLeaves error:nil];
        thumbData = [multiData subdataWithRange:NSMakeRange(multiJsonLength , multiData.length - multiJsonLength)];
        thumbSizes =  dicJson[@"icons"];
    }
    if ([dicJson[@"code"] integerValue] == MaterialInfo) {
        //        [self removeLoadAnimation];
        PPTModel *model = [PPTModel mj_objectWithKeyValues:dicJson];
        self.pptM = model;
        __block long long offsetData = 0;
        [self.arrIcons removeAllObjects];
        [thumbSizes enumerateObjectsUsingBlock:^(NSNumber *  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            if (thumbData.length >= (offsetData + [obj integerValue])) {
                NSData *data = [thumbData subdataWithRange:NSMakeRange(offsetData,[obj integerValue])];
                PPTThumbModel *thumbM;
                if (idx == 0) { //默认第一个选中
                    thumbM = [PPTThumbModel initWithImage:data selected:YES];
                }else{
                    thumbM = [PPTThumbModel initWithImage:data selected:NO];
                }
                [self.arrIcons addObject:thumbM];
                offsetData =  offsetData + [obj integerValue];
            }
        }];
        [self.collectionView reloadData];
        NSMutableDictionary *dic = [NSMutableDictionary dictionary];
        dic[@"fileId"] = self.fileId;
        dic[@"currentPage"] = @(self.currentIndex);
        [self showLoadAnimation];
        [HDSocketSendCommandTool sendSocketParam:dic commandCode:@(ChangePage) successBlock:^(id result) {
        }];
        self.nextBtn.enabled = YES;
        if (self.currentIndex == 1) {
            self.preBtn.enabled = NO;
        }else{
            self.preBtn.enabled = YES;
        }
    }
}

// ====================== 大图 ==================
- (void)refreshViewSockentData:(NSNotification *)notif
{
    HDLog(@" ====== 大图回调 ==========");
    NSDictionary *dicOrigin = notif.userInfo;
    NSData *data = dicOrigin[@"data"];
    // ======= 整合数据，显示 ======
    // pptpic0x:数据   0
    Byte *dataByte = (Byte *)[data bytes];
    Byte judgeByte[1],pageByte[1];
    memcpy(&judgeByte, &dataByte[6], 1);
    memcpy(&pageByte, &dataByte[7], 1);
    long status = judgeByte[0]&0xFF;
    if (status == 3) {
        multiData  = [NSMutableData data];
        [multiData appendData:[data subdataWithRange:NSMakeRange(8, data.length - 8)]];
        int page = pageByte[0]&0xFF;
        self.currentIndex = page;
        _lblPagehint.text = [NSString stringWithFormat:@"%d/%d",(int)self.currentIndex,(int)[self.pptM.totalPage intValue]];
        self.imgData = multiData;
        self.showImageView.image = [UIImage imageWithData:multiData];
        [self removeLoadAnimation];
    }else if (status == 0){
        multiData  = [NSMutableData data];
        [multiData appendData:[data subdataWithRange:NSMakeRange(8, data.length - 8)]];
    }else if (status == 1){
        [multiData appendData:[data subdataWithRange:NSMakeRange(8, data.length - 8)]];
    }else if (status == 2){
        [multiData appendData:[data subdataWithRange:NSMakeRange(8, data.length - 8)]];
        int page = pageByte[0]&0xFF;
        self.currentIndex = page;
        _lblPagehint.text = [NSString stringWithFormat:@"%d/%d",(int)self.currentIndex,(int)[self.pptM.totalPage intValue]];
        self.imgData = multiData;
        self.showImageView.image = [UIImage imageWithData:multiData];
        [self removeLoadAnimation];
    }
}
- (IBAction)preAction:(UIButton *)sender {
    if (self.currentIndex <= 1) {
        return;
    }
    self.currentIndex--;
    _lblPagehint.text = [NSString stringWithFormat:@"%d/%d",(int)self.currentIndex,(int)[self.pptM.totalPage intValue]];
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    dic[@"fileId"] = self.fileId;
    dic[@"currentPage"] = @(self.currentIndex);
    [self showLoadAnimation];
    [HDSocketSendCommandTool sendSocketParam:dic commandCode:@(ChangePage) successBlock:^(id result) {
    }];
    self.nextBtn.enabled = YES;
    if (self.currentIndex == 1) {
        self.preBtn.enabled = NO;
    }else{
        self.preBtn.enabled = YES;
    }
}
- (IBAction)nextAction:(UIButton *)sender {
    if (self.currentIndex == (int)[self.pptM.totalPage integerValue]) {
        return;
    }
    self.currentIndex++;
    _lblPagehint.text = [NSString stringWithFormat:@"%d/%d",(int)self.currentIndex,(int)[self.pptM.totalPage intValue]];
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    dic[@"fileId"] = self.fileId;
    dic[@"currentPage"] = @(self.currentIndex);
    [self showLoadAnimation];
    [HDSocketSendCommandTool sendSocketParam:dic commandCode:@(ChangePage) successBlock:^(id result) {
    }];
    self.preBtn.enabled = YES;
    if (self.currentIndex == (int)[self.pptM.totalPage intValue]) {
        self.nextBtn.enabled = NO;
    }else{
        self.nextBtn.enabled = YES;
    }
}
- (IBAction)exitAction:(UIButton *)sender {
    [HDSocketSendCommandTool sendSocketParam:@{} commandCode:@(CloseUI) successBlock:^(id result) {
    }];
    [self.navigationController popViewControllerAnimated:YES];
}


#pragma mark ================= collection delegate ==================
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.arrIcons.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    PPTIconCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"PPTIconCell" forIndexPath:indexPath];
    cell.model = self.arrIcons[indexPath.row];
    return cell;
}

//定义每个UICollectionView 的大小
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake((SCREEN_WIDTH - 20 - 8 )/2, 120);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return 4;
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return 4;
}
//UICollectionView被选中时调用的方法
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    [self changeModelWithIndex:indexPath.row];
}
- (void)changeModelWithIndex:(NSInteger)index {
    for (PPTThumbModel *model in self.arrIcons) {
        model.isSelected = NO;
    }
    PPTThumbModel *model = [self.arrIcons objectAtIndex:index];
    self.currentIndex = index + 1;
    model.isSelected = YES;
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    dic[@"fileId"] = self.fileId;
    dic[@"currentPage"] = @(self.currentIndex);
    [self showLoadAnimation];
    [HDSocketSendCommandTool sendSocketParam:dic commandCode:@(ChangePage) successBlock:^(id result) {
    }];
    self.nextBtn.enabled = YES;
    if (self.currentIndex == 1) {
        self.preBtn.enabled = NO;
    }else{
        self.preBtn.enabled = YES;
    }
    [self.collectionView reloadData];
}


-(long)heightBytesToInt:(Byte[]) byte
{
    long one = byte[0]&0xFF;
    long two = byte[1]&0xFF;  // 03
    long twoHext = two * 16 * 16;
    
    long three = byte[2]&0xFF;
    long threeHex = three * 16 * 16 * 16;
    
    long four = byte[3]&0xFF;
    long fourHex = four * 16 * 16 * 16 * 16;
    long total = fourHex + threeHex +  twoHext + one;
    return total;
}

- (NSMutableArray *)arrIcons
{
    if (!_arrIcons) {
        _arrIcons = [NSMutableArray array];
    }
    return _arrIcons;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


@end
