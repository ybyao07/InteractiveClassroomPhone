//
//  PPTViewController.h
//  InteractiveClassroomPhone
//
//  Created by 姚永波 on 2018/3/27.
//  Copyright © 2018年 姚永波. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YBaseViewController.h"
@interface PPTViewController : YBaseViewController

@property (strong, nonatomic) NSString *fileId;
@property (nonatomic, copy) NSString *pptTitle;

@end
