//
//  ResourceListCell.h
//  InteractiveClassroomPhone
//
//  Created by 姚永波 on 2018/3/27.
//  Copyright © 2018年 姚永波. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ResourceTypeModel.h"

@interface ResourceListCell : UITableViewCell

@property (assign, nonatomic) ResourceCellType myType;

@end
