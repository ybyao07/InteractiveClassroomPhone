//
//  ResourceCell.m
//  InteractiveClassroom
//
//  Created by 姚永波 on 2018/2/12.
//  Copyright © 2018年 姚永波. All rights reserved.
//

#import "ResourceCell.h"
#import "UtilTools.h"
@interface ResourceCell()

@property (weak, nonatomic) IBOutlet UILabel *lblSort;
@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UIImageView *imgFileType;
@property (weak, nonatomic) IBOutlet UILabel *lblTime;
@property (weak, nonatomic) IBOutlet UIButton *btnRead;

@end
@implementation ResourceCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
- (void)setModel:(ResourceModel *)model
{
    _model  = model;
    _lblName.text = model.documentName;
    _lblSort.text = model.sort;
    if (model.isTeach) {
        _btnRead.hidden = NO;
    }else{
        _btnRead.hidden = YES;
    }
    if ([model.fileType isEqualToString:@"exe"]) {
        [_imgFileType setImage:[UIImage imageNamed:@"rerouce_exe"]];
    }else if ([model.fileType isEqualToString:@"swf"]){
        [_imgFileType setImage:[UIImage imageNamed:@"rerouce_flash"]];
    }else if ([model.fileType isEqualToString:@"pdf"]){
        [_imgFileType setImage:[UIImage imageNamed:@"rerouce_pdf"]];
    }else if ([model.fileType isEqualToString:@"ppt"] || [model.fileType isEqualToString:@"pptx"]) {
        [_imgFileType setImage:[UIImage imageNamed:@"rerouce_ppt"]];
    }else if ([model.fileType isEqualToString:@"rar"]){
        [_imgFileType setImage:[UIImage imageNamed:@"rerouce_rar"]];
    }else if ([model.fileType isEqualToString:@"txt"]){
        [_imgFileType setImage:[UIImage imageNamed:@"rerouce_txt"]];
    }else if ([model.fileType isEqualToString:@"doc"] || [model.fileType isEqualToString:@"docx"] ){
        [_imgFileType setImage:[UIImage imageNamed:@"rerouce_word"]];
    }else if ([model.fileType isEqualToString:@"zip"] || [model.fileType isEqualToString:@"zip"]){
        [_imgFileType setImage:[UIImage imageNamed:@"rerouce_zip"]];
    }else if ([model.fileType isEqualToString:@"avi"] || [model.fileType isEqualToString:@"mov"] || [model.fileType isEqualToString:@"mkv"] || [model.fileType isEqualToString:@"flv"] || [model.fileType isEqualToString:@"rm"] || [model.fileType isEqualToString:@"rmvb"]  || [model.fileType isEqualToString:@"mp4"] ){
        [_imgFileType setImage:[UIImage imageNamed:@"rerouce_vedio"]];
    }else if ([model.fileType isEqualToString:@"mp3"] || [model.fileType isEqualToString:@"wma"] || [model.fileType isEqualToString:@"wav"] || [model.fileType isEqualToString:@"aac"] || [model.fileType isEqualToString:@"flac"] ){
        [_imgFileType setImage:[UIImage imageNamed:@"rerouce_radio"]];
    }else if ([model.fileType isEqualToString:@"bmp"] || [model.fileType isEqualToString:@"jpg"] || [model.fileType isEqualToString:@"jpeg"] || [model.fileType isEqualToString:@"png"] || [model.fileType isEqualToString:@"gif"] ){
        [_imgFileType setImage:[UIImage imageNamed:@"rerouce_png"]];
    }else{
        [_imgFileType setImage:[UIImage imageNamed:@"rerouce_question"]];
    }
    if (_resourceTag == ResourceTab_Upload) {
        _lblTime.text = [NSString stringWithFormat:@"上传时间%@", [UtilTools timeStamp:model.uploadTime]];
    }else{
        _lblTime.text = [NSString stringWithFormat:@"收藏时间%@", [UtilTools timeStamp:model.collectionTime]];
    }
}

@end
