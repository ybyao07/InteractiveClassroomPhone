//
//  PPTThumbViewCell.m
//  InteractiveClassroomPhone
//
//  Created by 姚永波 on 2018/3/28.
//  Copyright © 2018年 姚永波. All rights reserved.
//

#import "PPTThumbViewCell.h"

@implementation PPTThumbViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setModel:(PPTCellSelectModel *)model
{
    _model = model;
    if (model.isSelected) {
        self.layer.borderColor = [UIColor orangeColor].CGColor;
        self.layer.borderWidth = 4;
    }else{
        self.layer.borderWidth = 0;
    }
    
}

@end
