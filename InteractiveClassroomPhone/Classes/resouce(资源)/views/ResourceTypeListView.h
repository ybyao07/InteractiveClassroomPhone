//
//  ResourceTypeListView.h
//  InteractiveClassroomPhone
//
//  Created by 姚永波 on 2018/3/27.
//  Copyright © 2018年 姚永波. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ResourceListCell.h"

@class ResourceTypeListView;
@protocol ResourceTypeListViewDelegate <NSObject>

- (void)resourceTypeListView:(ResourceTypeListView *)view didSelectWithModel:(ResourceTypeModel *)model;

@end
@interface ResourceTypeListView : UIView

@property (weak, nonatomic)  id <ResourceTypeListViewDelegate> delegate;

- (void)updateViewWithDatas:(NSArray *)datas;

@end
