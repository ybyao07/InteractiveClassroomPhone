//
//  HDResourceViewController.m
//  InteractiveClassroomPhone
//
//  Created by 姚永波 on 2018/2/26.
//  Copyright © 2018年 姚永波. All rights reserved.
//

#import "HDResourceViewController.h"
#import "ResourceModel.h"
#import "ResourceCell.h"
#import "PPTViewController.h"
#import "ResourceTypeListView.h"

@interface HDResourceViewController ()<UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout,ResourceTypeListViewDelegate>

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (strong, nonatomic) NSMutableArray *resources;
@property (weak, nonatomic) IBOutlet UIButton *btnChoose;

@end

@implementation HDResourceViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = [[NSUserDefaults standardUserDefaults] objectForKey:@"chapterName"];
    [self.collectionView registerNib:[UINib nibWithNibName:@"ResourceCell" bundle:nil] forCellWithReuseIdentifier:@"ResourceCell"];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshViewSockentData:) name:kHDNotificationCenterSocket object:nil];
//    [self addTestData];
    
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    dic[@"materialType"] = @(2);
    [HDSocketSendCommandTool sendSocketParam:dic commandCode:@(Material) successBlock:^(id result) {
    }];
    [self.btnChoose setTitle:@"课件" forState:UIControlStateNormal];
}


- (void)addTestData
{
    NSDictionary *dic = @{
                          @"data":@[
                                  @{
                                      @"fId":@"12",
                                      @"fileType":@"doc",
                                      @"documentName":@"平行线",
                                      @"sort":@"1"
                                      },
                                  @{
                                      @"fId":@"123",
                                      @"fileType":@"ppt",
                                      @"documentName":@"平行线",
                                      @"sort":@"2"
                                      },
                                  @{
                                      @"fId":@"11",
                                      @"fileType":@"doc",
                                      @"documentName":@"平行线",
                                      @"sort":@"3"
                                      }
                                  ]
                          };
    [self.resources removeAllObjects];
    NSArray *array = [ResourceModel mj_objectArrayWithKeyValuesArray:dic[@"data"]];
    [self.resources addObjectsFromArray:array];
    [self.collectionView reloadData];
}


- (void)refreshViewSockentData:(NSNotification *)notif
{
    NSDictionary *dic = notif.userInfo;
    if ([dic[@"code"] integerValue] == MaterialList) {
            [self.resources removeAllObjects];
            NSArray *array = [ResourceModel mj_objectArrayWithKeyValuesArray:dic[@"data"]];
            [self.resources addObjectsFromArray:array];
            [self.collectionView reloadData];
    }
}
#pragma mark action ======
- (IBAction)moreList:(UIButton *)btn
{
    ResourceTypeListView *moreView = [[ResourceTypeListView alloc] init];
    moreView.delegate = self;
    NSMutableArray *listArray = [NSMutableArray array];
    [listArray addObject:[ResourceTypeModel modelWithType:ResourceTypeModelCellCourseware]];
    [listArray addObject:[ResourceTypeModel modelWithType:ResourceTypeModelCellMaterials]];
    [listArray addObject:[ResourceTypeModel modelWithType:ResourceTypeModelCellUpload]];
    [listArray addObject:[ResourceTypeModel modelWithType:ResourceTypeModelCellRecommand]];
    [moreView updateViewWithDatas:listArray];
}


- (void)resourceTypeListView:(ResourceTypeListView *)view didSelectWithModel:(ResourceTypeModel *)model
{
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    switch (model.type) {
        case ResourceTypeModelCellCourseware:{
            dic[@"materialType"] = @(2);
            [HDSocketSendCommandTool sendSocketParam:dic commandCode:@(Material) successBlock:^(id result) {
            }];
            [self.btnChoose setTitle:@"课件" forState:UIControlStateNormal];
        }
        break;
        case ResourceTypeModelCellMaterials:{
            dic[@"materialType"] = @(3);
            [HDSocketSendCommandTool sendSocketParam:dic commandCode:@(Material) successBlock:^(id result) {
            }];
            [self.btnChoose setTitle:@"素材" forState:UIControlStateNormal];
        }
            break;
        case ResourceTypeModelCellUpload:{
            dic[@"materialType"] = @(5);
            [HDSocketSendCommandTool sendSocketParam:dic commandCode:@(Material) successBlock:^(id result) {
            }];
            [self.btnChoose setTitle:@"我的上传" forState:UIControlStateNormal];
        }
            break;
        case ResourceTypeModelCellRecommand:{
            [self.btnChoose setTitle:@"推荐资源" forState:UIControlStateNormal];
        }
            default:
            break;
    }
}
    

#pragma mark ================= collection delegate ==================
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.resources.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    ResourceCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"ResourceCell" forIndexPath:indexPath];
    if ([self.btnChoose.currentTitle isEqualToString:@"我的上传"]) {
        cell.resourceTag = ResourceTab_Upload;
    }else{
        cell.resourceTag = ResourceTab_Courseware;
    }
    cell.model = self.resources[indexPath.row];
    return cell;
}

//定义每个UICollectionView 的大小
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake((SCREEN_WIDTH - 20 - 8 )/2, (SCREEN_HEIGHT - 74 - 120 - 3 * 10)/4);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return 4;
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return 4;
}
//UICollectionView被选中时调用的方法
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    ResourceModel *resourceM = self.resources[indexPath.row];
    dic[@"fileId"] = resourceM.fId;
    [HDSocketSendCommandTool sendSocketParam:dic commandCode:@(OpenMaterial) successBlock:^(id result) {
    }];
    PPTViewController *pptVC = [[PPTViewController alloc] init];
    pptVC.fileId = resourceM.fId;
    pptVC.pptTitle = resourceM.documentName;

    pptVC.view.frame = Rect(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT - 500);
    [[JUITools currentRootController] pushViewController:pptVC animated:YES];
}

- (void)goBackPage
{
    [HDSocketSendCommandTool sendSocketParam:@{} commandCode:@(CloseUI) successBlock:^(id result) {
    }];
    [self.navigationController popToRootViewControllerAnimated:YES];
}

#pragma mark ===== accessory ===
- (NSMutableArray *)resources
{
    if (!_resources) {
        _resources = [NSMutableArray array];
    }
    return _resources;
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
