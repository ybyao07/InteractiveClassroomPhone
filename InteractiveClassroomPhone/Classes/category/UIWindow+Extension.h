//
//  UIWindow+Extension.h
//  InteractiveClassroom
//
//  Created by 姚永波 on 2018/3/17.
//  Copyright © 2018年 姚永波. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIWindow (Extension)

/**
 *  切换当前窗口的RootViewController
 *
 *  @param viewController 要切换到的ViewController
 *
 *  @return 切换成功后的Window
 */
+ (UIWindow *) changeWindowRootViewController:(UIViewController *) viewController animated:(BOOL)animated;

/**
 *  获取当前最上面显示的窗口
 *
 *  @return Window
 */
+ (UIWindow *) getCurrentWindow;
@end
