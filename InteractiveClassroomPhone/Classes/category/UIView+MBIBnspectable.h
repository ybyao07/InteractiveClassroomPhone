//
//  UIView+MBIBnspectable.h
//
//  Created by sunyang on 15/10/10.
//  Copyright (c) 2015年 All rights reserved.
//

#import <UIKit/UIKit.h>

//IB_DESIGNABLE//声明类是可设计的， 此处测试无效，分类应该不能实现

@interface UIView (MBIBnspectable)

@property(assign, nonatomic) IBInspectable NSInteger cornerRadius;
@property(assign, nonatomic) IBInspectable CGFloat borderWidth;
@property(strong, nonatomic) IBInspectable UIColor *borderColor;
@property(assign, nonatomic) IBInspectable BOOL onePx;

@end
