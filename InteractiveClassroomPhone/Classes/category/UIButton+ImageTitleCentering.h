//
//  UIButton+ImageTitleCentering.h
//  WKCarInsurance
//
//  Created by 姚永波 on 2017/2/27.
//  Copyright © 2017年 wkbins. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIButton (ImageTitleCentering)

- (void)centerVerticallyWithPadding:(float)padding;
- (void)centerVertically;

@end
