//
//  HD_SolveProblemVC.h
//  InteractiveClassroomPhone
//
//  Created by 姚永波 on 2018/3/29.
//  Copyright © 2018年 姚永波. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YBaseViewController.h"

typedef NS_ENUM(NSUInteger,SubjectType)
{
    SubjectType_Mine,               //我的题目单
    SubjectType_Recommend,          //推荐题目单
};

@interface HD_SolveProblemVC : YBaseViewController

@property (assign, nonatomic) SubjectType pageType;

@end
