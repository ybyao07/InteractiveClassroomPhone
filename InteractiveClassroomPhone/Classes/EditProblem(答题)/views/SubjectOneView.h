//
//  SubjectOneView.h
//  InteractiveClassroomPhone
//
//  Created by 姚永波 on 2018/4/16.
//  Copyright © 2018年 姚永波. All rights reserved.
//

#import <UIKit/UIKit.h>
#import<WebKit/WebKit.h>
#import "SubjectSchedualModel.h"

@protocol OperationChangeSubjectDelegate <NSObject>

- (void)changeSubject;

@end

@interface SubjectOneView : UIView<WKNavigationDelegate,WKUIDelegate>

@property (weak, nonatomic) IBOutlet UIButton *btnThrow;
@property (weak, nonatomic) IBOutlet UIButton *preBtn;
@property (weak, nonatomic) IBOutlet UIButton *nextBtn;

@property (strong, nonatomic) NSMutableArray <SubjectSchedualModel *> *resources;

+(SubjectOneView *)instanceSubjectView;
@property (nonatomic, weak) id<OperationChangeSubjectDelegate> delegate;

- (void)loadHtml:(NSString *)html;


@end
