//
//  SubjectCollectionCell.m
//  InteractiveClassroom
//
//  Created by 姚永波 on 2018/2/24.
//  Copyright © 2018年 姚永波. All rights reserved.
//

#import "SubjectCollectionCell.h"

@implementation SubjectCollectionCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}


- (void)setModel:(TestModel *)model
{
    _model = model;
    _btnSelected.hidden = !model.Selected;
    _lblSort.text = model.sort;
    _lblTitle.text = model.cardName;
    _lblProblemNum.text = [NSString stringWithFormat:@"%d  题",(int)[model.problemNum intValue]];
}

- (void)setSubModel:(SubjectClassModel *)subModel
{
    _subModel = subModel;
    _btnSelected.hidden = !subModel.isTeach;
    _lblSort.text = subModel.sort;
    _lblTitle.text = subModel.classTestName;
    _lblProblemNum.text = [NSString stringWithFormat:@"%d  题",(int)[subModel.problemCount intValue]];
}



@end
