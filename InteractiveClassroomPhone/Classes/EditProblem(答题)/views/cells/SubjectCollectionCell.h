//
//  SubjectCollectionCell.h
//  InteractiveClassroom
//
//  Created by 姚永波 on 2018/2/24.
//  Copyright © 2018年 姚永波. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TestModel.h"
#import "SubjectClassModel.h"


@interface SubjectCollectionCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIButton *btnSelected;
@property (weak, nonatomic) IBOutlet UILabel *lblSort;
@property (weak, nonatomic) IBOutlet UILabel *lblProblemNum;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;


@property (nonatomic, strong) TestModel *model;

@property (nonatomic, strong) SubjectClassModel *subModel;


@end
