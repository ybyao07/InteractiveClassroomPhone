//
//  SubjectResultView.h
//  InteractiveClassroomPhone
//
//  Created by 姚永波 on 2018/4/16.
//  Copyright © 2018年 姚永波. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SubjectResultView : UIView
@property (weak, nonatomic) IBOutlet UIView *containView;
+(SubjectResultView *)instanceResultView;

@end
