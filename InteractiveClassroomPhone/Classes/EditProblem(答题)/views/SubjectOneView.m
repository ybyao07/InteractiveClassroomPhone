//
//  SubjectOneView.m
//  InteractiveClassroomPhone
//
//  Created by 姚永波 on 2018/4/16.
//  Copyright © 2018年 姚永波. All rights reserved.
//

#import "SubjectOneView.h"
#import "PageMangerCenter.h"
#import "HDSubjectMainViewController.h"

@interface SubjectOneView ()<UIWebViewDelegate>
{
    UIWebView *webView;
}

@property (weak, nonatomic) IBOutlet UIView *containView;
@property (strong, nonatomic) WKWebView *wkView;


@end

@implementation SubjectOneView

- (void)awakeFromNib
{
    [super awakeFromNib];
    self.frame = Rect(0,0, SCREEN_WIDTH, SCREEN_HEIGHT - 64 - 160);
    self.wkView = [[WKWebView alloc] initWithFrame:Rect(0, 0, SCREEN_WIDTH-20, SCREEN_HEIGHT - 64 - 160 - 80)];
//   self.wkView.backgroundColor = [UIColor yellowColor];
    self.wkView.UIDelegate = self;
    self.wkView.navigationDelegate = self;
//    _wkView.scrollView.bounces =NO;
//    _wkView.scrollView.showsVerticalScrollIndicator = NO;
//    _wkView.scrollView.showsHorizontalScrollIndicator =NO;
    [self.containView addSubview:self.wkView];
//    [self setupWebView];
    _preBtn.enabled = NO;
}

+(SubjectOneView *)instanceSubjectView
{
    NSArray* nibView =  [[NSBundle mainBundle] loadNibNamed:@"SubjectOneView" owner:nil options:nil];
    return [nibView objectAtIndex:0];
}
- (void)loadHtml:(NSString *)html
{
    if (html != nil) {
//        NSString *html = @"<html><body><p>怎么显示网络图片 <img src=\"http://p0.ifengimg.com/pmop/2017/1218/F9636BB16CC72EC34B5FCB78B3CC3F8AF9B6503E_size33_w720_h413.jpeg\" width=\"600\" height=\"344\" alt=\"\" style=\"border: 0px; vertical-align: bottom; display: block; margin: 0px auto; max-width: 100%; height: auto;\"/>打飞机阿里阿萨德飞机埃里克森的艾萨拉时代峻峰了阿萨德了飞机埃里克阿萨德飞机了卡士大夫阿萨德飞机爱丽丝打飞机拉水电费；就拉上的发了建档立卡第三方了静安寺大立科技阿萨德飞机埃里克的算法简历爱上邓丽君发两块地离开了阿拉山口大家发卡兰蒂斯福利卡圣诞节来看删掉就埃弗拉收到了发句sad就发了卡束带结发拉伸的发送打飞机阿里卡萨丁放假啊落实到法律达康书记法律手段就发来看的说法阿萨德了飞机ADSL开发的说法是的看法</p></body></html>";
//        [webView loadHTMLString:html baseURL:nil];
        [_wkView loadHTMLString:html baseURL:nil];
    }
}


- (void)setupWebView
{
    webView=[[UIWebView alloc] initWithFrame:Rect(0, 0, SCREEN_WIDTH-20, SCREEN_HEIGHT - 64 - 160 - 80)];
    webView.scrollView.bounces = NO;
    webView.delegate=self;
    webView.contentMode = UIViewContentModeScaleAspectFit;
    [self.containView addSubview:webView];
}
#pragma mark Web运行
//开始加载
- (void)webViewDidStartLoad:(UIWebView *)webView
{
}

//加载完成
- (void)webViewDidFinishLoad:(UIWebView *)webView
{
}
//加载失败
- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    
}



- (IBAction)nextAction:(UIButton *)sender {
    int pageIndex = [PageMangerCenter shareInstance].currentIndex;
    [PageMangerCenter shareInstance].currentIndex = pageIndex + 1;
    if ([PageMangerCenter shareInstance].currentIndex > [PageMangerCenter shareInstance].kIndexMax) {
        [PageMangerCenter shareInstance].currentIndex = [PageMangerCenter shareInstance].kIndexMax;
        return;
    }
//    self.btnThrow.hidden = YES;
    SubjectSchedualModel *model = self.resources[[PageMangerCenter shareInstance].currentIndex];
    [self loadHtml:model.problemHtml];
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    dic[@"operation"] = @(OperationNextProblems);
    dic[@"status"] = @(OpenOperation);
    self.preBtn.enabled = YES;
    if ([PageMangerCenter shareInstance].currentIndex == self.resources.count-1) {
        self.nextBtn.enabled = NO;
    }else{
        self.nextBtn.enabled = YES;
    }
    [HDSocketSendCommandTool sendSocketParam:dic commandCode:@(ClassroomTestOperation) successBlock:^(id result) {
    }];
    if ([self.delegate respondsToSelector:@selector(changeSubject)]) {
        [self.delegate changeSubject];
    }
}

- (IBAction)preAction:(UIButton *)sender {
    int pageIndex = [PageMangerCenter shareInstance].currentIndex;
    [PageMangerCenter shareInstance].currentIndex = pageIndex - 1;
    if ([PageMangerCenter shareInstance].currentIndex < [PageMangerCenter shareInstance].kIndexMin) {
        [PageMangerCenter shareInstance].currentIndex = [PageMangerCenter shareInstance].kIndexMin;
        return;
    }
//    self.btnThrow.hidden = YES;
    SubjectSchedualModel *model = self.resources[[PageMangerCenter shareInstance].currentIndex];
    [self loadHtml:model.problemHtml];
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    dic[@"operation"] = @(OperationLastProblems);
    dic[@"status"] = @(OpenOperation);
    self.nextBtn.enabled = YES;
    if ([PageMangerCenter shareInstance].currentIndex == 0) {
        self.preBtn.enabled = NO;
    }else{
        self.preBtn.enabled = YES;
    }
    [HDSocketSendCommandTool sendSocketParam:dic commandCode:@(ClassroomTestOperation) successBlock:^(id result) {
    }];
    if ([self.delegate respondsToSelector:@selector(changeSubject)]) {
        [self.delegate changeSubject];
    }
}

- (IBAction)showAction:(UIButton *)sender {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    dic[@"operation"] = @(OperationAnalysis);
    dic[@"status"] = @(OpenOperation);
    [HDSocketSendCommandTool sendSocketParam:dic commandCode:@(ClassroomTestOperation) successBlock:^(id result) {
    }];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"NotificationThrowAction" object:nil];
}

- (void)setResources:(NSMutableArray<SubjectSchedualModel *> *)resources
{
    _resources = resources;
    SubjectSchedualModel *model = resources.firstObject;
    if (resources.count <= 1) {
        self.nextBtn.enabled = NO;
    }
    [self loadHtml:model.problemHtml];
}



- (WKWebView *)webView:(WKWebView *)webView createWebViewWithConfiguration:(WKWebViewConfiguration *)configuration forNavigationAction:(WKNavigationAction *)navigationAction windowFeatures:(WKWindowFeatures *)windowFeatures {
    //    NSString *targetStr = navigationAction.request.URL.absoluteString;
    if (!navigationAction.targetFrame.isMainFrame) {
        [webView loadRequest:navigationAction.request];
    }
    return nil;
}

- (void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation{
    //修改字体大小 300%
    [ webView evaluateJavaScript:@"document.getElementsByTagName('body')[0].style.webkitTextSizeAdjust= '100%'" completionHandler:nil];
//    //修改字体颜色  #9098b8
//    [ webView evaluateJavaScript:@"document.getElementsByTagName('body')[0].style.webkitTextFillColor= '#9098b8'" completionHandler:nil];
//
    // 禁止放大缩小
    NSString *injectionJSString = @"var script = document.createElement('meta');"
    "script.name = 'viewport';"
    "script.content=\"width=device-width, initial-scale=1.0,maximum-scale=1.0, minimum-scale=1.0, user-scalable=no\";"
    "document.getElementsByTagName('head')[0].appendChild(script);";
    [webView evaluateJavaScript:injectionJSString completionHandler:nil];
    dispatch_async(dispatch_get_main_queue(), ^{
//        _webView.height = webView.scrollView.contentSize.height;
//        base_Sc.contentSize =CGSizeMake(base_Sc.width,_webView.bottom);
        [webView.scrollView setContentOffset:CGPointZero];
        [webView.scrollView setScrollsToTop:YES];
    });
}



@end
