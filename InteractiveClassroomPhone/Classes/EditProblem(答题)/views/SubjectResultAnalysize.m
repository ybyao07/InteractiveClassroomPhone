//
//  SubjectResultAnalysize.m
//  InteractiveClassroomPhone
//
//  Created by 姚永波 on 2018/4/16.
//  Copyright © 2018年 姚永波. All rights reserved.
//

#import "SubjectResultAnalysize.h"
#import "TestStudentProgressCell.h"
#import "AnswerStudentList.h"
#import "HDSubjectMainViewController.h"

@interface SubjectResultAnalysize ()<UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout>

@property (strong, nonatomic) NSMutableArray *resources;

@property (weak, nonatomic) IBOutlet UICollectionView *myCollectionView;

@end

@implementation SubjectResultAnalysize

- (void)awakeFromNib
{
    [super awakeFromNib];
    self.frame = Rect(0,0, SCREEN_WIDTH, SCREEN_HEIGHT - 64 - 155);
    [self.myCollectionView registerNib:[UINib nibWithNibName:@"TestStudentProgressCell" bundle:nil] forCellWithReuseIdentifier:@"TestStudentProgressCell"];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshViewSockentData:) name:kHDNotificationCenterSocket object:nil];
    self.myCollectionView.delegate = self;
    self.myCollectionView.dataSource = self;
    //    [self addTestData];
}
- (void)refreshViewSockentData:(NSNotification *)notif
{
    NSDictionary *dic = notif.userInfo;
    if ([dic[@"code"] integerValue] == AnswerResult) {
        [self.resources removeAllObjects];
        NSArray *array = [AnswerStudentList mj_objectArrayWithKeyValuesArray:dic[@"multipleAnswerList"]];
        [array enumerateObjectsUsingBlock:^(AnswerStudentList  * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            [obj.studentAnswerList enumerateObjectsUsingBlock:^(SubjectAnswerResultModel * _Nonnull studentM, NSUInteger idx, BOOL * _Nonnull stop) {
                [self.resources addObject:studentM];
            }];
        }];
        [self.myCollectionView reloadData];
    }
}

+(SubjectResultAnalysize *)instanceView
{
    NSArray* nibView =  [[NSBundle mainBundle] loadNibNamed:@"SubjectResultAnalysize" owner:nil options:nil];
    return [nibView objectAtIndex:0];
}


#pragma mark ================= collection delegate ==================
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.resources.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    TestStudentProgressCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"TestStudentProgressCell" forIndexPath:indexPath];
    SubjectAnswerResultModel *model = self.resources[indexPath.row];
    if ([model.answerResult intValue] == 2) { //正确
        cell.backgroundColor = JUIColorFromRGB_Major2;
    }else if([model.answerResult intValue] == 3){ //错误
        cell.backgroundColor = JUIColorFromRGB_Major3;
    }else{
        cell.backgroundColor = JUIColorFromRGB_Gray2;
    }
    cell.lblName.text = model.studentName;
    return cell;
}
//定义每个UICollectionView 的大小
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake((SCREEN_WIDTH - 20 - 10*3)/4.0, 30);
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return 5;
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return 5;
}

//- (IBAction)praiseAction:(UIButton *)sender {
//    [HDSocketSendCommandTool sendSocketParam:@{@"PraiseMode":@(0)} commandCode:@(Praise) successBlock:^(id result) {
//    }];
//}

- (IBAction)refreshAction:(UIButton *)sender {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    dic[@"operation"] = @(OperationAnswerAnalysis);
    dic[@"status"] = @(OpenOperation);
    [HDSocketSendCommandTool sendSocketParam:dic commandCode:@(ClassroomTestOperationCastScreen) successBlock:^(id result) {
    }];
}
- (IBAction)throwAction:(UIButton *)sender {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    dic[@"operation"] = @(OperationAnswerAnalysis);
    dic[@"status"] = @(OpenOperation);
    [HDSocketSendCommandTool sendSocketParam:dic commandCode:@(ClassroomTestOperation) successBlock:^(id result) {
    }];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"NotificationThrowAction" object:nil];
}

#pragma mark ===== accessory ===
- (NSMutableArray *)resources
{
    if (!_resources) {
        _resources = [NSMutableArray array];
    }
    return _resources;
}
- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}



@end
