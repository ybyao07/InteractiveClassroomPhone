//
//  SubjectClassModel.h
//  InteractiveClassroom
//
//  Created by 姚永波 on 2018/3/24.
//  Copyright © 2018年 姚永波. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SubjectClassModel : NSObject

@property (nonatomic, strong) NSString *subjectId;
@property (nonatomic, copy) NSString *classTestName;
@property (nonatomic, copy) NSString *classTestExplain;
@property (nonatomic, strong) NSNumber *problemCount;
@property (nonatomic, assign) BOOL isTeach;

@property (nonatomic, copy) NSString *sort; //序号

@end
