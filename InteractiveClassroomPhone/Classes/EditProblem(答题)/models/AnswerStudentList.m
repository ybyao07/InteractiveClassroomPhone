//
//  AnswerStudentList.m
//  InteractiveClassroom
//
//  Created by 姚永波 on 2018/4/1.
//  Copyright © 2018年 姚永波. All rights reserved.
//

#import "AnswerStudentList.h"

@implementation AnswerStudentList

+ (NSDictionary *)mj_objectClassInArray {
    return @{
             @"studentAnswerList" : [SubjectAnswerResultModel class],
             };
}
@end
