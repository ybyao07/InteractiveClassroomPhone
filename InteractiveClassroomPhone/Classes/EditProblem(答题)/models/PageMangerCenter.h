//
//  PageMangerCenter.h
//  InteractiveClassroom
//
//  Created by 姚永波 on 2018/3/25.
//  Copyright © 2018年 姚永波. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PageMangerCenter : NSObject

@property (nonatomic, assign) int currentIndex;
@property (nonatomic, assign) int kIndexMin;
@property (nonatomic, assign) int kIndexMax;

+(instancetype) shareInstance;
@end
