//
//  AnswerStudentList.h
//  InteractiveClassroom
//
//  Created by 姚永波 on 2018/4/1.
//  Copyright © 2018年 姚永波. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SubjectAnswerResultModel.h"

@interface AnswerStudentList : NSObject

@property (nonatomic, strong) NSNumber *answerScort;//答案序号，一个题目多个答案时用序号表示1,2,3(单题的话就是1)
@property (nonatomic, strong) NSArray <SubjectAnswerResultModel *> *studentAnswerList;


@end
