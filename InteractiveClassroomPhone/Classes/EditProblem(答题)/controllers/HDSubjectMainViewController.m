//
//  HDSubjectMainViewController.m
//  InteractiveClassroomPhone
//
//  Created by 姚永波 on 2018/4/16.
//  Copyright © 2018年 姚永波. All rights reserved.
//

#import "HDSubjectMainViewController.h"
#import "SubjectOneView.h"
#import "SubjectResultView.h"
#import "SubjectResultAnalysize.h"
#import "UIButton+ImageTitleCentering.h"
#import "SubjectSchedualModel.h"
#import "PageMangerCenter.h"

typedef enum
{
    SubjectTag_Zero = 0,
    SubjectTag_One =  1,
    SubjectTag_Two = 2,
    SubjectTag_Three = 3,
    SubjectTag_Four = 4,
    SubjectTag_Five = 5,
}SubjectTag;

@interface HDSubjectMainViewController ()<OperationChangeSubjectDelegate>
{
    NSString *_classTestName;
}

@property (weak, nonatomic) IBOutlet UIView *containerView;

@property (strong, nonatomic) SubjectOneView *oneView;

@property (strong, nonatomic) SubjectResultView *analizeView;//答题分析
@property (strong, nonatomic) SubjectResultAnalysize *resultView;// 答题结果

@property (strong, nonatomic) NSMutableArray <SubjectSchedualModel *> *resources;

@property (weak, nonatomic) IBOutlet UIButton *btnOne;
@property (weak, nonatomic) IBOutlet UIButton *btnTwoAuto;
@property (weak, nonatomic) IBOutlet UIButton *btnThreeLook;
@property (weak, nonatomic) IBOutlet UIButton *btnFourResult;
@property (weak, nonatomic) IBOutlet UIButton *btnFiveAnalize;
@property (weak, nonatomic) IBOutlet UIButton *btnSixProgress;
@property (weak, nonatomic) IBOutlet UIButton *btnSevenExit;


@end

@implementation HDSubjectMainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = self.titleString;
//    [self.btnOne centerVerticallyWithPadding:0];
    [self.btnTwoAuto centerVerticallyWithPadding:0];
    [self.btnThreeLook centerVerticallyWithPadding:0];
    [self.btnFourResult centerVerticallyWithPadding:0];
    [self.btnFiveAnalize centerVerticallyWithPadding:0];
    [self.btnSixProgress centerVerticallyWithPadding:0];
    [self.btnSevenExit centerVerticallyWithPadding:0];
    
    self.oneView = [SubjectOneView instanceSubjectView];
    self.resultView = [SubjectResultAnalysize instanceView];
    self.analizeView = [SubjectResultView instanceResultView];
    self.btnSixProgress.enabled = NO;
     [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshViewSockentData:) name:kHDNotificationCenterSocket object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(unSelectedButton) name:@"NotificationThrowAction" object:nil];
}

- (void)refreshViewSockentData:(NSNotification *)notif
{
    NSDictionary *dic = notif.userInfo;
    if ([dic[@"code"] integerValue] == ClassroomTestInfo) { // 返回题目信息
        [self.resources removeAllObjects];
        NSArray *array = [SubjectSchedualModel mj_objectArrayWithKeyValuesArray:dic[@"ClassSchedulesList"]];
        [self.resources addObjectsFromArray:array];
        self.oneView.resources = self.resources;
        self.oneView.delegate = self;
        self.oneView.btnThrow.hidden = YES;
        self.oneView.preBtn.hidden = NO;
        self.oneView.nextBtn.hidden = NO;
        _classTestName = dic[@"classroomTestName"];
        self.title = _classTestName;
        [self.containerView addSubview:self.oneView];
        [PageMangerCenter shareInstance].kIndexMax = (int)self.resources.count;
        [self removeLoadAnimation];
    }else if ([dic[@"code"] integerValue] == SwitchNextQuestion){ //返回切换下一题
        [PageMangerCenter shareInstance].currentIndex = [PageMangerCenter shareInstance].currentIndex + 1;
        SubjectSchedualModel *model = self.resources[[PageMangerCenter shareInstance].currentIndex];
        [self.oneView loadHtml:model.problemHtml];
        self.oneView.preBtn.enabled = YES;
        if ([PageMangerCenter shareInstance].currentIndex == self.resources.count-1) {
            self.oneView.nextBtn.enabled = NO;
        }else{
            self.oneView.nextBtn.enabled = YES;
        }
    }
}

- (void)unSelectedButton
{
    self.btnTwoAuto.selected = NO;
}
#pragma mark ==============
- (IBAction)changeItem:(UIButton *)btn
{
    btn.selected = !btn.selected;
    switch (btn.tag) {
        case SubjectTag_Zero: // 开始答题
        {
            if (btn.selected) {
                self.btnFourResult.selected = NO;
                self.btnFiveAnalize.selected = NO;
                [self.analizeView removeFromSuperview];
                [self.resultView removeFromSuperview];
                SubjectSchedualModel *model = self.resources[[PageMangerCenter shareInstance].currentIndex];
                [self.oneView loadHtml:model.problemHtml];
            }
            self.oneView.btnThrow.hidden = YES;
            self.oneView.preBtn.hidden = NO;
            self.oneView.nextBtn.hidden = NO;
            NSMutableDictionary *dic = [NSMutableDictionary dictionary];
            dic[@"operation"] = @(OperationStartAnswer);
            if (btn.selected) {
                dic[@"status"] = @(OpenOperation);
            }else{
                dic[@"status"] = @(CloseOperation);
            }
            [HDSocketSendCommandTool sendSocketParam:dic commandCode:@(ClassroomTestOperation) successBlock:^(id result) {
            }];
        }
            break;
        case SubjectTag_One:  // 自动切换
        {
            if (btn.selected) {
                self.btnSixProgress.enabled = YES;
                self.btnFourResult.selected = NO;
                self.btnFiveAnalize.selected = NO;
                self.btnThreeLook.selected = NO;
                [self.analizeView removeFromSuperview];
                [self.resultView removeFromSuperview];
                SubjectSchedualModel *model = self.resources[[PageMangerCenter shareInstance].currentIndex];
                [self.oneView loadHtml:model.problemHtml];
            }else{
                self.btnSixProgress.enabled = NO;
            }
            self.oneView.btnThrow.hidden = YES;
            self.oneView.preBtn.hidden = NO;
            self.oneView.nextBtn.hidden = NO;
            NSMutableDictionary *dic = [NSMutableDictionary dictionary];
            dic[@"operation"] = @(OperationAutoChange);
            if (btn.selected) {
                dic[@"status"] = @(OpenOperation);
            }else{
                dic[@"status"] = @(CloseOperation);
            }
            [HDSocketSendCommandTool sendSocketParam:dic commandCode:@(ClassroomTestOperation) successBlock:^(id result) {
            }];
        }
            break;
        case SubjectTag_Two:  //查看解析
        {
            if (btn.selected) {
                self.btnFourResult.selected = NO;
                self.btnFiveAnalize.selected = NO;
                [self.analizeView removeFromSuperview];
                [self.resultView removeFromSuperview];
                self.oneView.btnThrow.hidden = NO;
                self.oneView.preBtn.hidden = YES;
                self.oneView.nextBtn.hidden = YES;
                SubjectSchedualModel *model = self.resources[[PageMangerCenter shareInstance].currentIndex];
                [self.oneView loadHtml:model.answerHtml];
                NSMutableDictionary *dic = [NSMutableDictionary dictionary];
                dic[@"operation"] = @(OperationAnalysis);
                dic[@"status"] = @(OpenOperation);
                [HDSocketSendCommandTool sendSocketParam:dic commandCode:@(ClassroomTestOperationCastScreen) successBlock:^(id result) {
                }];
            }else{
                self.oneView.btnThrow.hidden = YES;
                self.oneView.preBtn.hidden = NO;
                self.oneView.nextBtn.hidden = NO;
                SubjectSchedualModel *model = self.resources[[PageMangerCenter shareInstance].currentIndex];
                [self.oneView loadHtml:model.problemHtml];
                NSMutableDictionary *dic = [NSMutableDictionary dictionary];
                dic[@"operation"] = @(OperationAnalysis);
                dic[@"status"] = @(CloseOperation);
                [HDSocketSendCommandTool sendSocketParam:dic commandCode:@(ClassroomTestOperation) successBlock:^(id result) {
                }];
            }
        }
            break;
        case SubjectTag_Three: //答题结果
        {
            if (!btn.selected) {
                [self.analizeView removeFromSuperview];
            }else{
                [self closeResultView];
                self.btnThreeLook.selected = NO;
                [self.containerView addSubview:_analizeView];
            }
            NSMutableDictionary *dic = [NSMutableDictionary dictionary];
            dic[@"operation"] = @(OperationAnswerAnalysis);
            if (btn.selected) {
                dic[@"status"] = @(OpenOperation);
                [HDSocketSendCommandTool sendSocketParam:dic commandCode:@(ClassroomTestOperationCastScreen) successBlock:^(id result) {
                }];
            }else{
                dic[@"status"] = @(CloseOperation);
                [HDSocketSendCommandTool sendSocketParam:dic commandCode:@(ClassroomTestOperation) successBlock:^(id result) {
                }];
            }
            if (!btn.selected) {
                [self.analizeView removeFromSuperview];
            }
        }
            break;
        case SubjectTag_Four:  //答题分析
        {
            if (!btn.selected) {
                [self.resultView removeFromSuperview];
            }else{
                [self closeAnalizeView];
                self.btnThreeLook.selected = NO;
                [self.containerView addSubview:_resultView];
            }
            NSMutableDictionary *dic = [NSMutableDictionary dictionary];
            dic[@"operation"] = @(OperationAnswerResult);
            if (btn.selected) {
                dic[@"status"] = @(OpenOperation);
                [HDSocketSendCommandTool sendSocketParam:dic commandCode:@(ClassroomTestOperationCastScreen) successBlock:^(id result) {
                }];
            }else{
                dic[@"status"] = @(CloseOperation);
                [HDSocketSendCommandTool sendSocketParam:dic commandCode:@(ClassroomTestOperation) successBlock:^(id result) {
                }];
            }
            if (!btn.selected) {
                [self.resultView removeFromSuperview];
            }
        }
            break;
        case SubjectTag_Five:  //答题进度
        {
            NSMutableDictionary *dic = [NSMutableDictionary dictionary];
            dic[@"operation"] = @(OperationAnswerProgress);
            dic[@"status"] = @(OpenOperation);
            [HDSocketSendCommandTool sendSocketParam:dic commandCode:@(ClassroomTestOperation) successBlock:^(id result) {
            }];
        }
            break;
        default:
            break;
    }
}


#pragma mark ======= OperationChangeSubjectDelegate ====
- (void)changeSubject
{
    self.btnThreeLook.selected = NO;
    SubjectSchedualModel *model = self.resources[[PageMangerCenter shareInstance].currentIndex];
    if (model.problemAnswer == nil || model.problemAnswer.length == 0 || [model.problemAnswer isEqualToString:@"\"\""] || [model.problemAnswer isEqualToString:@"{}"]) {
//        self.btnOne.enabled = NO;
        self.btnTwoAuto.enabled = NO;
        self.btnFourResult.enabled = NO;
        self.btnFiveAnalize.enabled = NO;
    }else{
//        self.btnOne.enabled = YES;
        self.btnTwoAuto.enabled = YES;
        self.btnFourResult.enabled = YES;
        self.btnFiveAnalize.enabled = YES;
    }
}

- (void)closeAnalizeView
{
    [self.analizeView removeFromSuperview];
    self.btnFourResult.selected = NO;
}
- (void)closeResultView
{
    [self.resultView removeFromSuperview];
    self.btnFiveAnalize.selected = NO;
}

- (IBAction)closeAction:(UIButton *)sender {
    [HDSocketSendCommandTool sendSocketParam:@{} commandCode:@(CloseUI) successBlock:^(id result) {
    }];
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)goBackPage
{
    [HDSocketSendCommandTool sendSocketParam:@{} commandCode:@(CloseUI) successBlock:^(id result) {
    }];
    [self.navigationController popViewControllerAnimated:YES];
}
#pragma mark ===== accessory ===
- (NSMutableArray *)resources
{
    if (!_resources) {
        _resources = [NSMutableArray array];
    }
    return _resources;
}
- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
