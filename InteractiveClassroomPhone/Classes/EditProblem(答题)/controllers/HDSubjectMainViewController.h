//
//  HDSubjectMainViewController.h
//  InteractiveClassroomPhone
//
//  Created by 姚永波 on 2018/4/16.
//  Copyright © 2018年 姚永波. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YBaseViewController.h"

typedef enum
{
    OperationStartAnswer  = 0,
    OperationAutoChange  =  1,
    OperationAnalysis  = 2,
    OperationAnswerResult = 3,
    OperationAnswerAnalysis = 4,
    OperationAnswerProgress  = 5,
    OperationNextProblems  = 6,
    OperationLastProblems  = 7,
    RetuenProblems
}OperationOption;

typedef enum
{
    OpenOperation = 0,
    CloseOperation = 1
}OperationOpenOrClose;

@interface HDSubjectMainViewController : YBaseViewController

@property (nonatomic, copy) NSString *titleString;

@end
