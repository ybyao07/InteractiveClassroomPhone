//
//  HD_SolveProblemVC.m
//  InteractiveClassroomPhone
//
//  Created by 姚永波 on 2018/3/29.
//  Copyright © 2018年 姚永波. All rights reserved.
//

#import "HD_SolveProblemVC.h"
#import "SubjectClassModel.h"
#import "SubjectCollectionCell.h"
#import "TypeListView.h"
#import "HDSubjectMainViewController.h"

@interface HD_SolveProblemVC ()<UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout,TypeListViewDelegate>
{
    NSMutableArray *popArray;
}
@property (weak, nonatomic) IBOutlet UIButton *btnChoose;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

@property (strong, nonatomic) NSMutableArray *resources;

@end

@implementation HD_SolveProblemVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = [[NSUserDefaults standardUserDefaults] objectForKey:@"chapterName"];
    [_collectionView registerNib:[UINib nibWithNibName:@"SubjectCollectionCell" bundle:nil] forCellWithReuseIdentifier:@"SubjectCollectionCell"];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshViewSockentData:) name:kHDNotificationCenterSocket object:nil];
    
    popArray = [NSMutableArray array];
    [popArray addObject:@"我的题目单"];
    [popArray addObject:@"推荐题目单"];
    
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    dic[@"materialType"] = @(2);
    [HDSocketSendCommandTool sendSocketParam:dic commandCode:@(ClassroomTest) successBlock:^(id result) {
    }];
//    [self addTestData];
}

- (void)addTestData
{
    NSDictionary *dic = @{
                          @"code":@"0",
                          @"data":
                              @[
                                  @{
                                      @"subjectId":@"11",
                                      @"classTestName":@"Test 1",
                                      @"problemCount":@(10),
                                      @"isTeach":@(YES),
                                      @"sort":@"1"
                                      },
                                  @{
                                      @"subjectId":@"12",
                                      @"classTestName":@"Test 2",
                                      @"problemCount":@(1),
                                      @"isTeach":@(NO),
                                      @"sort":@"2"
                                      }
                                  ]
                          };
    [self.resources removeAllObjects];
    NSArray *array = [SubjectClassModel mj_objectArrayWithKeyValuesArray:dic[@"data"]];
    [self.resources addObjectsFromArray:array];
    [self.collectionView reloadData];
}

- (void)refreshViewSockentData:(NSNotification *)notif
{
    NSDictionary *dic = notif.userInfo;
    if ([dic[@"code"] integerValue] == ClassroomTestList) {
        [self.resources removeAllObjects];
        NSArray *array = [SubjectClassModel mj_objectArrayWithKeyValuesArray:dic[@"data"]];
        [self.resources addObjectsFromArray:array];
        [self.collectionView reloadData];
    }
}
#pragma mark ================= collection delegate ==================
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.resources.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    SubjectCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"SubjectCollectionCell" forIndexPath:indexPath];
    cell.subModel = self.resources[indexPath.row];
    return cell;
}

//定义每个UICollectionView 的大小
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake((SCREEN_WIDTH - 20 - 10 )/2, (SCREEN_HEIGHT - 74 - 60 - 3 * 10)/4);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return 5;
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return 5;
}
//UICollectionView被选中时调用的方法
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    SubjectClassModel *model = self.resources[indexPath.row];
    [self sendSocketData:model];
}

- (void)sendSocketData:(SubjectClassModel *)model;
{
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    dic[@"testId"] = model.subjectId;
    [HDSocketSendCommandTool sendSocketParam:dic commandCode:@(OpenClassroomTest) successBlock:^(id result) {
    }];
    HDSubjectMainViewController *vc = [HDSubjectMainViewController new];
    vc.titleString = model.classTestName;
    [[JUITools currentRootController] pushViewController:vc animated:YES];
}

- (IBAction)moreList:(UIButton *)btn
{
    TypeListView *moreView = [[TypeListView alloc] init];
    moreView.delegate = self;
    [moreView updateViewWithDatas:popArray];
}
- (void)typeListView:(TypeListView *)view didSelectIndex:(int)typeIndex
{
    [self.btnChoose setTitle:popArray[typeIndex] forState:UIControlStateNormal];
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    dic[@"materialType"] = @(2);
    [HDSocketSendCommandTool sendSocketParam:dic commandCode:@(ClassroomTest) successBlock:^(id result) {
    }];
    [self.collectionView reloadData];
}


- (void)goBackPage
{
    [HDSocketSendCommandTool sendSocketParam:@{} commandCode:@(CloseUI) successBlock:^(id result) {
    }];
    [self.navigationController popViewControllerAnimated:YES];
}
#pragma mark ===== accessory ===
- (NSMutableArray *)resources
{
    if (!_resources) {
        _resources = [NSMutableArray array];
    }
    return _resources;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
