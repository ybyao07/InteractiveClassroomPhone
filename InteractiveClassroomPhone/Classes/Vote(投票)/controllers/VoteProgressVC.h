//
//  VoteProgressVC.h
//  InteractiveClassroomPhone
//
//  Created by 姚永波 on 2018/4/20.
//  Copyright © 2018年 姚永波. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VoteDetailModel.h"
#import "YBaseViewController.h"

@interface VoteProgressVC : YBaseViewController
@property (nonatomic, strong) VoteDetailModel *voteModel;

@end
