//
//  VoteProgressVC.m
//  InteractiveClassroomPhone
//
//  Created by 姚永波 on 2018/4/20.
//  Copyright © 2018年 姚永波. All rights reserved.
//

#import "VoteProgressVC.h"
#import "VoteListNoEditCell.h"
#import "VoteResultVC.h"

@interface VoteProgressVC ()<UITableViewDelegate,UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UILabel *lblProgress;
@end

@implementation VoteProgressVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = self.voteModel.voteContent;
    [self.tableView registerNib:[UINib nibWithNibName:@"VoteListNoEditCell" bundle:nil] forCellReuseIdentifier:@"VoteListNoEditCell"];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshOnSocketData:) name:kHDNotificationCenterSocket object:nil];
}

- (void)refreshOnSocketData:(NSNotification *)notif
{
    NSDictionary *dic = notif.userInfo;
    if ([dic[@"code"] integerValue] == VoteCountDetails) { //时时投票消息
        long voteCount = [dic[@"votedCount"] integerValue];
        long studentCount = [dic[@"studentCount"] integerValue];
        self.lblProgress.text = [NSString stringWithFormat:@"%ld/%ld",voteCount,studentCount];
    }
}

#pragma mark ======= UITableView ====
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.voteModel.optionList.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return  80.f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    VoteListNoEditCell *noEditCell = [tableView dequeueReusableCellWithIdentifier:@"VoteListNoEditCell"];
    noEditCell.selectionStyle = UITableViewCellSelectionStyleNone;
    noEditCell.model = self.voteModel.optionList[indexPath.row];
    return noEditCell;
}

- (IBAction)checkResultAction:(UIButton *)sender {
    VoteResultVC *voteR = [VoteResultVC new];
    [self.navigationController pushViewController:voteR animated:YES];
}


- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)goBackPage
{
    [HDSocketSendCommandTool sendSocketParam:@{} commandCode:@(CloseWindow) successBlock:^(id result) {
    }];
    [self.navigationController popToRootViewControllerAnimated:YES];
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
