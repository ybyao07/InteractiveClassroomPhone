//
//  VoteOptionDetailVC.m
//  InteractiveClassroomPhone
//
//  Created by 姚永波 on 2018/3/28.
//  Copyright © 2018年 姚永波. All rights reserved.
//

#import "VoteOptionDetailVC.h"
#import "VoteResultVC.h"
#import "VoteDetailModel.h"
#import "VoteListNoEditCell.h"
#import "VoteProgressVC.h"

@interface VoteOptionDetailVC ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) VoteDetailModel *voteModel;

@end

@implementation VoteOptionDetailVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = _voteItem.voteContent;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshOnSocketData:) name:kHDNotificationCenterSocket object:nil];
    [self.tableView registerNib:[UINib nibWithNibName:@"VoteListNoEditCell" bundle:nil] forCellReuseIdentifier:@"VoteListNoEditCell"];
    [self addBottomView];
}
- (void)refreshOnSocketData:(NSNotification *)notif
{
    NSDictionary *dic = notif.userInfo;
    if ([dic[@"code"] integerValue] == VoteOption) {
        //        NSArray *arr = [VoteItem mj_objectArrayWithKeyValuesArray:dic[@"data"]];
        _voteModel = [VoteDetailModel mj_objectWithKeyValues:dic[@"data"]];
        [self.tableView reloadData];
    }
}
- (void)addBottomView
{
    UIButton *btnStart = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnStart setTitle:@"开始投票" forState:UIControlStateNormal];
    [btnStart setTitleColor:JUIColorFromRGB_Major1 forState:UIControlStateNormal];
    [btnStart setBackgroundImage:[UIImage imageNamed:@"icon_btn_unSelected"] forState:UIControlStateNormal];
    btnStart.frame = Rect((SCREEN_WIDTH - 200)/2.0, SCREEN_HEIGHT - 50 - 10 - 40, 200, 40);
    btnStart.titleLabel.font = kFont16;
    [btnStart addTarget:self action:@selector(startVoteAction) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btnStart];
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setTitle:@"退出" forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(exitAction) forControlEvents:UIControlEventTouchUpInside];
    btn.frame = Rect(0, SCREEN_HEIGHT-50, SCREEN_WIDTH, 50);
    btn.backgroundColor = JUIColorFromRGB_Major1;
    [self.view addSubview:btn];
}
#pragma mark ====== UITableViewDelegate ====
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.voteModel.optionList.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 64.f;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    VoteListNoEditCell *noEditCell = [tableView dequeueReusableCellWithIdentifier:@"VoteListNoEditCell"];
    noEditCell.selectionStyle = UITableViewCellSelectionStyleNone;
    noEditCell.model = self.voteModel.optionList[indexPath.row];
    return noEditCell;
}
#pragma mark ========= Action ======
- (void)exitAction
{
    [HDSocketSendCommandTool sendSocketParam:@{} commandCode:@(CloseWindow) successBlock:^(id result) {
    }];
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (void)startVoteAction
{
    [HDSocketSendCommandTool sendSocketParam:@{@"voteId":self.voteItem.voteId} commandCode:@(StartVote) successBlock:^(id result) {
    }];
    VoteProgressVC *progressVC = [VoteProgressVC new];
    progressVC.voteModel = self.voteModel;
    [self.navigationController pushViewController:progressVC animated:YES];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
