//
//  VoteOptionDetailVC.h
//  InteractiveClassroomPhone
//
//  Created by 姚永波 on 2018/3/28.
//  Copyright © 2018年 姚永波. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YBaseTableVC.h"
#import "VoteItem.h"

@interface VoteOptionDetailVC : YBaseTableVC
@property (strong, nonatomic) VoteItem *voteItem;

@end
