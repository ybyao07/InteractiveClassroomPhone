//
//  VoteListNewCell.m
//  InteractiveClassroom
//
//  Created by 姚永波 on 2018/3/2.
//  Copyright © 2018年 姚永波. All rights reserved.
//

#import "VoteListNewCell.h"

@implementation VoteListNewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setModel:(VoteOptionModel *)model
{
    _model = model;
    
}

@end
