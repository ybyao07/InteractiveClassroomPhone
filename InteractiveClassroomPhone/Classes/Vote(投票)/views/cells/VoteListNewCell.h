//
//  VoteListNewCell.h
//  InteractiveClassroom
//
//  Created by 姚永波 on 2018/3/2.
//  Copyright © 2018年 姚永波. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VoteOptionModel.h"

@interface VoteListNewCell : UITableViewCell

@property (strong, nonatomic) VoteOptionModel *model;

@end
