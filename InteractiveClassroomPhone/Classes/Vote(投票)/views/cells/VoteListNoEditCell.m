//
//  VoteListNoEditCell.m
//  InteractiveClassroom
//
//  Created by 姚永波 on 2018/3/18.
//  Copyright © 2018年 姚永波. All rights reserved.
//

#import "VoteListNoEditCell.h"

@implementation VoteListNoEditCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setModel:(VoteOptionModel *)model
{
    _model = model;
    _lblTitle.text = [NSString stringWithFormat:@"选项%@",model.optionLetter];
    _lblContent.text = model.optionContent;
}
@end
