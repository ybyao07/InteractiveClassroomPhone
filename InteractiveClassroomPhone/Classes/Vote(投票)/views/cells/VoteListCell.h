//
//  VoteListCell.h
//  InteractiveClassroom
//
//  Created by 姚永波 on 2018/3/3.
//  Copyright © 2018年 姚永波. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VoteItem.h"

@interface VoteListCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lblContent;
@property (nonatomic, strong) VoteItem *model;

@end
