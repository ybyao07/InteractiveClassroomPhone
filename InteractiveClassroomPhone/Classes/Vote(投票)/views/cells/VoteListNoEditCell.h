//
//  VoteListNoEditCell.h
//  InteractiveClassroom
//
//  Created by 姚永波 on 2018/3/18.
//  Copyright © 2018年 姚永波. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VoteOptionModel.h"

@interface VoteListNoEditCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblContent;

@property (strong, nonatomic) VoteOptionModel *model;

@end
