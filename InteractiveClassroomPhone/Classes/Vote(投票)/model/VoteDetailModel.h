//
//  VoteDetailModel.h
//  InteractiveClassroom
//
//  Created by 姚永波 on 2018/3/11.
//  Copyright © 2018年 姚永波. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VoteOptionModel.h"

@interface VoteDetailModel : NSObject

@property (strong, nonatomic) NSString *voteId;
@property (strong, nonatomic) NSString *voteContent;
@property (strong, nonatomic) NSArray<VoteOptionModel *> *optionList;

@end
