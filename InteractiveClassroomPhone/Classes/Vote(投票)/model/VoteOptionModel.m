//
//  VoteOptionModel.m
//  InteractiveClassroom
//
//  Created by 姚永波 on 2018/3/11.
//  Copyright © 2018年 姚永波. All rights reserved.
//

#import "VoteOptionModel.h"

@implementation VoteOptionModel
+ (instancetype)createOptionLetter:(NSString *)letter content:(NSString *)content
{
    VoteOptionModel *model = [[VoteOptionModel alloc] init];
    model.optionLetter = letter;
    model.optionContent = content;
    return model;
}
@end
