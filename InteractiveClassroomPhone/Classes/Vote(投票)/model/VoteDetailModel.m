//
//  VoteDetailModel.m
//  InteractiveClassroom
//
//  Created by 姚永波 on 2018/3/11.
//  Copyright © 2018年 姚永波. All rights reserved.
//

#import "VoteDetailModel.h"

@implementation VoteDetailModel

+ (NSDictionary *)mj_objectClassInArray {
    return @{
             @"optionList" : [VoteOptionModel class],
             };
}
@end
