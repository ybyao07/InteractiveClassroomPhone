//
//  VoteOptionModel.h
//  InteractiveClassroom
//
//  Created by 姚永波 on 2018/3/11.
//  Copyright © 2018年 姚永波. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VoteOptionModel : NSObject

@property (strong, nonatomic) NSString *optionId;
@property (strong, nonatomic) NSString *optionLetter;   //选项序号
@property (strong, nonatomic) NSString *optionContent;  //选项内容

@property (nonatomic, strong) NSNumber *selectedCount;
@property (nonatomic, strong) NSNumber *selectedPercentage;

// 创建投票时用
+ (instancetype)createOptionLetter:(NSString *)letter content:(NSString *)content;


@end
