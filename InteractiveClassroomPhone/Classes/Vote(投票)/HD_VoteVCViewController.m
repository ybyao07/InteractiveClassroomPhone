//
//  HD_VoteVCViewController.m
//  InteractiveClassroomPhone
//
//  Created by 姚永波 on 2018/3/28.
//  Copyright © 2018年 姚永波. All rights reserved.
//

#import "HD_VoteVCViewController.h"
#import "VoteListNewCell.h"
#import "VoteListCell.h"
#import "VoteNewVC.h"
#import "VoteItem.h"
#import "VoteOptionDetailVC.h"


@interface HD_VoteVCViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (strong, nonatomic) NSMutableArray *arrData;

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation HD_VoteVCViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"投票";
    [self.tableView registerNib:[UINib nibWithNibName:@"VoteListNewCell" bundle:nil] forCellReuseIdentifier:@"VoteListNewCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"VoteListCell" bundle:nil] forCellReuseIdentifier:@"VoteListCell"];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshOnSocketData:) name:kHDNotificationCenterSocket object:nil];
    [self sendSocketData];
//    [self addTestData];
}

- (void)addTestData
{
    for (int i = 0; i < 4 ; i++) {
        VoteItem *itemM = [[VoteItem alloc] init];
        itemM.voteContent = @"投票内容";
        itemM.voteId = @"123";
        [self.arrData addObject:itemM];
    }
}

- (void)sendSocketData
{
    [HDSocketSendCommandTool sendSocketParam:@{} commandCode:@(Vote) successBlock:^(id result) {
    }];
}
- (void)refreshOnSocketData:(NSNotification *)notif
{
    NSDictionary *dic = notif.userInfo;
    if ([dic[@"code"] integerValue] == VoteList) {
        NSArray *arr = [VoteItem mj_objectArrayWithKeyValuesArray:dic[@"data"]];
        [self.arrData removeAllObjects];
        [self.arrData addObjectsFromArray:arr];
        [self.tableView reloadData];
    }else if ([dic[@"code"] integerValue] == CreateVoteResponse){//新建投票是否成功
        
        
    }
}

#pragma mark ======== UITableView =======
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.arrData.count + 1 ;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 64.f;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    VoteListNewCell *newCell = [tableView dequeueReusableCellWithIdentifier:@"VoteListNewCell"];
    newCell.selectionStyle = UITableViewCellSelectionStyleNone;
    VoteListCell *listCell = [tableView dequeueReusableCellWithIdentifier:@"VoteListCell"];
    listCell.selectionStyle = UITableViewCellSelectionStyleNone;
    if (indexPath.row == 0) {
        return newCell;
    }else{
        VoteItem *model = self.arrData[indexPath.row-1];
        listCell.model = model;
        return listCell;
    }
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.row == 0) { //添加投票
        VoteNewVC *voteNewVC = [VoteNewVC new];
        [self.navigationController pushViewController:voteNewVC animated:YES];
    }else{ //查看
        VoteItem *itemM = self.arrData[indexPath.row - 1];
        VoteOptionDetailVC *voteNewVC = [VoteOptionDetailVC new];
        voteNewVC.voteItem = itemM;
        [self sendSocketVoteDetailData:itemM.voteId];
        [self.navigationController pushViewController:voteNewVC animated:YES];
    }
}

//获取投票详情
- (void)sendSocketVoteDetailData:(NSString *)voteId
{
    [HDSocketSendCommandTool sendSocketParam:@{@"voteId":voteId} commandCode:@(VoteDetails) successBlock:^(id result) {
    }];
}
- (void)goBackPage
{
    [HDSocketSendCommandTool sendSocketParam:@{} commandCode:@(CloseWindow) successBlock:^(id result) {
    }];
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)exitAction:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated:YES];
}



#pragma mark ======  accessory =====
- (NSMutableArray *)arrData
{
    if (!_arrData) {
        _arrData = [NSMutableArray array];
    }
    return _arrData;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
@end
