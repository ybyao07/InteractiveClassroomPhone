//
//  JBaseHttpUpLoader.h
//  PgtThirdParty
//
//  Created by HX on 2017/8/22.
//  Copyright © 2017年 Pugutang. All rights reserved.
//  上传图片基类，上传图片时不可直接使用，请使用 JHttpUpLoader.

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
@interface JBaseHttpUpLoader : NSObject


/**
 上传单图

 @param url 请求的url 字符串
 @param parameters 参数表 字典
 @param image 上传的图片 UIImage
 @param success 成功返回 未做解析的数据
 @param failure 错误返回 NSError
 */
+ (void)POST : (NSString *)url parameters : (NSMutableDictionary *)parameters image : (UIImage*)image success :(void (^)(id responseObject))success failure : (void (^)(NSError *error))failure;


/**
 上传多图

 @param url 请求的url 字符串
 @param parameters 参数表 字典
 @param images 上传的图片 数组  元素类型：UIImage
 @param success 成功返回 未做解析的数据
 @param failure 错误返回 NSError
 */
+ (void)POST : (NSString *)url parameters : (NSMutableDictionary *)parameters images : (NSMutableArray*)images success :(void (^)(id responseObject))success failure : (void (^)(NSError *error))failure;
@end
