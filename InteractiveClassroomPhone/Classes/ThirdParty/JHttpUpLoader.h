//
//  JHttpUpLoader.h
//  PgtThirdParty
//
//  Created by HX on 2017/8/22.
//  Copyright © 2017年 Pugutang. All rights reserved.
//  图片上传类，用于单张图片以及多张图片的上传、多图顺序上传

#import "JBaseHttpUpLoader.h"
#import <UIKit/UIKit.h>

#define IMAGE_URL @"https://www.pugutang.com/user/img"

@interface JHttpUpLoader : JBaseHttpUpLoader


#pragma mark --------上传单张图片--------
/**
 上传单张图片

 @param image 图片（UIImage*)
 @param success 成功返回 未做解析的数据
 @param failure 错误返回 NSError
 */
+ (void)upLoadImage : (UIImage *)image success :(void (^)(id responseObject))success failure : (void (^)(NSError *error))failure;

#pragma mark --------上传单张图片带参数--------
/**
 上传单张图片 带参数

 @param image 图片（UIImage*)
 @param parameter 参数表
 @param success 成功返回 未做解析的数据
 @param failure 错误返回 NSError
 */
+ (void)upLoadImage : (UIImage *)image parameters : (NSMutableDictionary *)parameter success :(void (^)(id responseObject))success failure : (void (^)(NSError *error))failure;

#pragma mark --------上传多张图片--------
/**
 上传多张图片

 @param images 图片（NSMutableArray)
 @param success 成功返回 未做解析的数据
 @param failure 错误返回 NSError
 */
+ (void)upLoadImages : (NSMutableArray *)images success :(void (^)(id responseObject))success failure : (void (^)(NSError *error))failure;

#pragma mark --------上传多张图片 带参数--------
/**
 上传多张图片 带参数

 @param images 图片（NSMutableArray)
 @param parameter 参数表
 @param success 成功返回 未做解析的数据
 @param failure 错误返回 NSError
 */
+ (void)upLoadImages : (NSMutableArray *)images parameters : (NSMutableDictionary *)parameter success :(void (^)(id responseObject))success failure : (void (^)(NSError *error))failure;

#pragma mark --------上传多张图片，每次只传一张，每一次的成功和失败都会返回--------
/**
 上传多张图片 带参数 每次只传一张 每一次的成功和失败都会返回
 
 @param images 图片（NSMutableArray)
 @param parameter 参数表
 @param success 成功返回 未做解析的数据
 @param failure 错误返回 NSError
 */
+ (void)upLoadImagesBySerial : (NSMutableArray *)images parameters : (NSMutableDictionary *)parameter success :(void (^)(id responseObject))success failure : (void (^)(NSError *error))failure;
@end
