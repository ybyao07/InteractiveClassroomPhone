//
//  JBaseHttpUpLoader.m
//  PgtThirdParty
//
//  Created by HX on 2017/8/22.
//  Copyright © 2017年 Pugutang. All rights reserved.
//

#import "JBaseHttpUpLoader.h"
#import "AFNetworking.h"
#import "AFHTTPRequestOperationManager.h"
@implementation JBaseHttpUpLoader

// 上传单图
+ (void)POST : (NSString *)url parameters : (NSMutableDictionary *)parameters image : (UIImage*)image success :(void (^)(id responseObject))success failure : (void (^)(NSError *error))failure
{
    AFHTTPRequestOperationManager *manager = [self getHttpManager];
    
    [manager POST:url parameters:nil constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        
        NSData  *imageData = UIImageJPEGRepresentation(image, 0.8);
        
        [formData appendPartWithFileData:imageData name:@"imgfile" fileName:[self getImageName] mimeType:@"image/jpeg"];
        
    } success:^(AFHTTPRequestOperation *operation, id responseObject) {
        success(responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure(error);
    }];
    
}

// 上传多图
+ (void)POST : (NSString *)url parameters : (NSMutableDictionary *)parameters images : (NSMutableArray*)images success :(void (^)(id responseObject))success failure : (void (^)(NSError *error))failure
{
    AFHTTPRequestOperationManager *manager = [self getHttpManager];
    
    [manager POST:url parameters:nil constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        for(UIImage *image in images)
        {
            
            NSData  *imageData = UIImageJPEGRepresentation(image, 0.8);
                
            NSString *fileName = [self getImageName];
                
            [formData appendPartWithFileData:imageData name:@"imgfile" fileName:fileName mimeType:@"image/jpeg"];
        }
        
    } success:^(AFHTTPRequestOperation *operation, id responseObject) {
        success(responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure(error);
    }];
}

// 得到 Http manager 及设置相关信息
+ (AFHTTPRequestOperationManager *) getHttpManager {
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    manager.requestSerializer.timeoutInterval = 20;
    
    return manager;
}

// 用时间来做为 上传图片的名称
+ (NSString*) getImageName {
    
    NSDateFormatter *format = [[NSDateFormatter alloc]init];
    format.dateFormat = @"yyyy-MM-dd HH:mm:ss";
    
    NSString *name = [NSString stringWithFormat:@"%@",[format stringFromDate:[NSDate date]]];
    
    return name;
}


@end
