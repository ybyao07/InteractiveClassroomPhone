//
//  PreViewCellModel.h
//  InteractiveClassroom
//
//  Created by 姚永波 on 2018/3/11.
//  Copyright © 2018年 姚永波. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PreViewCellModel : NSObject

@property (nonatomic, strong) UIImageView *imgView;
@property (nonatomic, assign) BOOL isSelected;

+ (instancetype)createWithImageView:(UIImageView *)imgV selected:(BOOL)selected;

@end
