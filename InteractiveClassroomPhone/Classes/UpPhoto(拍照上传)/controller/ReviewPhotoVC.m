//
//  ReviewPhotoVC.m
//  InteractiveClassroomPhone
//
//  Created by 姚永波 on 2018/3/28.
//  Copyright © 2018年 姚永波. All rights reserved.
//

#import "ReviewPhotoVC.h"
#import "UIImage+Stretch.h"

@interface ReviewPhotoVC ()

@property (weak, nonatomic) IBOutlet UIButton *preBtn;
@property (weak, nonatomic) IBOutlet UIButton *nextBtn;

@property (nonatomic, assign) NSInteger totalPage;
@property (nonatomic, assign) NSInteger currentIndex;

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@end

@implementation ReviewPhotoVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.scrollView.pagingEnabled = YES;
    self.scrollView.userInteractionEnabled = NO;
    self.currentIndex = 0;
    [self showLoadAnimation];
    [self initData];
    [self uploadImage];
}

- (void)initData
{
    self.scrollView.contentSize = CGSizeMake(SCREEN_WIDTH*self.modelArray.count, SCREEN_HEIGHT - 74 - 74);
    [self.modelArray enumerateObjectsUsingBlock:^(UIImage * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        CGRect pFrame = Rect(SCREEN_WIDTH*idx, 0, SCREEN_WIDTH, SCREEN_HEIGHT - 74 - 110);
        UIImageView *img = [[UIImageView alloc] initWithImage:obj];
        img.frame = pFrame;
        img.contentMode = UIViewContentModeScaleToFill;
        [self.scrollView addSubview:img];
    }];
    self.title = [NSString stringWithFormat:@"%d/%d",(int)self.currentIndex+1,(int)self.modelArray.count];
}

- (IBAction)preAction:(UIButton *)sender {
    if (self.currentIndex < 1) {
        return;
    }
    self.currentIndex--;
    self.title = [NSString stringWithFormat:@"%d/%d",(int)self.currentIndex+1,(int)self.modelArray.count];
    [self uploadImage];
    [self.scrollView setContentOffset:CGPointMake(self.currentIndex*SCREEN_WIDTH, 0) animated:YES];
    self.nextBtn.enabled = YES;
    if (self.currentIndex == 1) {
        self.preBtn.enabled = NO;
    }else{
        self.preBtn.enabled = YES;
    }
}
- (IBAction)nextAction:(UIButton *)sender {
    if (self.currentIndex == self.modelArray.count - 1) {
        return;
    }
    self.currentIndex++;
    self.title = [NSString stringWithFormat:@"%d/%d",(int)self.currentIndex+1,(int)self.modelArray.count];
    [self uploadImage];
    [self.scrollView setContentOffset:CGPointMake(self.currentIndex*SCREEN_WIDTH, 0) animated:YES];
    self.preBtn.enabled = YES;
    if (self.currentIndex == self.totalPage) {
        self.nextBtn.enabled = NO;
    }else{
        self.nextBtn.enabled = YES;
    }
}


#pragma mark ======= 上传UIImage =======

- (void)uploadImage
{
    [self showLoadAnimation];
    UIImage *image = self.modelArray[self.currentIndex];
    CGFloat expectWidth  = 0.;
    CGFloat expectHeight = 0;
    if (image.size.width > image.size.height) {
        expectWidth = 800;
        CGFloat imageWidth  = image.size.width;
        expectHeight= expectWidth / imageWidth * image.size.height;
    }else{
        expectHeight = 800;
        CGFloat imageHeight  = image.size.height;
        expectWidth = expectHeight / imageHeight * image.size.width;
    }
    UIImage *resizeImage = [image scaleToSize:CGSizeMake(expectWidth, expectHeight)];
    NSData *imageData = UIImageJPEGRepresentation(resizeImage,.8);
    NSLog(@"the lengt of image is %ld",imageData.length);
    NSMutableData *dataHeader = [NSMutableData data];
    NSData *dataPhoto = [@"photo" dataUsingEncoding:NSUTF8StringEncoding];
    [dataHeader appendData:dataPhoto];
    int curretPhotoPage = (int)self.currentIndex + 1;
    int listSize = (int)self.modelArray.count;
    [dataHeader appendData:[NSData dataWithBytes:&curretPhotoPage length:1]]; //当前页数
    [dataHeader appendData:[NSData dataWithBytes:&listSize length:1]]; //总页数
    [self sendImageDataHeader:dataHeader msgByte:imageData];
}


- (void)sendImageDataHeader:(NSMutableData *)header msgByte:(NSData *)data
{
    NSArray <NSData *> *list = [self getListIntArray:data length:1400];
    for (int i = 0; i < list.count; i++) {
        NSMutableData *sendBytes = [NSMutableData data];
        [sendBytes appendData:header];
        if (i == 0) {
            int dataStatus = 0;
            [sendBytes appendData:[NSData dataWithBytes:&dataStatus length:1]];
        }else if(i == (list.count -1)){
            int dataStatus = 2;
            [sendBytes appendData:[NSData dataWithBytes:&dataStatus length:1]];
            [self removeLoadAnimation];
        }else{
            int dataStatus = 1;
            [sendBytes appendData:[NSData dataWithBytes:&dataStatus length:1]];
        }
        [sendBytes appendData:list[i]];
        [[HDClient shareInstance] sendSynData:sendBytes];
    }
}

- (NSArray <NSData *> *)getListIntArray:(NSData *)dd length:(int)b{
    NSMutableArray<NSData *> *aa = [NSMutableArray array];
    //tyy 取整代表可以拆分的数组个数
    int f = (int)dd.length/b;
    int m = (int)dd.length%b;
    for (int i = 0; i < f; i++) {
        NSMutableData *bbb = [NSMutableData data];
        [bbb appendData:[dd subdataWithRange:NSMakeRange(i*b, b)]];
        [aa addObject:bbb];
    }
    if (m!=0) {
        NSMutableData *bbb = [NSMutableData data];
        [bbb appendData:[dd subdataWithRange:NSMakeRange(f*b,m)]];
        [aa addObject:bbb];
    }
    return aa;
}




- (IBAction)closePhoto:(UIButton *)sender {
    [HDSocketSendCommandTool sendSocketParam:@{} commandCode:@(CloseUI) successBlock:^(id result) {
    }];
    [self.navigationController popToRootViewControllerAnimated:YES];
}
- (void)goBackPage
{
    [HDSocketSendCommandTool sendSocketParam:@{} commandCode:@(CloseUI) successBlock:^(id result) {
    }];
    [self.navigationController popToRootViewControllerAnimated:YES];
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
