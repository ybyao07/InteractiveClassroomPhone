//
//  PreviewPhotoVC.m
//  InteractiveClassroomPhone
//
//  Created by 姚永波 on 2018/3/28.
//  Copyright © 2018年 姚永波. All rights reserved.
//

#import "PreviewPhotoVC.h"
#import "PhotoPreviewCell.h"
#import "ReviewPhotoVC.h"
#import "HelperToast.h"
@interface PreviewPhotoVC ()<UICollectionViewDelegate,UICollectionViewDataSource>

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

@property (strong, nonatomic) NSMutableArray <UIImage *> *selectedCellModel;

@end

@implementation PreviewPhotoVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"照片预览";
    [self.collectionView registerNib:[UINib nibWithNibName:@"PhotoPreviewCell" bundle:nil] forCellWithReuseIdentifier:@"PhotoPreviewCell"];
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setTitle:@"全选" forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(chooseAllAction:) forControlEvents:UIControlEventTouchUpInside];
    self.hd_navigationBar.rightButton = btn;
//    [self addTestData];
}


- (void)addTestData
{
    self.data = [NSMutableArray array];
    for (int i = 0; i < 100 ; i++) {
        PreViewCellModel *model = [[PreViewCellModel alloc] init];
        [self.data addObject:model];
    }
}


#pragma mark ================= collection delegate ==================
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.data.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    PhotoPreviewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"PhotoPreviewCell" forIndexPath:indexPath];
    cell.model = self.data[indexPath.row];
    return cell;
}
//定义每个UICollectionView 的大小
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake((SCREEN_WIDTH - 20 - 10)/2, (SCREEN_HEIGHT - 74 - 120 - 3 * 10)/4);
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return 5;
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return 5;
}
//UICollectionView被选中时调用的方法
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    PreViewCellModel *model = self.data[indexPath.row];
    model.isSelected = !model.isSelected;
    [self.collectionView reloadData];
}

- (void)chooseAllAction:(UIButton *)btn
{
    [self.data enumerateObjectsUsingBlock:^(PreViewCellModel * _Nonnull model, NSUInteger idx, BOOL * _Nonnull stop) {
        model.isSelected = YES;
    }];
    [self.collectionView reloadData];
}



- (IBAction)sendPhoto:(UIButton *)sender {
    //选中投屏的图片
    [self.selectedCellModel removeAllObjects];
    [self.data enumerateObjectsUsingBlock:^(PreViewCellModel * _Nonnull model, NSUInteger idx, BOOL * _Nonnull stop) {
        if(model.isSelected){
            [self.selectedCellModel addObject:model.imgView.image];
        }
    }];
    if (self.selectedCellModel.count < 1) {
        [HelperToast showMessageWithHud:@"请先选择要投屏的图片" addTo:self yOffset:0];
        return;
    }
    ReviewPhotoVC *throwVC = [ReviewPhotoVC new];
    throwVC.modelArray = self.selectedCellModel;
    [self.navigationController pushViewController:throwVC animated:YES];
}


- (IBAction)close:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated:YES];
}


- (NSMutableArray *)data
{
    if (!_data) {
        _data = [NSMutableArray array];
    }
    return _data;
}
- (NSMutableArray <UIImage *> *)selectedCellModel
{
    if (!_selectedCellModel) {
        _selectedCellModel = [NSMutableArray array];
    }
    return _selectedCellModel;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
