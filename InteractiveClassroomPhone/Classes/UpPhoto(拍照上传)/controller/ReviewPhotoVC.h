//
//  ReviewPhotoVC.h
//  InteractiveClassroomPhone
//
//  Created by 姚永波 on 2018/3/28.
//  Copyright © 2018年 姚永波. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YBaseViewController.h"

@interface ReviewPhotoVC : YBaseViewController

@property (strong, nonatomic) NSMutableArray <UIImage *> *modelArray;

@end
