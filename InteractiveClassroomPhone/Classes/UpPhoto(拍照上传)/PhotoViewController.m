//
//  PhotoViewController.m
//  InteractiveClassroomPhone
//
//  Created by 姚永波 on 2018/3/28.
//  Copyright © 2018年 姚永波. All rights reserved.
//

#import "PhotoViewController.h"
#import "DBAddPhotoView.h"
#import "PreviewPhotoVC.h"

#define kLeftMargin 20

@interface PhotoViewController ()<DBAddPhotoViewDelegate>

@property (strong, nonatomic) DBAddPhotoView *addPhotoView;

@property (strong, nonatomic) UIScrollView * scrollView;


@end

@implementation PhotoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"添加照片";
    [self addScrollView];
    [self addChildView];
}

-(void)addScrollView{
    _scrollView  = [[UIScrollView alloc] initWithFrame:CGRectMake(0,64, SCREEN_WIDTH, SCREEN_HEIGHT - 74 - 60)];
    _scrollView.backgroundColor = JUIColorFromRGB_bgGrayBlue;
    _scrollView.contentSize = CGSizeMake(0, 0);
    _scrollView.scrollEnabled = YES;
    [self.view addSubview:_scrollView];
}

-(void)addChildView{
    _addPhotoView = [[DBAddPhotoView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT- 74 - 60)];
    _addPhotoView.userInteractionEnabled = YES;
    _addPhotoView.delegate = self;
    [_scrollView addSubview:_addPhotoView];
    [_addPhotoView addObserver:self forKeyPath:@"frame" options:NSKeyValueObservingOptionNew context:nil];
}

#pragma mark =========  =======
- (IBAction)completeAction:(UIButton *)btn
{
    PreviewPhotoVC *preVC = [PreviewPhotoVC new];
    NSMutableArray *dataA = [NSMutableArray arrayWithCapacity:_addPhotoView.getWillUploadImageViews.count];
    [_addPhotoView.getWillUploadImageViews enumerateObjectsUsingBlock:^(UIImageView * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        PreViewCellModel * cellM = [PreViewCellModel createWithImageView:obj selected:NO];
        [dataA addObject:cellM];
    }];
    preVC.data = dataA;
    [self.navigationController pushViewController:preVC animated:YES];
}

- (IBAction)clearAction:(UIButton *)btn
{
    [_addPhotoView removeFromSuperview];
    _addPhotoView = [[DBAddPhotoView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT- 74 - 60)];
    _addPhotoView.userInteractionEnabled = YES;
    _addPhotoView.delegate = self;
    [_scrollView addSubview:_addPhotoView];
    [_addPhotoView addObserver:self forKeyPath:@"frame" options:NSKeyValueObservingOptionNew context:nil];
}

#pragma mark ------- KVO -----
-(void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSKeyValueChangeKey,id> *)change context:(void *)context
{
    HDLog(@"the frame has been changed:%@",NSStringFromCGRect(self.addPhotoView.frame));
    self.scrollView.contentSize = CGSizeMake(SCREEN_WIDTH, self.addPhotoView.frame.size.height);
}


#pragma mark ----- DBAddPhotoViewDelegate ----
- (void)photoViewClick:(DBAddPhotoView *)photoView imageArray:(NSArray *)imageArray photoViewHight:(CGFloat)hight
{
//    [self.noPicAddView removeFromSuperview];
}

- (void)goBackPage
{
    [HDSocketSendCommandTool sendSocketParam:@{} commandCode:@(CloseUI) successBlock:^(id result) {
    }];
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (void)dealloc
{
    [_addPhotoView removeObserver:self forKeyPath:@"frame"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
