//
//  upload.h
//  WKCarInsurance
//
//  Created by 姚永波 on 2017/9/26.
//  Copyright © 2017年 wkbins. All rights reserved.
//

#ifndef upload_h
#define upload_h

//间距
#define spaceLeft 10
#define spaceTop 10


#define space 10
#define spaceVertical 10

//一行几个数
#define ColumnCount 2
//能显示几行
#define RowCount 4

#define tableViewRowHeight 80.0f

#endif /* upload_h */
