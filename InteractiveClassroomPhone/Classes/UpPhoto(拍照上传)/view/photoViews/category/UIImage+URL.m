//
//  UIImage+URL.m
//  TestMultiPhoto
//
//  Created by 姚永波 on 2017/9/25.
//  Copyright © 2017年 姚永波. All rights reserved.
//

#import "UIImage+URL.h"
#import <objc/runtime.h>

static char kYBImageURLKey;

@implementation UIImage (URL)

@dynamic url;

- (void)setUrl:(NSString *)url
{
    objc_setAssociatedObject(self, &kYBImageURLKey, url, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}
- (NSString *)url
{
    return objc_getAssociatedObject(self, &kYBImageURLKey);
}

@end
