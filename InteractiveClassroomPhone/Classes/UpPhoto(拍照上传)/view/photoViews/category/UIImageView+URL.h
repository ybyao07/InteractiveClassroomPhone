//
//  UIImageView+URL.h
//  TestMultiPhoto
//
//  Created by 姚永波 on 2017/9/22.
//  Copyright © 2017年 姚永波. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImageView (URL)

@property (nonatomic, strong) NSString *url;

@end
