//
//  UIImage+URL.h
//  TestMultiPhoto
//
//  Created by 姚永波 on 2017/9/25.
//  Copyright © 2017年 姚永波. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (URL)

@property (nonatomic, strong) NSString *url;

@end
