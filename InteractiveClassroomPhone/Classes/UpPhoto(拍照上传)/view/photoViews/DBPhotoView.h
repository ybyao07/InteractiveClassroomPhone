//
//  DBPhotoView.h
//  danbai_client_ios
//
//  Created by dbjyz on 15/7/6.
//  Copyright (c) 2015年 db. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DBPhotoView : UIView

@property (nonatomic, copy) void (^HandleAddLocation)();
@property (nonatomic, assign) int addimageCount;
/**
 *  添加一张新的图片
 */
- (void)addImage:(UIImage *)image;

/**
 *  返回内部所有的图片
 */
- (NSArray *)totalImages;


- (NSArray *)totalImageViews;


@end
