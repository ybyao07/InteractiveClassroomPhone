//
//  HD_TeamCompetionVC.m
//  InteractiveClassroomPhone
//
//  Created by 姚永波 on 2018/3/29.
//  Copyright © 2018年 姚永波. All rights reserved.
//

#import "HD_TeamCompetionVC.h"
#import "HD_StartCompeteVC.h"
#import "TeamGroupModel.h"
#import "TeamGroupSectionCell.h"
#import "TypeListView.h"

@interface HD_TeamCompetionVC ()<UITableViewDataSource,UITableViewDelegate,TypeListViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (weak, nonatomic) IBOutlet UIButton *btnGroupType;

@property (nonatomic, strong) NSArray *dataGroups;
@property (strong, nonatomic) NSMutableArray <SmallGroupModel *> *resources;

@property (strong, nonatomic) TeamGroupModel *currentGroupM;

@end

@implementation HD_TeamCompetionVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"团队竞赛";
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshViewSockentData:) name:kHDNotificationCenterSocket object:nil];
    [HDSocketSendCommandTool sendSocketParam:@{} commandCode:@(GroupCompetition) successBlock:^(id result) {
    }];
    [self.tableView registerNib:[UINib nibWithNibName:@"TeamGroupSectionCell" bundle:nil] forCellReuseIdentifier:@"TeamGroupSectionCell"];
    self.tableView.separatorInset = UIEdgeInsetsMake(0, 10, 0, 10);
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
//    [self addTestData];
}


- (void)addTestData
{
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    dic[@"data"] = @[
                     @{
                         @"classId":@"15",
                         @"groupName":@"预设分组1",
                         @"id":@"22",
                         @"isPresent":@(1),
                         @"smallGroupsInfo":
                             @[
                                 @{
                                     @"StarCount":@(0),
                                     @"smallGroup":@"A",
                                     @"studentList":@[
                                             @{
                                                 @"isLeader":@"1",
                                                 @"studentId":@"124",
                                                 @"studentName":@"张三"
                                                 },
                                             @{
                                                 @"isLeader":@"0",
                                                 @"studentId":@"125",
                                                 @"studentName":@"李四"
                                                 }, @{
                                                 @"isLeader":@"1",
                                                 @"studentId":@"124",
                                                 @"studentName":@"张三"
                                                 },
                                             @{
                                                 @"isLeader":@"0",
                                                 @"studentId":@"125",
                                                 @"studentName":@"李四"
                                                 }, @{
                                                 @"isLeader":@"1",
                                                 @"studentId":@"124",
                                                 @"studentName":@"张三"
                                                 },
                                             @{
                                                 @"isLeader":@"0",
                                                 @"studentId":@"125",
                                                 @"studentName":@"李四"
                                                 }]
                                     },
                                 @{
                                     @"StarCount":@(0),
                                     @"smallGroup":@"B",
                                     @"studentList":@[
                                             @{
                                                 @"isLeader":@"1",
                                                 @"studentId":@"126",
                                                 @"studentName":@"王五"
                                                 }]
                                     }
                                 ]
                         },
                     @{
                         @"classId":@"15",
                         @"groupName":@"预设分组2",
                         @"id":@"22",
                         @"isPresent":@(1),
                         @"smallGroupsInfo":
                             @[
                                 @{
                                     @"StarCount":@(0),
                                     @"smallGroup":@"B",
                                     @"studentList":@[
                                             @{
                                                 @"isLeader":@"1",
                                                 @"studentId":@"124",
                                                 @"studentName":@"张三"
                                                 },
                                             @{
                                                 @"isLeader":@"0",
                                                 @"studentId":@"125",
                                                 @"studentName":@"李四"
                                                 }]
                                     },
                                 @{
                                     @"StarCount":@(0),
                                     @"smallGroup":@"B",
                                     @"studentList":@[
                                             @{
                                                 @"isLeader":@"1",
                                                 @"studentId":@"126",
                                                 @"studentName":@"王五"
                                                 }]
                                     }
                                 ]
                         }];
    
    self.dataGroups = [TeamGroupModel mj_objectArrayWithKeyValuesArray:dic[@"data"]];
    self.currentGroupM = self.dataGroups.firstObject;
    [self.resources removeAllObjects];
    [self.resources addObjectsFromArray:self.currentGroupM.smallGroupsInfo];
    //    [self.collectionView reloadData];
}
- (void)refreshViewSockentData:(NSNotification *)notif
{
    NSDictionary *dic = notif.userInfo;
    if ([dic[@"code"] integerValue] == GroupsInfo) {
        self.dataGroups = [TeamGroupModel mj_objectArrayWithKeyValuesArray:dic[@"data"]];
        self.currentGroupM = self.dataGroups.firstObject;
        [self.btnGroupType setTitle:[NSString stringWithFormat:@"当前：%@",self.currentGroupM.groupName] forState:UIControlStateNormal];
        [self.resources removeAllObjects];
        [self.resources addObjectsFromArray:self.currentGroupM.smallGroupsInfo];
        [self.tableView reloadData];
    }
}
#pragma mark ==== UITableViewDelegate ======
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.resources.count;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SmallGroupModel *model = self.resources[indexPath.section];
    int line = (int) model.studentList.count / 4;
    if ( line >= 1) {
        return  60 + (line + 1) *46;
    }
    return 60 + 46 + 10;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    TeamGroupSectionCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TeamGroupSectionCell"];
    SmallGroupModel *model = self.resources[indexPath.section];
    cell.model = model;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}


- (IBAction)moreList:(UIButton *)btn
{
    TypeListView *moreView = [[TypeListView alloc] init];
    moreView.delegate = self;
    NSMutableArray *popArray = [NSMutableArray array];
    [self.dataGroups enumerateObjectsUsingBlock:^(TeamGroupModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        [popArray addObject:obj.groupName];
    }];
    [moreView updateViewWithDatas:popArray];
}

#pragma mark ===== TypeListViewDelegate =======
- (void)typeListView:(TypeListView *)view didSelectIndex:(int)typeIndex
{
    self.currentGroupM = self.dataGroups[typeIndex];
    [self.btnGroupType setTitle:[NSString stringWithFormat:@"当前：%@",self.currentGroupM.groupName] forState:UIControlStateNormal];
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    dic[@"groupId"] = self.currentGroupM.groupId;
    [HDSocketSendCommandTool sendSocketParam:dic commandCode:@(CurrentGroupInfo) successBlock:^(id result) {
        [self.resources removeAllObjects];
        [self.resources addObjectsFromArray:self.currentGroupM.smallGroupsInfo];
        [self.tableView reloadData];
    }];
    [self.resources removeAllObjects];
    [self.resources addObjectsFromArray:self.currentGroupM.smallGroupsInfo];
    [self.tableView reloadData];
}

- (IBAction)goStartAction:(UIButton *)sender {
    HD_StartCompeteVC *startVC = [HD_StartCompeteVC new];
    startVC.model = self.currentGroupM;
    [self.navigationController pushViewController:startVC animated:YES];
}

- (IBAction)exitAction:(UIButton *)sender {
    [HDSocketSendCommandTool sendSocketParam:@{} commandCode:@(CloseUI) successBlock:^(id result) {
    }];
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (void)goBackPage
{
    [HDSocketSendCommandTool sendSocketParam:@{} commandCode:@(CloseUI) successBlock:^(id result) {
    }];
    [self.navigationController popToRootViewControllerAnimated:YES];
}

#pragma mark ===== accessory =====
- (NSMutableArray <SmallGroupModel *> *)resources
{
    if (!_resources) {
        _resources = [NSMutableArray array];
    }
    return _resources;
}


- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
