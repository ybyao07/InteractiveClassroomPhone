//
//  HD_StartCompeteVC.h
//  InteractiveClassroomPhone
//
//  Created by 姚永波 on 2018/4/15.
//  Copyright © 2018年 姚永波. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YBaseViewController.h"
#import "TeamGroupModel.h"

@interface HD_StartCompeteVC : YBaseViewController

@property (strong, nonatomic) TeamGroupModel *model;

@end
