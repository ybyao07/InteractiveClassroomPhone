//
//  TeamNoAnswerVC.m
//  InteractiveClassroomPhone
//
//  Created by 姚永波 on 2018/4/15.
//  Copyright © 2018年 姚永波. All rights reserved.
//

#import "TeamNoAnswerVC.h"
#import "AlertScreenView.h"

@interface TeamNoAnswerVC ()


@end

@implementation TeamNoAnswerVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"团队竞赛";
}

// 重新抢答
- (IBAction)reQucickAgain:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)closeAction:(UIButton *)sender {
    AlertScreenView *alertView = [[AlertScreenView alloc] initWithTitle:@"是否结束本次竞赛？" message:@"" delegate:self cancelButtonTitle:@"否" otherButtonTitle:@"是"];
    [alertView show];
}

- (void)goBackPage
{
    AlertScreenView *alertView = [[AlertScreenView alloc] initWithTitle:@"是否结束本次竞赛赛？" message:@"" delegate:self cancelButtonTitle:@"否" otherButtonTitle:@"是"];
    [alertView show];
}

#pragma mark ==== AlertScreenViewDelegate ===
- (void)alertScreenView:(AlertScreenView *)view clickedButtonAtIndex:(NSInteger)tag
{
    if (tag == 1) {
        [HDSocketSendCommandTool sendSocketParam:@{} commandCode:@(CloseUI) successBlock:^(id result) {
        }];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"CloseTeamCompetion" object:nil];
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
