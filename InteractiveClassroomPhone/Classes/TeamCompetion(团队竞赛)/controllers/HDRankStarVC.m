//
//  HDRankStarVC.m
//  InteractiveClassroomPhone
//
//  Created by 姚永波 on 2018/4/15.
//  Copyright © 2018年 姚永波. All rights reserved.
//

#import "HDRankStarVC.h"
#import "TeamStarModel.h"
#import "YBBarCharVerticalView.h"
#import "AlertScreenView.h"

@interface HDRankStarVC ()<UIAlertViewDelegate>

@property (strong, nonatomic) NSArray <TeamStarModel *> *dataArr;
@property (weak, nonatomic) IBOutlet UIView *containerView;

@end

@implementation HDRankStarVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title  = @"排行榜";
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshViewSockentData:) name:kHDNotificationCenterSocket object:nil];
    [self sendSocketData];
}

- (void)addTestData
{
    NSDictionary *dic = @{
                          @"smallGroupsInfo":
                              @[@{
                                    @"smallGroup":@"A",
                                    @"StarCount":@(5)
                                    },
                                @{
                                    @"smallGroup":@"B",
                                    @"StarCount":@(4)
                                    }]
                          };
    _dataArr = [TeamStarModel mj_objectArrayWithKeyValuesArray:dic[@"smallGroupsInfo"]];
    self.dataArr = _dataArr;
}

- (void)refreshViewSockentData:(NSNotification *)notif
{
    NSDictionary *dic = notif.userInfo;
    if ([dic[@"code"] integerValue] == GroupAnswerRankingInfo) {
       NSArray *data = [TeamStarModel mj_objectArrayWithKeyValuesArray:dic[@"smallGroupsInfo"]];
        self.dataArr = data;
    }
}

- (void)setDataArr:(NSArray<TeamStarModel *> *)dataArr
{
    _dataArr = dataArr;
    NSMutableArray *xlabels = [NSMutableArray arrayWithCapacity:dataArr.count];
    NSMutableArray *yValues = [NSMutableArray arrayWithCapacity:dataArr.count];
    NSMutableArray *colors = [NSMutableArray arrayWithCapacity:dataArr.count];
    __block int index = 0;
    __block int maxValue = 0;
    __block int noValue = 0;
    [_dataArr enumerateObjectsUsingBlock:^(TeamStarModel * _Nonnull starM, NSUInteger idx, BOOL * _Nonnull stop) {
        [xlabels addObject:[NSString stringWithFormat:@"小组%@",starM.smallGroup]];
        [yValues addObject:[NSString stringWithFormat:@"%d星",(int)[starM.StarCount integerValue]]];
        [colors addObject:JUIColorFromRGB_Main3];
        if ([starM.smallGroup isEqualToString:@"X"]||[starM.smallGroup isEqualToString:@"x"]) {
            noValue = [starM.StarCount intValue];
            [colors replaceObjectAtIndex:idx withObject:JUIColorFromRGB_Gray2];//灰色
            [xlabels replaceObjectAtIndex:idx withObject:@"无效"];
        }else{
            if ([starM.StarCount intValue]  > maxValue) {
                maxValue = [starM.StarCount intValue];
                index = (int)idx;
            }
        }
    }];
    [colors replaceObjectAtIndex:index withObject:JUIColorFromRGB_Major1];
    CGRect frame = CGRectMake(50, 100.0, SCREEN_WIDTH-100, SCREEN_HEIGHT - 400);
    CGFloat startX = 0;
    startX = ((SCREEN_WIDTH - 100) - _dataArr.count*40 - (_dataArr.count - 1)*40)/2.0;
    CGPoint startPoint = CGPointMake(startX, 0);
    YBBarCharVerticalView *barChartView = [[YBBarCharVerticalView alloc] initWithFrame:frame
                                                                            startPoint:startPoint
                                                                                values:yValues
                                                                                colors:colors
                                                                              maxValue:maxValue > noValue?maxValue:noValue
                                                                        textIndicators:xlabels
                                                                             textColor:[UIColor blackColor]
                                                                              barWidth:30
                                                                          barMaxHeight:SCREEN_HEIGHT-400];
    
    
    [self.containerView addSubview:barChartView];
}

- (void)sendSocketData
{
    [HDSocketSendCommandTool sendSocketParam:@{} commandCode:@(GroupAnswerRanking) successBlock:^(id result) {
    }];
}

- (IBAction)closeAction:(UIButton *)sender {
    AlertScreenView *alertView = [[AlertScreenView alloc] initWithTitle:@"是否结束本次竞赛？" message:@"" delegate:self cancelButtonTitle:@"否" otherButtonTitle:@"是"];
    [alertView show];
}

#pragma mark ==== AlertScreenViewDelegate ===
- (void)alertScreenView:(AlertScreenView *)view clickedButtonAtIndex:(NSInteger)tag
{
    if (tag == 1) {
        [HDSocketSendCommandTool sendSocketParam:@{} commandCode:@(CloseUI) successBlock:^(id result) {
        }];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"CloseTeamCompetion" object:nil];
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
}

- (void)goBackPage{
    [HDSocketSendCommandTool sendSocketParam:@{} commandCode:@(ReturnGroupAnswer) successBlock:^(id result) {
    }];
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
