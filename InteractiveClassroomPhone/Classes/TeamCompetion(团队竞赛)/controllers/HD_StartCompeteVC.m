//
//  HD_StartCompeteVC.m
//  InteractiveClassroomPhone
//
//  Created by 姚永波 on 2018/4/15.
//  Copyright © 2018年 姚永波. All rights reserved.
//

#import "HD_StartCompeteVC.h"
#import "PingJiaXingjiView.h"
#import "TeamGroupModel.h"
#import "TeamNoAnswerVC.h"
#import "TeamAllAnswerVC.h"

@interface HD_StartCompeteVC ()
{
    long _currentStarLevel;
}
@property (weak, nonatomic) IBOutlet UIView *starViews;

@property (weak, nonatomic) IBOutlet PingJiaXingjiView *starView;
@property (weak, nonatomic) IBOutlet UIButton *btnStart;
@property (weak, nonatomic) IBOutlet UIButton *btnGo;

@property (weak, nonatomic) IBOutlet UILabel *lblTitle;

@property (weak, nonatomic) IBOutlet UIButton *btnMinus;
@property (weak, nonatomic) IBOutlet UIButton *btnAdd;

//@property (strong, nonatomic) TeamGroupModel *model;


@end

static const NSInteger  starLevelMin = 1;
static const NSInteger  starLevelMax = 5;

@implementation HD_StartCompeteVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"团队竞赛";
    self.starView.space = 6;
    self.starView.userInteractionEnabled = NO;
    _btnGo.hidden = YES;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshViewSockentData:) name:kHDNotificationCenterSocket object:nil];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    _currentStarLevel= 1;
    self.starView.show_star = _currentStarLevel;
    self.starViews.hidden = NO;
    self.btnStart.hidden = NO;
    self.btnGo.hidden = YES;
    self.lblTitle.text = @"倒计时结束时，按下答题键开始抢答！";
}

- (void)refreshViewSockentData:(NSNotification *)notif
{
    NSDictionary *dic = notif.userInfo;
    if ([dic[@"code"] integerValue] == GroupAnswerState) {
        SmallGroupModel *groupM = [SmallGroupModel mj_objectWithKeyValues:dic];
        if (groupM.studentName.length > 0) {//有人抢答
            TeamAllAnswerVC *rankVC = [TeamAllAnswerVC new];
            rankVC.model = groupM;
            [self.navigationController pushViewController:rankVC animated:YES];
        }else{ // 无人抢答
            TeamNoAnswerVC *noVC = [TeamNoAnswerVC new];
            [self.navigationController pushViewController:noVC animated:YES];
        }
    }
}

- (IBAction)decreaseClicked:(UIButton *)sender {
    _currentStarLevel = _currentStarLevel - 1;
    if (_currentStarLevel <= starLevelMin) {
        _currentStarLevel = starLevelMin;
    }
    self.starView.show_star = _currentStarLevel;
}
- (IBAction)increaseClicked:(UIButton *)sender {
    _currentStarLevel = _currentStarLevel + 1;
    if (_currentStarLevel >= starLevelMax) {
        _currentStarLevel = starLevelMax;
    }
    self.starView.show_star = _currentStarLevel;
}

- (IBAction)goAction:(UIButton *)sender {
    self.starViews.hidden = YES;
    self.btnStart.hidden = YES;
    self.btnGo.hidden = NO;
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    dic[@"groupId"] = self.model.groupId;
    dic[@"starValue"] = [NSString stringWithFormat:@"%d",(int)self.starView.show_star];
    [HDSocketSendCommandTool sendSocketParam:dic commandCode:@(StartGroupAnswer) successBlock:^(id result) {
        
    }];
    [self performSelector:@selector(updateLabel) withObject:nil afterDelay:3.0];
    NSArray *imgs = [NSArray arrayWithObjects:[UIImage imageNamed:@"quick_three"],[UIImage imageNamed:@"quick_two"],[UIImage imageNamed:@"quick_one"], nil];
    UIImageView *imgView = [UIImageView new];
    imgView.animationImages = imgs;
    imgView.animationDuration = 3.0;
    imgView.animationRepeatCount = 1;
    imgView.frame = Rect(0, 0, self.btnGo.bounds.size.width, self.btnGo.bounds.size.height);
    [self.btnGo addSubview:imgView];
    [imgView startAnimating];
#pragma warning test
//        TeamAllAnswerVC *rankVC = [TeamAllAnswerVC new];
//        [self.navigationController pushViewController:rankVC animated:YES];
    //    TeamNoAnswerVC *noVC = [TeamNoAnswerVC new];
    //    [[self getCurrentViewController].navigationController pushViewController:noVC animated:YES];
    //    TeamAllAnswerVC *rankVC = [TeamAllAnswerVC new];
    //    rankVC.model = self.model.smallGroupsInfo.firstObject;
    //    [[self getCurrentViewController].navigationController pushViewController:rankVC animated:YES];
}


- (IBAction)exitAction:(UIButton *)sender {
    [HDSocketSendCommandTool sendSocketParam:@{} commandCode:@(CloseUI) successBlock:^(id result) {
    }];
    [self.navigationController popToRootViewControllerAnimated:YES];
}


- (void)updateLabel
{
    self.lblTitle.text = @"按下答题键开始抢答！";
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
