//
//  TeamStudent.h
//  InteractiveClassroom
//
//  Created by 姚永波 on 2018/3/17.
//  Copyright © 2018年 姚永波. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TeamStudent : NSObject

@property (nonatomic, copy) NSString *isLeader;
@property (nonatomic, copy) NSString *studentId;
@property (nonatomic, copy) NSString *studentName;
@end
