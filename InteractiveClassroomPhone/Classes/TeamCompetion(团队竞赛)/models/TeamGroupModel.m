//
//  TeamGroupModel.m
//  InteractiveClassroom
//
//  Created by 姚永波 on 2018/3/17.
//  Copyright © 2018年 姚永波. All rights reserved.
//

#import "TeamGroupModel.h"

@implementation TeamGroupModel

+ (NSDictionary *)mj_replacedKeyFromPropertyName{
    return @{
             @"groupId"     :@"id",
             };
}

+ (NSDictionary *)mj_objectClassInArray {
    return @{
             @"smallGroupsInfo" : [SmallGroupModel class],
             };
}



@end
