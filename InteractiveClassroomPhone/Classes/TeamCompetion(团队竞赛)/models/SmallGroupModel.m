//
//  SmallGroupModel.m
//  InteractiveClassroom
//
//  Created by 姚永波 on 2018/3/17.
//  Copyright © 2018年 姚永波. All rights reserved.
//

#import "SmallGroupModel.h"

@implementation SmallGroupModel

+ (NSDictionary *)mj_objectClassInArray {
    return @{
             @"studentList" : [TeamStudent class],
             };
}
@end
