//
//  SmallGroupModel.h
//  InteractiveClassroom
//
//  Created by 姚永波 on 2018/3/17.
//  Copyright © 2018年 姚永波. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TeamStudent.h"
@interface SmallGroupModel : NSObject

@property (nonatomic, copy) NSString *smallGroup;
@property (nonatomic, strong) NSNumber *StarCount;
@property (nonatomic, copy) NSString *studentName;
//@property (nonatomic, copy) NSString *studentId;

@property (nonatomic, strong) NSArray <TeamStudent *> *studentList;

@end

