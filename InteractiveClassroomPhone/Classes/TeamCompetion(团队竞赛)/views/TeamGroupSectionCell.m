//
//  TeamGroupSectionCell.m
//  InteractiveClassroomPhone
//
//  Created by 姚永波 on 2018/4/15.
//  Copyright © 2018年 姚永波. All rights reserved.
//

#import "TeamGroupSectionCell.h"
#import "TestStudentProgressCell.h"

@implementation TeamGroupSectionCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    [self.collectionView registerNib:[UINib nibWithNibName:@"TestStudentProgressCell" bundle:nil] forCellWithReuseIdentifier:@"TestStudentProgressCell"];
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    self.constraitW.constant = (SCREEN_WIDTH - 20 - 10*4)/4;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setModel:(SmallGroupModel *)model
{
    _model = model;
    _lblSmallGroupName.text = [NSString stringWithFormat:@"小组%@ ",model.smallGroup];
    [self.collectionView reloadData];
}


#pragma mark ================= collection delegate ==================
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.model.studentList.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    TestStudentProgressCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"TestStudentProgressCell" forIndexPath:indexPath];
    TeamStudent *model = self.model.studentList[indexPath.row];
    cell.backgroundColor = JUIColorFromRGB_Major1;
    cell.lblName.text = model.studentName;
    return cell;
}
//定义每个UICollectionView 的大小
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake((SCREEN_WIDTH - 20 - 10*4)/4, 36);
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return 5;
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return 5;
}

@end
