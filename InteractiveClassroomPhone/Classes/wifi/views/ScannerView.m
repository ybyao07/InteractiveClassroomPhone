//
//  ScannerView.m
//  InteractiveClassroom
//
//  Created by 姚永波 on 2018/2/11.
//  Copyright © 2018年 姚永波. All rights reserved.
//

#import "ScannerView.h"

#define PHONE_SCALE_PARAMETER [UIScreen mainScreen].scale

#define SCAN_BOX_WIDTH 150*PHONE_SCALE_PARAMETER

@interface ScannerView()
{
    UIImageView *targetView;
    UIImageView *line;
    int num;
    BOOL upOrdown;
    NSTimer * timer;
}
@end

@implementation ScannerView
- (id)initWithFrame:(CGRect)frame
{
    self  = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = UIColorFromRGBA(0x000000, 0.6);
        targetView = [[UIImageView alloc] init];
        targetView.frame = CGRectMake(0, 0,SCREEN_WIDTH - 100 ,SCREEN_WIDTH - 100);
        targetView.center = CGPointMake(self.center.x, self.center.y);
        
        UIImageView *scanBorder = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"scan_border"]];
        scanBorder.frame = Rect(0, 0, SCREEN_WIDTH - 100, SCREEN_WIDTH - 100);
        [targetView addSubview:scanBorder];
        
        line = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"scan_line"]];
        line.frame = Rect(0,0, SCREEN_WIDTH - 100, 3);
        [targetView addSubview:line];
        [self addSubview:targetView];
    }
    return self;
}


- (CGRect)getBoxFrame{
    return targetView.frame;
}
- (void)beginLineAnimation{
    [timer invalidate];
    timer = nil;
    num = 0;
    upOrdown = NO;
    if(!timer){
        timer = [NSTimer scheduledTimerWithTimeInterval:.06 target:self selector:@selector(lineAnimation) userInfo:nil repeats:YES];
    }
}
- (void)stopLineAnimation{
    [timer invalidate];
    timer = nil;
}

- (void)lineAnimation{
    float orginX = line.frame.origin.x;
    float width = line.frame.size.width;
#define step 4
    if (upOrdown == NO) {
        num ++;
        if (step*num >(SCREEN_WIDTH - 100 - line.frame.size.height)) {
            upOrdown = YES;
        }
    }else{
        num --;
        if (num < 0) {
            upOrdown = NO;
        }
    }
    line.frame = CGRectMake(orginX,step*num, width, 6);
}
@end
