//
//  ScannerView.h
//  InteractiveClassroom
//
//  Created by 姚永波 on 2018/2/11.
//  Copyright © 2018年 姚永波. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ScannerView : UIView

- (void)beginLineAnimation;
- (void)stopLineAnimation;

- (CGRect)getBoxFrame;

@end
