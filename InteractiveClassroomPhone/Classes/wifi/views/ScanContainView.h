//
//  ScanContainView.h
//  InteractiveClassroom
//
//  Created by 姚永波 on 2018/2/11.
//  Copyright © 2018年 姚永波. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

@protocol ScanContaiViewDelegate <NSObject>

- (void)scanContainViewBack;

@end

@interface ScanContainView : UIView

@property (nonatomic, weak) id<ScanContaiViewDelegate> delegate;

@end
