//
//  HD_AllMemerAnswerVC.m
//  InteractiveClassroomPhone
//
//  Created by 姚永波 on 2018/3/29.
//  Copyright © 2018年 姚永波. All rights reserved.
//

#import "HD_AllMemerAnswerVC.h"
#import "TeamStudent.h"
#import "QuickAnswerResultVC.h"

@interface HD_AllMemerAnswerVC ()<QuickAnswerResultViewDelegate>

@property (weak, nonatomic) IBOutlet UILabel *lblTitle;


@property (weak, nonatomic) IBOutlet UIButton *btnStart;
@property (weak, nonatomic) IBOutlet UIButton *btnGo;

@end

@implementation HD_AllMemerAnswerVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"全员抢答";
    [HDSocketSendCommandTool sendSocketParam:@{} commandCode:@(QuickAnswer) successBlock:^(id result) {
    }];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshViewSockentData:) name:kHDNotificationCenterSocket object:nil];
    _btnGo.hidden = YES;
    _btnStart.hidden = NO;
    _lblTitle.text = @"倒计时结束时，\n按下答题键开始抢答！";
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}
- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(updateLabel) object:nil];
}
- (void)refreshViewSockentData:(NSNotification *)notif
{
    NSDictionary *dic = notif.userInfo;
    if ([dic[@"code"] integerValue] == QuickAnswerState) {
        //跳转到抢答成功或者抢答失败
        TeamStudent *studentM = [TeamStudent mj_objectWithKeyValues:dic];
        QuickAnswerResultVC *resultVC = [QuickAnswerResultVC new];
        resultVC.model = studentM;
        resultVC.delegate = self;
        [self.navigationController pushViewController:resultVC animated:YES];
    }
}
- (IBAction)startAction:(UIButton *)sender {
    self.btnStart.hidden = YES;
    self.btnGo.hidden = NO;
    [HDSocketSendCommandTool sendSocketParam:@{} commandCode:@(StartQuickAnswer) successBlock:^(id result) {
    }];
    [self performSelector:@selector(updateLabel) withObject:nil afterDelay:3.0];
    NSArray *imgs = [NSArray arrayWithObjects:[UIImage imageNamed:@"quick_three"],[UIImage imageNamed:@"quick_two"],[UIImage imageNamed:@"quick_one"], nil];
    UIImageView *imgView = [UIImageView new];
    imgView.animationImages = imgs;
    imgView.animationDuration = 3;
    imgView.animationRepeatCount = 1;
    imgView.frame = Rect(0, 0, self.btnGo.bounds.size.width, self.btnGo.bounds.size.height);
    [self.btnGo addSubview:imgView];
    [imgView startAnimating];
}

#pragma mark === QuickAnswerResultViewDelegate =====
- (void)closeView
{
    _lblTitle.text = @"倒计时结束时，\n按下答题键开始抢答！";
    self.btnStart.hidden = YES;
    self.btnGo.hidden = NO;
    [HDSocketSendCommandTool sendSocketParam:@{} commandCode:@(StartQuickAnswer) successBlock:^(id result) {
    }];
    [self performSelector:@selector(updateLabel) withObject:nil afterDelay:3.0];
    NSArray *imgs = [NSArray arrayWithObjects:[UIImage imageNamed:@"quick_three"],[UIImage imageNamed:@"quick_two"],[UIImage imageNamed:@"quick_one"], nil];
    UIImageView *imgView = [UIImageView new];
    imgView.animationImages = imgs;
    imgView.animationDuration = 3;
    imgView.animationRepeatCount = 1;
    imgView.frame = Rect(0, 0, self.btnGo.bounds.size.width, self.btnGo.bounds.size.height);
    [self.btnGo addSubview:imgView];
    [imgView startAnimating];
}

- (void)updateLabel
{
    self.lblTitle.text = @"按下答题键开始抢答！";
}

- (void)goBackPage{
    [HDSocketSendCommandTool sendSocketParam:@{} commandCode:@(CloseWindow) successBlock:^(id result) {
    }];
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (IBAction)closeAction:(UIButton *)sender {
    [HDSocketSendCommandTool sendSocketParam:@{} commandCode:@(CloseWindow) successBlock:^(id result) {
    }];
    [self.navigationController popToRootViewControllerAnimated:YES];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
