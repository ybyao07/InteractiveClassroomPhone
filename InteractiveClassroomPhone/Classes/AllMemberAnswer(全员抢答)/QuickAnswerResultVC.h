//
//  QuickAnswerResultVC.h
//  InteractiveClassroomPhone
//
//  Created by 姚永波 on 2018/4/15.
//  Copyright © 2018年 姚永波. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YBaseViewController.h"
#import "TeamStudent.h"

@protocol QuickAnswerResultViewDelegate<NSObject>

- (void)closeView;

@end

@interface QuickAnswerResultVC : YBaseViewController

@property (nonatomic, strong) TeamStudent *model;

@property (nonatomic, weak) id<QuickAnswerResultViewDelegate> delegate;

@end
