 //
//  QuickAnswerResultVC.m
//  InteractiveClassroomPhone
//
//  Created by 姚永波 on 2018/4/15.
//  Copyright © 2018年 姚永波. All rights reserved.
//

#import "QuickAnswerResultVC.h"

@interface QuickAnswerResultVC ()

@property (weak, nonatomic) IBOutlet UIButton *btnPraise;
@property (weak, nonatomic) IBOutlet UIButton *btnAgain;
@property (weak, nonatomic) IBOutlet UIButton *btnAgainUp;

@property (weak, nonatomic) IBOutlet UIButton *btnStatus;
@property (weak, nonatomic) IBOutlet UILabel *lblStatus;


@end

@implementation QuickAnswerResultVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"全员抢答";
    [self updateView];
}

- (void)updateView
{
    [self.btnStatus setTitle:_model.studentName forState:UIControlStateNormal];
    if (_model.studentName.length > 0) {
        self.btnPraise.hidden = NO;
        self.btnAgain.hidden = NO;
        self.btnAgainUp.hidden = YES;
        [self.btnStatus setBackgroundImage:[UIImage imageNamed:@"quick_answer_success"] forState:UIControlStateNormal];
        [self.btnStatus setTitle:_model.studentName forState:UIControlStateNormal];
        self.lblStatus.text = @"恭喜你抢答成功！";
        self.lblStatus.textColor = UIColorFromRGB(0xC01100);
        
    }else{
        self.btnPraise.hidden = YES;
        self.btnAgain.hidden = YES;
        self.btnAgainUp.hidden = NO;
        [self.btnStatus setBackgroundImage:[UIImage imageNamed:@"quick_answer_failure"] forState:UIControlStateNormal];
        [self.btnStatus setTitle:@"无" forState:UIControlStateNormal];
        self.lblStatus.text = @"本次无人抢答！";
        self.lblStatus.textColor = JUIColorFromRGB_Main1;
    }
}

- (IBAction)praiseAction:(UIButton *)sender {
    [HDSocketSendCommandTool sendSocketParam:@{@"PraiseMode":@(1)} commandCode:@(Praise) successBlock:^(id result) {
    }];
}

- (IBAction)reaginAction:(UIButton *)sender {
    if ([self.delegate respondsToSelector:@selector(closeView)]) {
        [self.delegate closeView];
    }
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)closeAction:(UIButton *)sender {
    [HDSocketSendCommandTool sendSocketParam:@{} commandCode:@(CloseWindow) successBlock:^(id result) {
    }];
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (void)goBackPage{
    [HDSocketSendCommandTool sendSocketParam:@{} commandCode:@(CloseWindow) successBlock:^(id result) {
    }];
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
