//
//  CallDetailVC.h
//  InteractiveClassroomPhone
//
//  Created by 姚永波 on 2018/4/15.
//  Copyright © 2018年 姚永波. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YBaseViewController.h"
#import "CallStudentModel.h"
#import "CallTypeModel.h"

@interface CallDetailVC : YBaseViewController

@property (strong, nonatomic) NSMutableArray <CallStudentModel *> *collectionViewStudentsData; //举手点名学生数据
@property (strong, nonatomic) CallTypeModel *currentMode;


@end
