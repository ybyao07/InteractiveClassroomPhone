//
//  RandomCallVC.m
//  InteractiveClassroomPhone
//
//  Created by 姚永波 on 2018/4/15.
//  Copyright © 2018年 姚永波. All rights reserved.
//

#import "RandomCallVC.h"
#import "RandomGeneratorTool.h"

@interface RandomCallVC ()

@property (weak, nonatomic) IBOutlet UIButton *btnRandomName;

@property (strong, nonatomic) CallStudentModel *currentStudent;


@property (weak, nonatomic) IBOutlet UIButton *btnInitial;


@property (weak, nonatomic) IBOutlet UIButton *btnCall;
@property (weak, nonatomic) IBOutlet UIButton *btnPraise;

@end

@implementation RandomCallVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"随机点名";
    self.btnCall.hidden = YES;
    self.btnPraise.hidden = YES;
    [self.btnRandomName setTitleColor:JUIColorFromRGB_Main3 forState:UIControlStateNormal];
}


- (IBAction)goRandomCall:(UIButton *)sender {
    self.btnInitial.hidden = YES;
    self.btnCall.hidden = NO;
    self.btnPraise.hidden = NO;
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    CallStudentModel *studentM = self.studentsData[[RandomGeneratorTool getRandomNumber:0 to:self.studentsData.count-1]];
    self.currentStudent = studentM;
    dic[@"studentId"] = studentM.studentId;;
    [HDSocketSendCommandTool sendSocketParam:dic commandCode:@(StartRandom) successBlock:^(id result) {
    }];
    [self.btnRandomName setTitle:self.currentStudent.studentName forState:UIControlStateNormal];
}

- (IBAction)praiseAction:(UIButton *)sender {
    [HDSocketSendCommandTool sendSocketParam:@{@"PraiseMode":@(2)} commandCode:@(Praise) successBlock:^(id result) {
    }];
}


- (IBAction)exitAction:(UIButton *)sender {
    [HDSocketSendCommandTool sendSocketParam:@{} commandCode:@(CloseWindow) successBlock:^(id result) {
    }];
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)goBackPage
{
    [HDSocketSendCommandTool sendSocketParam:@{} commandCode:@(CloseWindow) successBlock:^(id result) {
    }];
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




@end
