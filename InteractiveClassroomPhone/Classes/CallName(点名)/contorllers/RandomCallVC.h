//
//  RandomCallVC.h
//  InteractiveClassroomPhone
//
//  Created by 姚永波 on 2018/4/15.
//  Copyright © 2018年 姚永波. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YBaseViewController.h"
#import "CallStudentModel.h"

@interface RandomCallVC : YBaseViewController

@property (strong, nonatomic) NSArray <CallStudentModel *> *studentsData; //全部学生数据

@end
