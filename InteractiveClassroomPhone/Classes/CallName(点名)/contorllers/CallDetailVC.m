//
//  CallDetailVC.m
//  InteractiveClassroomPhone
//
//  Created by 姚永波 on 2018/4/15.
//  Copyright © 2018年 姚永波. All rights reserved.
//

#import "CallDetailVC.h"
#import "TestStudentProgressCell.h"

@interface CallDetailVC ()<UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout>

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

@property (weak, nonatomic) IBOutlet UIButton *btnRefresh;
@property (weak, nonatomic) IBOutlet UIButton *btnPraiseBottom;
@property (weak, nonatomic) IBOutlet UIButton *btnPraiseUp;


@end

@implementation CallDetailVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = self.currentMode.callModelName;
    [self.collectionView registerNib:[UINib nibWithNibName:@"TestStudentProgressCell" bundle:nil] forCellWithReuseIdentifier:@"TestStudentProgressCell"];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshViewSockentData:) name:kHDNotificationCenterSocket object:nil];
    if ([self.currentMode.callModelName isEqualToString:@"举手点名"]) {
        self.btnPraiseUp.hidden = YES;
        self.btnPraiseBottom.hidden = NO;
        self.btnRefresh.hidden = NO;
    }else{
        self.btnPraiseUp.hidden = NO;
        self.btnPraiseBottom.hidden = YES;
        self.btnRefresh.hidden = YES;
    }
}

- (void)refreshViewSockentData:(NSNotification *)notif
{
    NSDictionary *dic = notif.userInfo;
    if([dic[@"code"] integerValue] == RaiseHandsStudentInfo){//返回举手点名学生信息 --- 将对应的学生设置成true
        CallStudentModel *studentM = [CallStudentModel initWithStudentId:dic[@"studentId"] name:dic[@"studentName"]];
        studentM.isRaiseHand = NO;
        [self.collectionViewStudentsData addObject:studentM];
        [self.collectionView reloadData];
    }
}
#pragma mark ================= collection delegate ==================
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.collectionViewStudentsData.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    TestStudentProgressCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"TestStudentProgressCell" forIndexPath:indexPath];
    CallStudentModel *model = self.collectionViewStudentsData[indexPath.row];
    if (model.isRaiseHand) {
        cell.backgroundColor = JUIColorFromRGB_Main3;
    }else{
        cell.backgroundColor = JUIColorFromRGB_Major1;
    }
    cell.lblName.text = model.studentName;
    return cell;
}
//定义每个UICollectionView 的大小
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake((SCREEN_WIDTH - 20 - 4*10)/4, 36);
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return 5;
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return 5;
}
//UICollectionView被选中时调用的方法
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    CallStudentModel *model = self.collectionViewStudentsData[indexPath.row];
    model.isRaiseHand = !model.isRaiseHand;
        CallStudentModel *studentM = self.collectionViewStudentsData[indexPath.row];
        [HDSocketSendCommandTool sendSocketParam:@{
                                                   @"studentId":studentM.studentId,
                                                   @"mode":self.currentMode.callModeType,
                                                   @"isselected":@(model.isRaiseHand)
                                                   } commandCode:@(SelectedStudent) successBlock:^(id result) {
                                                   }];
        [self.collectionView reloadData];
}
- (IBAction)praiseAction:(UIButton *)sender {
    [HDSocketSendCommandTool sendSocketParam:@{@"PraiseMode":@(2)} commandCode:@(Praise) successBlock:^(id result) {
    }];
    [self.collectionViewStudentsData enumerateObjectsUsingBlock:^(CallStudentModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        obj.isRaiseHand = NO;
    }];
    [self.collectionView reloadData];
}


- (IBAction)refreshAction:(UIButton *)sender {
    [HDSocketSendCommandTool sendSocketParam:@{} commandCode:@(StartRaiseHands) successBlock:^(id result) {
    }];
    [self.collectionViewStudentsData removeAllObjects];
    [self.collectionView reloadData];
}


- (IBAction)exitAction:(UIButton *)sender {
    [HDSocketSendCommandTool sendSocketParam:@{} commandCode:@(CloseWindow) successBlock:^(id result) {
    }];
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)goBackPage{
    [HDSocketSendCommandTool sendSocketParam:@{} commandCode:@(CloseWindow) successBlock:^(id result) {
    }];
    [self.navigationController popViewControllerAnimated:YES];
}

- (NSMutableArray <CallStudentModel *> *)collectionViewStudentsData
{
    if (!_collectionViewStudentsData) {
        _collectionViewStudentsData = [NSMutableArray array];
    }
    return _collectionViewStudentsData;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
