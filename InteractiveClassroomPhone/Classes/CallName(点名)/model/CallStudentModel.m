//
//  CallStudentModel.m
//  InteractiveClassroom
//
//  Created by 姚永波 on 2018/3/18.
//  Copyright © 2018年 姚永波. All rights reserved.
//

#import "CallStudentModel.h"

@implementation CallStudentModel

+ (NSDictionary *)mj_replacedKeyFromPropertyName
{
    return @{
             @"studentId"     :@"id",
             };
}

+ (instancetype)initWithStudentId:(NSString *)studentI name:(NSString *)name
{
    CallStudentModel *model = [CallStudentModel new];
    model.studentId = studentI;
    model.studentName =name;
    return model;
}


- (id)copyWithZone:(NSZone *)zone
{
    CallStudentModel * dog = [[self class] allocWithZone:zone];
    dog.cardnum = self.cardnum;
    dog.studentName = self.studentName;
    dog.studentId = self.studentId;
    dog.parentName = self.parentName;
    dog.parentTel = self.parentTel;
    dog.isRaiseHand = self.isRaiseHand;
    return dog;
}
- (id)mutableCopyWithZone:(NSZone *)zone
{
    CallStudentModel * dog = [[self class] allocWithZone:zone];
    dog.cardnum = self.cardnum;
    dog.studentName = self.studentName;
    dog.studentId = self.studentId;
    dog.parentName = self.parentName;
    dog.parentTel = self.parentTel;
    dog.isRaiseHand = self.isRaiseHand;
    return dog;
}
@end
