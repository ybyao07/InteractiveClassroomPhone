//
//  CallTypeModel.m
//  InteractiveClassroom
//
//  Created by 姚永波 on 2018/3/18.
//  Copyright © 2018年 姚永波. All rights reserved.
//

#import "CallTypeModel.h"

@implementation CallTypeModel

+ (CallTypeModel *)modelWithName:(NSString *)name type:(NSString *)type
{
    CallTypeModel *model = [[CallTypeModel alloc] init];
    model.callModelName = name;
    model.callModeType = type;
    return model;
}
@end
