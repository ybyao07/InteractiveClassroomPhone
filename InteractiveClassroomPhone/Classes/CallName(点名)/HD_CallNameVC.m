//
//  HD_CallNameVC.m
//  InteractiveClassroomPhone
//
//  Created by 姚永波 on 2018/3/29.
//  Copyright © 2018年 姚永波. All rights reserved.
//

#import "HD_CallNameVC.h"
#import "CallStudentModel.h"
#import "RandomCallVC.h"
#import "CallDetailVC.h"
@interface HD_CallNameVC ()

@property (strong, nonatomic) NSArray <CallStudentModel *> *studentsData; //全部学生数据
@end

@implementation HD_CallNameVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"点名";
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshViewSockentData:) name:kHDNotificationCenterSocket object:nil];
    [self sendInitSocket];
    [self showLoadAnimation];
//    [self addTestData];
}

- (void)sendInitSocket{
    [HDSocketSendCommandTool sendSocketParam:@{@"mode":@(3)} commandCode:@(RollCall) successBlock:^(id result) {
    }];
}

- (void)refreshViewSockentData:(NSNotification *)notif
{
    NSDictionary *dic = notif.userInfo;
    if ([dic[@"code"] integerValue] == StudentsList) { //获取学生列表
        [self removeLoadAnimation];
        self.studentsData = [CallStudentModel mj_objectArrayWithKeyValuesArray:dic[@"studentList"]];
    }
}
- (void)addTestData
{
    NSDictionary *dic = @{
                          @"studentList":@[
                                  @{
                                      @"id":@"124",
                                      @"cardNum":@"2061205",
                                      @"studentName":@"张三"
                                      },
                                  @{
                                      @"id":@"125",
                                      @"cardNum":@"2061205",
                                      @"studentName":@"李四"
                                      }
                                  ]
                          };
    self.studentsData = [CallStudentModel mj_objectArrayWithKeyValuesArray:dic[@"studentList"]];
    [self removeLoadAnimation];
}


#pragma mark ====== Action ======
- (IBAction)randomVCAction:(UIButton *)sender {
    [HDSocketSendCommandTool sendSocketParam:@{@"mode":@"0"} commandCode:@(RollCall) successBlock:^(id result) {
    }];
    RandomCallVC *randVC = [RandomCallVC new];
    randVC.studentsData = self.studentsData;
    [self.navigationController pushViewController:randVC animated:YES];
}
- (IBAction)raiseHandAction:(UIButton *)sender {
    [HDSocketSendCommandTool sendSocketParam:@{@"mode":@"2"} commandCode:@(RollCall) successBlock:^(id result) {
    }];
    [HDSocketSendCommandTool sendSocketParam:@{} commandCode:@(StartRaiseHands) successBlock:^(id result) {
    }];
    CallDetailVC *detailVC = [[CallDetailVC alloc] init];
    detailVC.currentMode = [CallTypeModel modelWithName:@"举手点名" type:@"2"];
    [self.navigationController pushViewController:detailVC animated:YES];
}
- (IBAction)allMemberAction:(UIButton *)sender {
    [HDSocketSendCommandTool sendSocketParam:@{@"mode":@"1"} commandCode:@(RollCall) successBlock:^(id result) {
    }];
    CallDetailVC *detailVC = [[CallDetailVC alloc] init];
    detailVC.currentMode = [CallTypeModel modelWithName:@"花名册点名" type:@"1"];
    NSMutableArray *arrayM = [NSMutableArray arrayWithCapacity:self.studentsData.count];
    [self.studentsData enumerateObjectsUsingBlock:^(CallStudentModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        CallStudentModel *newM = [obj copy];
        [arrayM addObject:newM];
    }];
    detailVC.collectionViewStudentsData = arrayM;
    [self.navigationController pushViewController:detailVC animated:YES];
}

- (void)goBackPage
{
    [HDSocketSendCommandTool sendSocketParam:@{} commandCode:@(CloseWindow) successBlock:^(id result) {
    }];
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
